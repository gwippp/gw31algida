#!/bin/bash

HOST=192.168.0.138  #This is the FTP servers host or IP address.
USER=admin             #This is the FTP user that has access to the server.
PASSWD=init99EE          #This is the password for the FTP user.

#-n Restrains FTP from attempting the auto-login feature. 

ftp -n -i $HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
cd /Work/Algida/Uploader
lcd /Users/gwhungary/Work/Interface

mget 5_doc_xRADAR*

quit
END_SCRIPT

cd /Users/gwhungary/Work/Interface
mv 5_doc_xRADAR_AKCIO* RADAR_AKCIO_V01.csv
mv 5_doc_xRADAR_CSATORNA* RADAR_CSATORNA_V01.csv
mv 5_doc_xRADAR_DEPO_V01* RADAR_DEPO_V01.csv
mv 5_doc_xRADAR_SRP_V01* RADAR_SRP_V01.csv
mv 5_doc_xRADAR_VEVO_V01* RADAR_VEVO_V01.csv

cd /Users/gwhungary/Work
sh ./gw31algida-upload.sh

exit 0