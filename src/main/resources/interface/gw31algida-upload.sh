#!/bin/bash
#
### BEGIN INIT INFO
# Provides: gw3
# Required-Start: $network
# X-UnitedLinux-Should-Start:
# Required-Stop: $network
# X-UnitedLinux-Should-Stop:
# Default-Start: 3 5
# Default-Stop: 0 1 2 6
# Description: GW31Algida Uploader
### END INIT INFO
JAVA_HOME=/usr/lib/jvm/jdk1.8.0_40
JAVA_OPTS="-Ddummy=GW31AlgidaUploader -Dlog4j.debug -Dlog4j.configuration=log4j-algida.properties -Dgw3.live=true -Djava.library.path=/usr/lib/jni/ -Djmagick.systemclassloader=no -Dgw.live=true -DshouldRegenerateIndexes=true -Dgwconfig_temp_folder=/tmp -Djetty.home=/Users/zsolthorvath/git/gw31algida/gw31algida/ -Dcsv_upload_folder=/Users/zsolthorvath/tmp/algidauploader/interface/"
GW3_DEMO_HOME="/Users/zsolthorvath/git/gw31algida/gw31algida"

#GW3_CLASSPATH=/root/git/gw31core/gw31core/extensions:$(find ${GW3_DEMO_HOME}/target/lib/ -name '*.jar' -printf '%p:' | sed 's/:$//')
GW3_CLASSPATH=/root/git/gw31core/gw31core/extensions:$(find ${GW3_DEMO_HOME}/target/lib/ -name '*.jar' | xargs echo | tr ' ' ':')

for GW3_PROJ_NAME in ${GW3_PROJECTS[*]}
do
GW3_PROJ_HOME=/root/git/${GW3_PROJ_NAME}/${GW3_PROJ_NAME}
GW3_CLASSPATH=${GW3_PROJ_HOME}/target/classes:${GW3_PROJ_HOME}/extensions:${GW3_CLASSPATH}
done

# Start GW3Server
echo -n $'Starting GW31 Algida Uploader\n'
nohup $JAVA_HOME/bin/java ${JAVA_OPTS} -cp ${GW3_CLASSPATH} org.junit.runner.JUnitCore com.gollywolly.persistance.tests.TestResourceAndPromoUpload &> /root/algidauploader/interface/log/gw31algidauploader.log

