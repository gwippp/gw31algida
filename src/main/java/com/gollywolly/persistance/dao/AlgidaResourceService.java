package com.gollywolly.persistance.dao;

public interface AlgidaResourceService {

	public void logFeedBack(String feedBackText, String feedBackEmail, String address, String lata, String longb, Long messageId);

	public void routeSearch(String lat, String lng, String resourceId, String customAddress);
}
