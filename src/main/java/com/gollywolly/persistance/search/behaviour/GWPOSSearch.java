package com.gollywolly.persistance.search.behaviour;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;

import org.apache.lucene.queries.ChainedFilter;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.springframework.context.ApplicationContext;
import org.springframework.util.CollectionUtils;

import com.gollywolly.gw3utils.lucenesearcher.SearchClause;
import com.gollywolly.gw3utils.lucenesearcher.SearchField;
import com.gollywolly.gw3utils.persistance.entity.IEntity;
import com.gollywolly.gw3utils.pluginloader.utils.StringUtils;
import com.gollywolly.persistance.dao.ResourceService;
import com.gollywolly.persistance.dao.UserService;
import com.gollywolly.persistance.model.Errors;
import com.gollywolly.persistance.model.Resource;
import com.gollywolly.persistance.model.User;
import com.gollywolly.persistance.search.behaviour.resources.AbstractGWResourceSearchBehaviour;

public class GWPOSSearch extends AbstractGWResourceSearchBehaviour{

	public static String ROLE_SUPERADMIN = "ROLE_SUPERADMIN";
	public static String ROLE_CARELINE = "ROLE_CARELINE";
	
	public static String AGENT_ID = "agentid";
	public static String DEPO_ID = "depoid";
	public static String APPROVED_STATUS = "locstat";
	public static String SEARCH = "search";
	
	public GWPOSSearch(ResourceService service, ApplicationContext appContext, HttpServletRequest request, Errors errors){
		super(service, appContext, null, null, null, null, request, errors);
	}

	
	@Override
	public void initSearchBehaviour(HttpServletRequest request) {
		Map<String, Object> searchCriteria = getSearchCriteria();
		try {
			String approvedStatus = (String)(request.getParameterValues(APPROVED_STATUS)[0]);
			if (org.springframework.util.StringUtils.hasText(approvedStatus))
				searchCriteria.put(APPROVED_STATUS, approvedStatus);
		} catch (Exception e) {}
		try {
			String approvedStatus = (String)(request.getParameterValues(SEARCH)[0]);
			if (org.springframework.util.StringUtils.hasText(approvedStatus))
				searchCriteria.put(SEARCH, approvedStatus);
		} catch (Exception e) {}
		try {
			Boolean isSuperAdmin = request.isUserInRole(ROLE_SUPERADMIN) || request.isUserInRole(ROLE_CARELINE);
			if (!isSuperAdmin) {
				UserService userService = appContext.getBean(UserService.class);
				String userId = request.getUserPrincipal().getName();
				User user = userService.getUserById(Long.valueOf(userId));
				if (user != null)
					searchCriteria.put(AGENT_ID, user.getUserName());
			} else {
				try {
					String approvedStatus = (String)(request.getParameterValues(DEPO_ID)[0]);
					if (org.springframework.util.StringUtils.hasText(approvedStatus))
						searchCriteria.put(DEPO_ID, approvedStatus);
				} catch (Exception e) {}
			}
		} catch (Exception e) {}
		setSearchCriteria(searchCriteria);
		
		super.initSearchBehaviour(request);
		
	}
	
	@Override
	public List<SearchClause> getSearchClausesOfBehaviour() {
		
		List<SearchClause> searchClauses = new ArrayList<SearchClause>();
		
		SearchClause clause = new SearchClause(Occur.MUST);
		SearchField field = new SearchField(RESOURCE_TYPE, Resource.TYPE_POS, SearchField.TERM, Occur.SHOULD, Occur.MUST, false);
		field.disableAnalyze();
		clause.addSearchField(field); 
		searchClauses.add(clause);
		
		if(getSearchCriteria().containsKey(AGENT_ID)){
			clause = new SearchClause(Occur.MUST);
			field = new SearchField(AGENT_ID, (String)(getSearchCriteria().get(AGENT_ID)), SearchField.TERM, Occur.SHOULD, Occur.MUST, false);
			field.disableAnalyze();
			clause.addSearchField(field);
			searchClauses.add(clause);
		}
		
		if(getSearchCriteria().containsKey(APPROVED_STATUS)){
			clause = new SearchClause(Occur.MUST);
			field = new SearchField("geodataapproved", (String)(getSearchCriteria().get(APPROVED_STATUS)), SearchField.TERM, Occur.SHOULD, Occur.MUST, false);
			field.disableAnalyze();
			clause.addSearchField(field);
			searchClauses.add(clause);
		}
		
		//Always filter by authorized projects
		if(getSearchCriteria().containsKey(SEARCH)){
			clause = new SearchClause(Occur.MUST);
			field = new SearchField(TITLE, (String)(getSearchCriteria().get(SEARCH)), SearchField.WILDCARD, Occur.SHOULD, Occur.MUST, false);
			field.disableAnalyze();
			clause.addSearchField(field);
			field = new SearchField(DESCRIPTION, (String)(getSearchCriteria().get(SEARCH)), SearchField.WILDCARD, Occur.SHOULD, Occur.MUST, false);
			field.disableAnalyze();
			clause.addSearchField(field);
			field = new SearchField(DETAILS, (String)(getSearchCriteria().get(SEARCH)), SearchField.WILDCARD, Occur.SHOULD, Occur.MUST, false);
			field.disableAnalyze();
			clause.addSearchField(field);
			searchClauses.add(clause);
		}
		
		List<SearchClause> additionalClauses = super.getSearchClausesOfBehaviour();
		if(!CollectionUtils.isEmpty(additionalClauses))
			searchClauses.addAll(additionalClauses);
		
		for(Map.Entry<String, Object> criteria : getSearchCriteria().entrySet()){
			logger.debug(criteria.getKey() + " : " + criteria.getValue());
		}
		
		return searchClauses;
	}
	
	@Override
	public Sort getSort(){
		Sort sort = null;
		SortField[] sortFields;
		sortFields = new SortField[2];
		sortFields[0] = new SortField("geodataapproved", SortField.Type.STRING, false);
		sortFields[1] = new SortField("title", SortField.Type.STRING, false);
		sort = new Sort(sortFields);
		
		return sort;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	public void postProcessLoadedObjects(List<IEntity> objects, Map<String, Object> options){
		for (IEntity resource : objects) {
			service.loadResourceProfileImage((Resource)resource);
		}
		
		if(objects != null)
			service.bulkLoadData((List) objects, null);
		
		super.postProcessLoadedObjects(objects, options);
	}
	
	@Override
	public boolean shouldLoadSubObjects(AtomicInteger level, List<String> includeObjectsOfType){
		
		
		return false;
	}
	
	@Override
	public LimitToObjects limitToObjectIds(){
		String depoId = (String)getSearchCriteria().get(DEPO_ID);
		if(!StringUtils.isInitial(depoId)){
			try {
				
				List<Long> relatedIds = service.getTargetIdsByResourceId1FilteredByResourceType2("pos", StringUtils.getStringAsLong(depoId));
				
				LimitToObjects limitToObjects = new LimitToObjects(relatedIds, ChainedFilter.AND);

				return limitToObjects;
			} catch (Exception e) {

			}
		}
		
		return null;
	}

}
