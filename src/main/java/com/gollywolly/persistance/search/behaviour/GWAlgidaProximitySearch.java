package com.gollywolly.persistance.search.behaviour;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;

import org.apache.lucene.queries.ChainedFilter;
import org.apache.lucene.queries.function.ValueSource;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopDocsCollector;
import org.apache.lucene.spatial.prefix.tree.GeohashPrefixTree;
import org.apache.lucene.spatial.prefix.tree.SpatialPrefixTree;
import org.apache.lucene.spatial.query.SpatialArgs;
import org.apache.lucene.spatial.query.SpatialOperation;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

import com.gollywolly.config.ResourceType;
import com.gollywolly.gw3utils.lucenesearcher.IndexSearcherProvider;
import com.gollywolly.gw3utils.lucenesearcher.RecursivePrefixTreeStrategy;
import com.gollywolly.gw3utils.lucenesearcher.SearchClause;
import com.gollywolly.gw3utils.lucenesearcher.SearchField;
import com.gollywolly.gw3utils.persistance.entity.IEntity;
import com.gollywolly.gw3utils.pluginloader.ConfigurationHolder;
import com.gollywolly.gw3utils.pluginloader.config.Aspect;
import com.gollywolly.gw3utils.pluginloader.utils.AppContextHolder;
import com.gollywolly.gw3utils.pluginloader.utils.StringUtils;
import com.gollywolly.persistance.dao.ResourceService;
import com.gollywolly.persistance.model.Errors;
import com.gollywolly.persistance.model.Resource;
import com.gollywolly.persistance.search.ResourceIndexBuilder;
import com.gollywolly.persistance.search.behaviour.resources.AbstractGWResourceSearchBehaviour;
import com.gollywolly.utils.GoogleGeodataUtils;
import com.spatial4j.core.context.SpatialContext;
import com.spatial4j.core.shape.Point;
import com.spatial4j.core.shape.Rectangle;

public class GWAlgidaProximitySearch extends AbstractGWResourceSearchBehaviour{
	public static final int MAX_NUMBER_OF_DOCUMENTS = 200;
	
	public static class Factory implements SearchFactory {
		public GWAlgidaProximitySearch createSearch(ResourceService service, ApplicationContext appContext, HttpServletRequest request, Errors errors){
			return new GWAlgidaProximitySearch(service, appContext, request, errors);
		}
	}
	
	private SpatialContext ctx;
	private SpatialPrefixTree grid;
	private RecursivePrefixTreeStrategy strategy;
	public static String RELATED_PROMOTION ="promotionid";
	
	public GWAlgidaProximitySearch(ResourceService service, ApplicationContext appContext, HttpServletRequest request, Errors errors){
		super(service, appContext, null, null, null, MAX_NUMBER_OF_DOCUMENTS, request, errors);
	}

	public static String LATITUDE ="latitude";
	public static String LONGITUDE ="longitude";
	public static String DISTANCE ="distance";
	public static String LOAD_ASPECT ="loadaspect";
	public static String RELATED_RESOURCE ="relatedid";
	
	@Override
	public void initSearchBehaviour(HttpServletRequest request) {

		ctx = SpatialContext.GEO;
		grid = new GeohashPrefixTree(ctx, 11);
		strategy = new RecursivePrefixTreeStrategy(grid, "resource");
		
		Map<String, Object> searchCriteria = getSearchCriteria();
		try {
			searchCriteria.put(LATITUDE, (String)(request.getParameterValues(LATITUDE)[0]));
		} catch (Exception e) {}
		try {
			searchCriteria.put(LONGITUDE, (String)(request.getParameterValues(LONGITUDE)[0]));
		} catch (Exception e) {}
		try {
			searchCriteria.put(GoogleGeodataUtils.SOUTH_WEST_LATIITUDE, (String)(request.getParameterValues(GoogleGeodataUtils.SOUTH_WEST_LATIITUDE)[0]));
		} catch (Exception e) {}
		try {
			searchCriteria.put(GoogleGeodataUtils.NORTH_EAST_LATIITUDE, (String)(request.getParameterValues(GoogleGeodataUtils.NORTH_EAST_LATIITUDE)[0]));
		} catch (Exception e) {}
		try {
			searchCriteria.put(GoogleGeodataUtils.SOUTH_WEST_LONGITUDE, (String)(request.getParameterValues(GoogleGeodataUtils.SOUTH_WEST_LONGITUDE)[0]));
		} catch (Exception e) {}
		try {
			searchCriteria.put(GoogleGeodataUtils.NORTH_EAST_LONGITUDE, (String)(request.getParameterValues(GoogleGeodataUtils.NORTH_EAST_LONGITUDE)[0]));
		} catch (Exception e) {}
		try {
			searchCriteria.put(RESOURCE_TYPE, (String)(request.getParameterValues(RESOURCE_TYPE)[0]));
		} catch (Exception e) {}
		try {
			Object loeadAspects = (request.getParameterValues(LOAD_ASPECT));
			if(loeadAspects != null)
				getOptions().put(LOAD_ASPECT, loeadAspects);
		} catch (Exception e) {}
		try {
			Object loeadAspects = (request.getParameterValues(RELATED_RESOURCE)[0]);
			if(loeadAspects != null)
				getOptions().put(RELATED_RESOURCE, loeadAspects);
		} catch (Exception e) {}
		
		setSearchCriteria(searchCriteria);
		
		super.initSearchBehaviour(request);
	}
	
	@Override
	public List<SearchClause> getSearchClausesOfBehaviour() {
		
		List<SearchClause> searchClauses = new ArrayList<SearchClause>();
		
		SearchClause clause = new SearchClause(Occur.MUST);
		SearchField field = null;
		if(getSearchCriteria().containsKey(RESOURCE_TYPE)){
			field = new SearchField(RESOURCE_TYPE, (String)(getSearchCriteria().get(RESOURCE_TYPE)), SearchField.TERM, Occur.SHOULD, Occur.MUST, false);
			field.disableAnalyze();
			clause.addSearchField(field); 
			searchClauses.add(clause);
		}
		
		clause = new SearchClause(Occur.MUST);
		clause.addSearchField(new SearchField("geodataapproved", "30", SearchField.TERM)); //at least one of the words must contain
		searchClauses.add(clause);
		
		clause = new SearchClause(Occur.MUST);
		field = new SearchField(ResourceIndexBuilder.ACTIVE, "T", SearchField.TERM);
		field.disableAnalyze();
		clause.addSearchField(field); 
		searchClauses.add(clause);
		
		//Always filter by authorized projects
		if(getSearchCriteria().containsKey("keyword")){
			clause = new SearchClause(Occur.MUST);
			field = new SearchField(TITLE, (String)(getSearchCriteria().get(KEYWORD)), SearchField.WILDCARD, Occur.SHOULD, Occur.MUST, false);
			field.disableAnalyze();
			clause.addSearchField(field);
			field = new SearchField(DESCRIPTION, (String)(getSearchCriteria().get(KEYWORD)), SearchField.WILDCARD, Occur.SHOULD, Occur.MUST, false);
			field.disableAnalyze();
			clause.addSearchField(field);
			field = new SearchField(DETAILS, (String)(getSearchCriteria().get(KEYWORD)), SearchField.WILDCARD, Occur.SHOULD, Occur.MUST, false);
			field.disableAnalyze();
			clause.addSearchField(field);
			searchClauses.add(clause);
		}
		
		for(Map.Entry<String, Object> criteria : getSearchCriteria().entrySet()){
			logger.debug(criteria.getKey() + " : " + criteria.getValue());
		}
		
		return searchClauses;
	}
	
	@Override
	public Sort getSort(){
		Point centerPoint = (Point)getSearchCriteria().get(GoogleGeodataUtils.CENTER_POINT);
		
		if (centerPoint != null) {
			Sort distSort = null;
		
			ValueSource valueSource = strategy.makeDistanceValueSource(centerPoint);
			
			try {
				distSort = new Sort(valueSource.getSortField(false)).rewrite(((IndexSearcherProvider)appContext.getBean("resourceIndexSearcher")).getIndexSearcher());
			} catch (BeansException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return distSort;
			
		} else {
			Sort sort = null;
			SortField[] sortFields;
			sortFields = new SortField[1];
			sortFields[0] = new SortField(RESOURCE_ID, SortField.Type.STRING, false);
			sort = new Sort(sortFields);
			
			return sort;
		}
	}
	
	@Override
	public void initLuceneFilter(){
		if(getSearchCriteria().containsKey(GoogleGeodataUtils.SOUTH_WEST_LATIITUDE) && getSearchCriteria().containsKey(GoogleGeodataUtils.SOUTH_WEST_LONGITUDE)){
			Double minLat = Double.parseDouble(getSearchCriteria().get(GoogleGeodataUtils.SOUTH_WEST_LATIITUDE).toString());
			Double minLng = Double.parseDouble(getSearchCriteria().get(GoogleGeodataUtils.SOUTH_WEST_LONGITUDE).toString());
			Double maxLat = Double.parseDouble(getSearchCriteria().get(GoogleGeodataUtils.NORTH_EAST_LATIITUDE).toString());
			Double maxLng = Double.parseDouble(getSearchCriteria().get(GoogleGeodataUtils.NORTH_EAST_LONGITUDE).toString());
			
			Rectangle r = ctx.makeRectangle(minLng, maxLng, minLat, maxLat);
			strategy.setSearchBounds(grid, r);
			getSearchCriteria().put(GoogleGeodataUtils.CENTER_POINT, r.getCenter());
			SpatialArgs args = new SpatialArgs(SpatialOperation.IsWithin, r);
			filter = strategy.makeFilter(args);
		}
	}
	
	
	
	@Override
	public TopDocsCollector<? extends ScoreDoc> createCustomCollector() {
		return strategy.createCollector(getSort().getSort()[0]);
	}

	@Override
	public void postProcessLoadedObjects(List<IEntity> objects, Map<String, Object> options){
		if(objects != null) {
			String[] additionalAspects = null;
			if(!getOptions().isEmpty()){
				if(getOptions().containsKey(LOAD_ASPECT)){
					Object loadAspectTemp = getOptions().get(LOAD_ASPECT);
					if(loadAspectTemp instanceof String){
						additionalAspects = new String[]{(String)loadAspectTemp};
					} else if(loadAspectTemp instanceof String[]){
						additionalAspects = (String[])loadAspectTemp;
					}
				}
			}
			ConfigurationHolder configHolder = (ConfigurationHolder)appContext.getBean(ConfigurationHolder.class);
			Map<List<String>, List<Resource>> resourceMaps = new HashMap<List<String>, List<Resource>>();
			if (objects != null) {
				for (IEntity r : objects) {
					if (r instanceof Resource) {
						ResourceType type = (ResourceType)configHolder.getQualifiedConfiguration(ResourceType.RESOURCE_CONFIGURATION_TYPE, AppContextHolder.getAppId(), ((Resource)r).getResourceType());
						Aspect mapAspect = type.getMapAspect();
						if (mapAspect != null) {
							List<Resource> resources = null;
							List<String> aspectsToLoad = new ArrayList<String>();
							aspectsToLoad.add(mapAspect.getAspectName());
							if (additionalAspects != null)
								aspectsToLoad.addAll(Arrays.asList(additionalAspects));
							if (resourceMaps.containsKey(aspectsToLoad)) {
								resources = resourceMaps.get(aspectsToLoad);
							} else {
								resources = new ArrayList<Resource>();
								resourceMaps.put(aspectsToLoad, resources);
							}
							
							resources.add(((Resource)r));
						}
					}
				}
				
				for (Map.Entry<List<String>, List<Resource>> entry : resourceMaps.entrySet()) {
					String[] aspectsToLoad = new String[entry.getKey().size()];
					entry.getKey().toArray(aspectsToLoad);
					service.bulkLoadData(entry.getValue(), aspectsToLoad);
				}
			}
			
		}
	}
	
	@Override
	public boolean shouldLoadSubObjects(AtomicInteger level, List<String> includeObjectsOfType){
		return false;
	}
	
	@Override
	public LimitToObjects limitToObjectIds(){
		if(getOptions().containsKey(RELATED_RESOURCE)){
			try {
				String resourceType = null;
				if(getSearchCriteria().containsKey(RESOURCE_TYPE))
					resourceType = (String)getSearchCriteria().get(RESOURCE_TYPE);
					Long relatedPromotionId = StringUtils.getStringAsLong((String)getOptions().get(RELATED_RESOURCE));
	
				List<Long> relatedIds = service.getTargetIdsByResourceId1FilteredByResourceType2(resourceType, relatedPromotionId);
				
				LimitToObjects limitToObjects = new LimitToObjects(relatedIds, ChainedFilter.AND);

				return limitToObjects;
			} catch (Exception e) {

			}
		}
		return null;
	}
}
