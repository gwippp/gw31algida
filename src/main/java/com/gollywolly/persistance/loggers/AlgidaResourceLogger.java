package com.gollywolly.persistance.loggers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.gollywolly.gw3utils.logger.annotation.AfterMethod;
import com.gollywolly.gw3utils.logger.annotation.BeforeMethod;
import com.gollywolly.gw3utils.logger.annotation.ModuleLogger;
import com.gollywolly.gw3utils.logger.dao.LogService;
import com.gollywolly.gw3utils.logger.model.LogEvent;
import com.gollywolly.gw3utils.logger.service.BaseLogger;
import com.gollywolly.gw3utils.logger.utils.LoggerUtils;
import com.gollywolly.gw3utils.persistance.entity.EntityFactory;
import com.gollywolly.persistance.dao.ResourceService;
import com.gollywolly.persistance.dao.UserService;
import com.gollywolly.persistance.model.Resource;
import com.gollywolly.persistance.model.User;
import com.gollywolly.persistance.search.behaviour.GWSearchBehaviour;
import com.gollywolly.persistance.search.behaviour.resources.AbstractGWResourceSearchBehaviour;
import com.gollywolly.utils.StringUtils;
import com.gollywolly.utils.WebUtils;

@ModuleLogger(targetClass="com.gollywolly.persistance.dao.ResourceService")
public class AlgidaResourceLogger extends BaseLogger {
	
	public static final String MODUL_NAME = "resource";
	
	
	private static final String DEFAULT_CONFIGURATION = "com/gollywolly/persistance/loggers/algida-resource-logger.xml";
	
	private static String[] SIMPLE_LOCATION_FIELDS = {"title","description", "posaddressmap_heading", "posaddressmap_pitch",
				"posaddressmap_latitude", "posaddressmap_longitude", "posaddressmap_approved", "posalternativeaddressmap_altaddress"};
	
	public static final String ROUTE_EVENT_TYPE = "route";
	public static final String FEEDBACK_EVENT_TYPE = "feedback";
	
	private Logger logger = Logger.getLogger(AlgidaResourceLogger.class);
	
	@Autowired
	private ResourceService resourceService;
	
	@Autowired
	private UserService userService;
	
	@Autowired 
	private LogService loggerService;

	@Override
	protected String getDefualtConfiguration() {
		return DEFAULT_CONFIGURATION;
	}
	
	@BeforeMethod(beforeMethod="updateData")
	public Resource getOldResource(Resource resource) {
		Resource oldResource  = resourceService.getResourceByResourceId(resource.getIdFieldAsLong());
		return oldResource;
	
	}
	
	@SuppressWarnings("static-access")
	@AfterMethod(afterMethod="updateData")
	public void logUpdateResource(Resource oldResource, Resource resource){
		logger.debug("logUpdateResource");
		WebUtils webUtils = new WebUtils();
		List<LogEvent> logEvents = new ArrayList<LogEvent>();
		
		Long userId = null;
		String userName = null;
		HttpServletRequest request = null;
		if ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes() != null){
			
			request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
			
			if (request != null) {
				Principal principal = request.getUserPrincipal();
				if (principal == null)
					userName = request.getRemoteAddr();
				else {
					userId = Long.valueOf(principal.getName());
					User loggedInUser = userService.getUserById(userId);
					
					if (loggedInUser != null) {
						userName = loggedInUser.getLastName()+", "+loggedInUser.getFirstName();
						userId = loggedInUser.getUserId();
					}
				}
			}
		}
		
		LogEvent logEvent = (LogEvent)EntityFactory.createEntityForModel(LogEvent.class);
		logEvent.setEventType(UPDATE_EVENT_TYPE);
		logEvent.setModuleName(MODUL_NAME);
		logEvent.setResourceKey(oldResource.getIdFieldAsLong().toString());
		logEvent.setResourceName(oldResource.getObjectType());
		logEvent.setUserId(userId);
		logEvent.setUserName(userName);
		
		//Log simple fields
		for (String fieldName : SIMPLE_LOCATION_FIELDS) {

			Object value = oldResource.get(fieldName);
			if (value != null && value instanceof List && ((List<?>)value).size() == 1)
				value = ((List<?>)value).get(0);
			
			Object newValue = resource.get(fieldName);
			if (newValue != null && newValue instanceof List && ((List<?>)newValue).size() == 1)
				newValue = ((List<?>)newValue).get(0);
		
			logEventParameter(logEvent, fieldName, 1, true, LoggerUtils.getValueAsString(value),
					LoggerUtils.getValueAsString(newValue),EVENT_PARAMETER_UPDATE);

		}
		
		if (request != null){
			logEventParameter(logEvent, "devicetype", 1, false, null, webUtils.getDeviceType(request), EVENT_PARAMETER_ONLY);
			logEventParameter(logEvent, "useragent", 1, false, null, request.getHeader("User-agent"), EVENT_PARAMETER_ONLY);
		}
		
		
		if (logEvent.getLogEventParameter() != null && logEvent.getLogEventParameter().size()>0){
			logEvents.add(logEvent);
			
			logEventUseSameChangeId(logEvents, true);
			
		}
	}

	@SuppressWarnings("static-access")
	@AfterMethod(afterMethod="getResourceByProximity")
	public void logGeoDataSearch(GWSearchBehaviour<Resource, ResourceService> behaviour) {
		logger.debug("logGeoDataSearch");
		WebUtils webUtils = new WebUtils();
		
		Map<String, Object> searchCriteria = behaviour.getSearchCriteria();
		
		List<LogEvent> logEvents = new ArrayList<LogEvent>();
		
		Long userId = null;
		String userName = null;
		String address = null;
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		if (request != null) {
			Principal principal = request.getUserPrincipal();
			if (principal == null)
				userName = request.getRemoteAddr();
			else {
				userId = Long.valueOf(principal.getName());
				User loggedInUser = userService.getUserById(userId);
				
				if (loggedInUser != null) {
					userName = loggedInUser.getLastName()+", "+loggedInUser.getFirstName();
					userId = loggedInUser.getUserId();
				}
			}
			if(!StringUtils.isInitial(request.getParameter("customAddress"))){
				address = request.getParameter("customAddress");
			}
		}
		
		LogEvent logEvent = (LogEvent)EntityFactory.createEntityForModel(LogEvent.class);
		logEvent.setEventType(SEARCH_EVENT_TYPE);
		logEvent.setModuleName(MODUL_NAME);
		
		String resourceType = (String) searchCriteria.get(AbstractGWResourceSearchBehaviour.RESOURCE_TYPE);
		
		if (resourceType != null){
			logEvent.setResourceName((String) searchCriteria.get(AbstractGWResourceSearchBehaviour.RESOURCE_TYPE));
		}else{
			logEvent.setResourceName(MODUL_NAME);
		}
		logEvent.setUserId(userId);
		logEvent.setUserName(userName);
		
		for (Entry<String, Object> entry : searchCriteria.entrySet())
		{
			logEventParameter(logEvent, entry.getKey(), 1, false, null, entry.getValue(), EVENT_PARAMETER_ONLY);
		//	System.out.println(entry.getKey() + "/" + entry.getValue());
		}
		
		logEventParameter(logEvent, "devicetype", 1, false, null, webUtils.getDeviceType(request), EVENT_PARAMETER_ONLY);
		logEventParameter(logEvent, "useragent", 1, false, null, request.getHeader("User-agent"), EVENT_PARAMETER_ONLY);
		
		if(!StringUtils.isInitial(address)){
			logEventParameter(logEvent, "customAddress", 1, false, null, address, EVENT_PARAMETER_ONLY);
		}
		
		logEvents.add(logEvent);
		
		logEventUseSameChangeId(logEvents, true);
		
	}
	
	@SuppressWarnings("static-access")
	@AfterMethod(afterMethod="logFeedBack", targetClass="com.gollywolly.persistance.dao.AlgidaResourceService")
	public void logFeedBack(String feedBackText, String feedBackEmail, String address, String lata, String longb, Long messageId){
		WebUtils webUtils = new WebUtils();
		List<LogEvent> logEvents = new ArrayList<LogEvent>();
		
		Long userId = null;
		String userName = null;
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		if (request != null) {
			Principal principal = request.getUserPrincipal();
			if (principal == null)
				userName = request.getRemoteAddr();
			else {
				userId = Long.valueOf(principal.getName());
				User loggedInUser = userService.getUserById(userId);
				if (loggedInUser != null) {
					userName = loggedInUser.getLastName() + ", "
							+ loggedInUser.getFirstName();
					userId = loggedInUser.getUserId();
				}
			}
		}
		
		LogEvent event = (LogEvent)EntityFactory.createEntityForModel(LogEvent.class);
		event.setEventType(FEEDBACK_EVENT_TYPE);
		event.setModuleName(MODUL_NAME);
		event.setResourceKey(messageId.toString());
		event.setResourceName("message");
		event.setUserId(userId);
		event.setUserName(userName);
		
		logEvents.add(event);
		
		logEventParameter(event, "feedbacktext", 1, false, null, feedBackText, EVENT_PARAMETER_ONLY);
		if(feedBackEmail != null){
			logEventParameter(event, "feedbackemail", 1, false, null, feedBackEmail, EVENT_PARAMETER_ONLY);
		}
		if(address != null){
			logEventParameter(event, "customAddress", 1, false, null, address, EVENT_PARAMETER_ONLY);
		}
		if(lata != null && longb != null){
			logEventParameter(event, "latitude", 1, false, null, lata, EVENT_PARAMETER_ONLY);
			logEventParameter(event, "longitude", 1, false, null, longb, EVENT_PARAMETER_ONLY);
		}
		logEventParameter(event, "devicetype", 1, false, null, webUtils.getDeviceType(request), EVENT_PARAMETER_ONLY);
		logEventParameter(event, "useragent", 1, false, null, request.getHeader("User-agent"), EVENT_PARAMETER_ONLY);
		logEventUseSameChangeId(logEvents, true);
	}//logFeedBack
	
	@SuppressWarnings("static-access")
	@AfterMethod(afterMethod="routeSearch", targetClass="com.gollywolly.persistance.dao.AlgidaResourceService")
	public void logRouteSearch(String lat, String lng, String resourceId, String customAddress){
		WebUtils webUtils = new WebUtils();
		List<LogEvent> logEvents = new ArrayList<LogEvent>();

		Long userId = null;
		String userName = null;
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		if (request != null) {
			Principal principal = request.getUserPrincipal();
			if (principal == null)
				userName = request.getRemoteAddr();
			else {
				userId = Long.valueOf(principal.getName());
				User loggedInUser = userService.getUserById(userId);
				if (loggedInUser != null) {
					userName = loggedInUser.getLastName() + ", "
							+ loggedInUser.getFirstName();
					userId = loggedInUser.getUserId();
				}
			}
		}
		
		LogEvent event = (LogEvent)EntityFactory.createEntityForModel(LogEvent.class);
		event.setEventType(ROUTE_EVENT_TYPE);
		event.setModuleName(MODUL_NAME);
		event.setResourceKey(resourceId.toString());
		event.setResourceName("message");
		event.setUserId(userId);
		event.setUserName(userName);
		
		logEvents.add(event);
		
		logEventParameter(event, "latitude", 1, false, null, lat, EVENT_PARAMETER_ONLY);
		logEventParameter(event, "longitude", 1, false, null, lng, EVENT_PARAMETER_ONLY);
		if(!StringUtils.isInitial(customAddress)){
			logEventParameter(event, "customAddress", 1, false, null, customAddress, EVENT_PARAMETER_ONLY);
		}
		logEventParameter(event, "devicetype", 1, false, null, webUtils.getDeviceType(request), EVENT_PARAMETER_ONLY);
		logEventParameter(event, "useragent", 1, false, null, request.getHeader("User-agent"), EVENT_PARAMETER_ONLY);
		
		logEventUseSameChangeId(logEvents, true);
	}//logRouteSearch
}
