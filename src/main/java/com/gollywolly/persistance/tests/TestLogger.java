package com.gollywolly.persistance.tests;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.gollywolly.gw3utils.persistance.entity.EAVEntity;
import com.gollywolly.gw3utils.pluginloader.ConfigurationHolder;
import com.gollywolly.gw3utils.pluginloader.utils.AppContextHolder;
import com.gollywolly.gw3utils.pluginloader.utils.StringUtils;
import com.gollywolly.persistance.dao.ResourceService;
import com.gollywolly.persistance.dao.UserService;
import com.gollywolly.persistance.model.Resource;
import com.gollywolly.persistance.model.User;
import com.gollywolly.web.controllers.tests.GW3TestBase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="/WebRoot/WEB-INF/context/gw3-algida-context.xml")
@TransactionConfiguration(transactionManager="gw3TxManager")
@Transactional
public class TestLogger extends GW3TestBase {

	private Logger logger = Logger.getLogger(TestLogger.class);
	
/*	private String userName = "2G-1S@gollywolly.com";
	private String userPass = "fagyika";
	
	private Object[] defaultParams = {"appId","GW31-ROCK-EBE3-ALGI"};
*/
	@Autowired
	private ResourceService resourceService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	ConfigurationHolder configHolder;	
	
	private String userName = "2G-1S@gollywolly.com";
	private String userPass = "fagyika";
	
	private Object[] defaultParams = {"appId","GW31-ROCK-EBE3-ALGI"};

	protected ApplicationContext appContext;
	
	@BeforeTransaction
	public void setAppId() {
		AppContextHolder.setAppId("GW31-ROCK-EBE3-ALGI");
	}
	
	@Override
	public String getUserName() {
		return userName;
	}

	@Override
	public String getUserPass() {
		return userPass;
	}

	@Override
	public Object[] getDefaultParams() {
		return defaultParams;
	}

	@Test
	@Rollback(value=false)
	//@Rollback(value=true)
	public void testRunMethod(){
		//testLoggerForUpdate();
		//getAllLogs();
		routeSearchLog();
		//testSendFeedBack();
	}
	
	public void testLoggerForUpdate() {
		
		Resource resource = resourceService.getResourceByResourceId(88832L);
		
		String title = resource.getFirstValue("title");
		String description = resource.getFirstValue("description");
		resource.setFieldValue("title", title + "_test");
		resource.setFieldValue("description", description + "_test");
		resourceService.updateData(resource);
		
		logger.debug(resource);
	}
	
	@SuppressWarnings("unused")
	private void getAllLogs(){
		User user = login();
		
		logger.debug(user == null ? "NULL" : user.toString());
		if (user != null) {
			postForObjects("logger/downloadlogs", 
				//	getParameters(new String[]{"from", "2000-01-01", "to", "2015-05-05"}), false);
			getParameters(new String[]{}), false);
		}
	}
	
	private void routeSearchLog(){
		User user = login();
		
		logger.debug(user == null ? "NULL" : user.toString());
		if (user != null) {
			postForObjects("logger/logroutesearch", 
					getParameters(new String[]{"lats", "47.5031558", "lngs", "19.06068742", "resourceid", "100000", "customAddress", "customAddress_String"}), false);
			
		}
	}
	
	@SuppressWarnings("unused")
	private void testSendFeedBack(){
		User loggedUser  = login();
		
		logger.debug(loggedUser == null ? "NULL" : loggedUser.toString());
		if (loggedUser != null) {
			postForObjects("logger/sendfeedback", 
					getParameters(new String[]{"feedBackEmail", "2G-1S@gollywolly.com",
												"feedBackText", "teszt",
												"lata", "47.503155899999996", 
												"longb", "19.060687400000006"}), false);
		}
	}
	
	

	@SuppressWarnings("unused")
	private void addMetaData(EAVEntity entity, String aspect, String name, Object... value) {
		addMetaDataWithSecurity(entity, aspect, name, null, value);
	}
	
	private void addMetaDataWithSecurity(EAVEntity entity, String aspect, String name, String security, Object... value) {
		String key = aspect + "_" + name;
		for (Object v: value) {
			entity.addFieldValue(key, v, false);
		}
		
		if (!StringUtils.isInitial(security)) 
			entity.addAspectSecurityToken(aspect, security);
	}
	
}
