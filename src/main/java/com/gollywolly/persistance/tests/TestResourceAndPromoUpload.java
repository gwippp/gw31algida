package com.gollywolly.persistance.tests;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.gollywolly.config.ResourceType;
import com.gollywolly.gw3utils.persistance.entity.EAVEntity;
import com.gollywolly.gw3utils.persistance.entity.EntityFactory;
import com.gollywolly.gw3utils.pluginloader.ConfigurationHolder;
import com.gollywolly.gw3utils.pluginloader.config.MetaData;
import com.gollywolly.gw3utils.pluginloader.utils.AppContextHolder;
import com.gollywolly.gw3utils.pluginloader.utils.StringUtils;
import com.gollywolly.persistance.dao.ResourceService;
import com.gollywolly.persistance.dao.UserService;
import com.gollywolly.persistance.model.Resource;
import com.gollywolly.utils.GoogleGeodataUtils;
import com.gollywolly.utils.GoogleGeodataUtils.GeoCodingBehaviour;
import com.gollywolly.utils.uploader.resource.AlgidaResourceUploader;
import com.gollywolly.utils.uploader.resource.ResourceUploader;
import com.gollywolly.utils.uploader.resource.ResourceUploader.ResourceUploadBehaviour;
import com.gollywolly.web.controllers.FileCacheController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="/WebRoot/WEB-INF/context/gw3-algida-context.xml")
@TransactionConfiguration(transactionManager="gw3TxManager")
@Transactional
public class TestResourceAndPromoUpload implements ApplicationContextAware {
	
	private Logger logger = Logger.getLogger(TestResourceAndPromoUpload.class);
	
	@Autowired
	private ResourceService resourceService;
	
	@Autowired
	protected ApplicationContext appContext;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	ConfigurationHolder configHolder;

	
	@Autowired
	private FileCacheController fileCacheController;

	public static String csvUploadFolder = System.getProperty("csv_upload_folder");
	public static String csvImageFolder = System.getProperty("csv_image_folder");
	
	@BeforeTransaction
	public void setAppId() {
		AppContextHolder.setAppId("GW31-ROCK-EBE3-ALGI");
	}
	
	List<String> resourceList = new ArrayList<String>();
	
	@Test
	//@Rollback(true)
	@Rollback(false)
	public void testMainMethod() throws Exception{
		fullFuntion();
	}
	
	@SuppressWarnings("unused")
	private void fullFuntion() throws Exception{
		GeoCodingBehaviour geoCodingBehaviour = new TestGeoCodingBehaviour();
		GoogleGeodataUtils.setGeoCodingBehaviour(geoCodingBehaviour);
		
		ResourceUploadBehaviour uploadBehaviour = new AlgidaUploadBehaviour();
		ResourceUploader.setUploadBehaviour(uploadBehaviour);
		
		resourceList.add("depo");
		resourceList.add("pos");
		resourceList.add("agent");
		
		fileExistCheck();
		
		resourceUpload();
		
		//insertpromo();
		promotionUpload();
		
		//separatedGeoCoding();
		logger.info("------vége--------");
	}
	
	@SuppressWarnings("unused")
	private void separatedGeoCoding() {
		
		Map<Long, String> geodataUpdateResourceId = new  HashMap<Long, String>();
		
	//	addPosWithoutApproved(geodataUpdateResourceId, resourceService);
		addPosOutOfHungary(geodataUpdateResourceId, resourceService);
		
	//	AlgidaResourceUploader.separatedGeoCoding(geodataUpdateResourceId, resourceService);
		
	}



	private void promotionUpload() {
		
		Map<String, String> promotionInnernameFilenameMap = new HashMap<>();
		promotionInnernameFilenameMap.put("scooping", "5_doc_xRADAR_AKC01.txt" );
		promotionInnernameFilenameMap.put("impulsive", "5_doc_xRADAR_AKC03.txt" );
	//	promotionInnernameFilenameMap.put("almond", "5_doc_xRADAR_AKC04.txt" );
	//	promotionInnernameFilenameMap.put("doublechoc", "5_doc_xRADAR_AKC05.txt");
	//	promotionInnernameFilenameMap.put("browniegelato", "5_doc_xRADAR_AKC06.txt");
	//	promotionInnernameFilenameMap.put("chocclassic", "5_doc_xRADAR_AKC07.txt");
		promotionInnernameFilenameMap.put("prom1", "5_doc_xRADAR_AKC08.txt");
	//	promotionInnernameFilenameMap.put("cornetto2weeksago", "5_doc_xRADAR_AKC09.txt");
	//	promotionInnernameFilenameMap.put("th", "5_doc_xRADAR_AKC10.txt");
	//	promotionInnernameFilenameMap.put("ic", "5_doc_xRADAR_AKC11.txt");
		
		
		AlgidaResourceUploader.promotionMapping(promotionInnernameFilenameMap, resourceService, csvUploadFolder);

	}


	private void resourceUpload() {
		
		//Algida to GW31
		AlgidaResourceUploader.mainProcessCSV(resourceList, resourceService, userService, configHolder, appContext, csvUploadFolder, csvImageFolder, fileCacheController);
		
	}


	@SuppressWarnings("unused")
	private void insertpromo() {
		Resource prom1 = (Resource)EntityFactory.createEntityForModel(Resource.class);
	//	Resource prom2 = (Resource)EntityFactory.createEntityForModel(Resource.class);
	//	Resource prom3 = (Resource)EntityFactory.createEntityForModel(Resource.class);
	//	Resource prom4 = (Resource)EntityFactory.createEntityForModel(Resource.class);
		
		prom1.setResourceType("promotion");
	//	prom2.setResourceType("promotion");
	//	prom3.setResourceType("promotion");
	//	prom4.setResourceType("promotion");
		
		//Karton (Akc2015_Cornetto)	Karton (Akc2015_Magnum)	Karton (Akc2015_BigMilk)	Karton (Akc2015_IC)
		prom1.setFieldValue("title", "Karton (Akc2015_Cornetto)");
	//	prom2.setFieldValue("title", "Karton (Akc2015_Magnum)");
	//	prom3.setFieldValue("title", "Karton (Akc2015_BigMilk)");
	//	prom4.setFieldValue("title", "Karton (Akc2015_IC)");
		
		prom1.setFieldValue("promotionbasicdata_innerid", "prom1");
	//	prom2.setFieldValue("promotionbasicdata_innerid", "prom2");
	//	prom3.setFieldValue("promotionbasicdata_innerid", "prom3");
	//	prom4.setFieldValue("promotionbasicdata_innerid", "prom4");
		
		prom1.setFieldValue("promotionbasicdata_startdate", "2015-03-16");
	//	prom2.setFieldValue("promotionbasicdata_startdate", "2015-04-16");
	//	prom3.setFieldValue("promotionbasicdata_startdate", "2015-05-16");
	//	prom4.setFieldValue("promotionbasicdata_startdate", "2015-06-16");
		
		prom1.setFieldValue("promotionbasicdata_enddate", "2015-09-16");
	/*	prom2.setFieldValue("promotionbasicdata_enddate", "2015-10-16");
		prom3.setFieldValue("promotionbasicdata_enddate", "2015-11-16");
		prom4.setFieldValue("promotionbasicdata_enddate", "2015-12-16");
	*/	
		
		
		Calendar c = Calendar.getInstance(); 
		
		prom1.setCreatedOn(c.getTime()); 
		prom1.setLastUpdatedOn(c.getTime());
		prom1.setActive(true);
		
	/*	prom2.setCreatedOn(c.getTime()); 
		prom2.setLastUpdatedOn(c.getTime());
		prom2.setActive(true);
		
		prom3.setCreatedOn(c.getTime()); 
		prom3.setLastUpdatedOn(c.getTime());
		prom3.setActive(true);
		
		prom4.setCreatedOn(c.getTime()); 
		prom4.setLastUpdatedOn(c.getTime());
		prom4.setActive(true);
	*/	
		prom1.setMetaDataFullyLoaded(true);
	/*	prom2.setMetaDataFullyLoaded(true);
		prom3.setMetaDataFullyLoaded(true);
		prom4.setMetaDataFullyLoaded(true);
	*/	
		
		resourceService.insertData(prom1);
	/*	resourceService.insertData(prom2);
		resourceService.insertData(prom3);
		resourceService.insertData(prom4);  */
	}

	@SuppressWarnings("unused")
	private void insertpromo2016() {
		
		Map<String, String> promData = new HashMap<>();
		promData.put("scooping2016", "Karton (Akc2016C_Scooping)");
		promData.put("almond", "Karton (Akc2016I_Magnum_Almond)");
		promData.put("doublechoc", "Karton (Akc2016I_Magnum_DoubleChoc)");
		
		promData.put("browniegelato", "Karton (Akc2016T_BrownieGelato)");
		promData.put("chocclassic", "Karton (Akc2016T_ChocClassic)");
		promData.put("cornetto", "Karton (Akc2016X_Cornetto)");
		
		promData.put("cornetto2weeksago", "Karton (Akc2016X_Cornetto_2hete)");
		promData.put("th", "Karton (Akc2016T_TH)");
		promData.put("ic", "Karton (Akc2016X_IC)");
		
		
		
		
		
		for (Map.Entry<String, String> entry : promData.entrySet()){
			Resource prom = (Resource)EntityFactory.createEntityForModel(Resource.class);
			prom.setResourceType("promotion");
			Calendar c = Calendar.getInstance(); 
			
			prom.setCreatedOn(c.getTime()); 
			prom.setLastUpdatedOn(c.getTime());
			prom.setActive(true);
			
			prom.setFieldValue("title", entry.getValue());
			prom.setFieldValue("promotionbasicdata_innerid", entry.getKey());
			prom.setMetaDataFullyLoaded(true);
			resourceService.insertData(prom);
		}
	}


	
	@Override
	public void setApplicationContext(ApplicationContext appContext)
			throws BeansException {
		this.appContext = appContext;
		
	}
	
	@SuppressWarnings("unused")
	private void addPosWithoutApproved(Map<Long, String> geodataUpdateResourceId, ResourceService resourceService) {
		List<Resource> poss = resourceService.getResourcesByResourceTypeAll("pos");
		
		for (int i = 0; i < poss.size(); i++){
			Resource pos = poss.get(i);
			
			resourceService.loadData(pos);
			
			//if (geodataUpdateResourceId.size() > 4) continue;
			
			if(pos.getFirstValue("posaddressmap_approved") == null){
				geodataUpdateResourceId.put(pos.getResourceId(), "posaddressmap");
				
			}
		}
		
		logger.debug(geodataUpdateResourceId.size());
		
	}
	
	private void addPosOutOfHungary(Map<Long, String> geodataUpdateResourceId, ResourceService resourceService) {
		List<Resource> poss = resourceService.getResourcesByResourceTypeAll("pos");
		
		for (int i = 0; i < poss.size(); i++){
			Resource pos = poss.get(i);
			
			resourceService.loadData(pos);
			
			String lng = pos.getFirstValue("posaddressmap_longitude");
			String lat = pos.getFirstValue("posaddressmap_latitude");
			
			if (lng != null && lat != null){
				Float longitude = Float.parseFloat(lng);
				Float latitude = Float.parseFloat(lat);

				if (!(longitude.floatValue() > 16.0 && longitude.floatValue() < 23.0 &&
						latitude.floatValue() > 45.7 && latitude.floatValue() < 48.65)) {
					geodataUpdateResourceId.put(pos.getResourceId(), "posaddressmap");
				
				}
					
			//	if (geodataUpdateResourceId.size() > 9) return;
				
				//logger.debug(geodataUpdateResourceId.size());
				
			} else {
				//TODO if we need we do this to
				// geodataUpdateResourceId.put(pos.getResourceId(), "posaddressmap");
				if(pos.getFirstValue("posaddressmap_approved") == null){
					geodataUpdateResourceId.put(pos.getResourceId(), "posaddressmap");
					
				}
			}
				
			
		}
		
		logger.debug(geodataUpdateResourceId.size());
		
	}
	
	public static class TestGeoCodingBehaviour extends GeoCodingBehaviour {

		@Override
		public String bestMatchGoogleGeoDataReverseRequest(EAVEntity entity, String mapAspectName, JsonNode jsonNode) {

			double minDistance = 20038;
			double entityLat = Double.parseDouble(entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_LATITUDE));
			double entityLng = Double.parseDouble(entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_LONGITUDE));
			String altAddressByGoogle = null;

			for (int i = 0; i < jsonNode.get("results").size(); i++) {

				JsonNode simpleResult = jsonNode.get("results").get(i);

				Double googleLat = new Double(simpleResult.get("geometry").get("location").get("lat").toString());
				Double googleLng = new Double(simpleResult.get("geometry").get("location").get("lng").toString());

				Double googledistance = calculateDistance(entityLat, entityLng, googleLat, googleLng);

				if (googledistance == null) {
					continue;
				}

				if (minDistance > googledistance) {
					minDistance = googledistance;
					altAddressByGoogle = simpleResult.get("formatted_address").toString();
				}
			}

			return altAddressByGoogle;
		}

		private Double calculateDistance(Double nelat, Double nelng, Double swlat, Double swlng) {
			if (nelat != null && nelng != null && swlat != null && swlng != null) {

				Double earthRadius = new Double(6371); // kilometers
				Double dLat = Math.toRadians(swlat - nelat);
				Double dLng = Math.toRadians(swlng - nelng);
				Double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(nelat)) * Math.cos(Math.toRadians(swlat)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
				Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
				Double distance = earthRadius * c;
				return distance;
			}

			return null;
		}

		@Override
		public Map<String, String> bestMatchGoogleGeoDataRequest(EAVEntity entity, String mapAspectName, boolean onlyZipCode, JsonNode jsonNode, Object object) {
			Map<String, String> googleLatLng = new HashMap<String, String>();

			double maxPoint = 0;
			String lat = null;
			String lng = null;

			for (int i = 0; i < jsonNode.get("results").size(); i++) {

				JsonNode simpleResult = jsonNode.get("results").get(i);

				String streetnumber = null;
				String route = null;
				String locality = null;
				String province = null;
				String postalcode = null;
				String country = null;

				for (int j = 0; j < simpleResult.get("address_components").size(); j++) {

					JsonNode addressComponent = simpleResult.get("address_components").get(j);

					String aCType = addressComponent.get("types").get(0).toString().replace("\"", "");

					if (aCType.equals("street_number")) {
						streetnumber = addressComponent.get("long_name").toString().replace("\"", "");
					} else if ("route".equals(aCType)) {
						route = addressComponent.get("long_name").toString().replace("\"", "");
					} else if ("locality".equals(aCType)) {
						locality = addressComponent.get("long_name").toString().replace("\"", "");
					} else if ("administrative_area_level_1".equals(aCType)) {
						province = addressComponent.get("long_name").toString().replace("\"", "");
					} else if ("postal_code".equals(aCType)) {
						postalcode = addressComponent.get("long_name").toString().replace("\"", "");
					} else if ("country".equals(aCType)) {
						country = addressComponent.get("short_name").toString().replace("\"", "");
					}
				}

				// Street1
				// TODO maybe replace fuzzy to equal
				double point = 0;
				String entityStreet1 = entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_STREET1);
				String googleStreet1 = streetnumber + " " + route;
				String googleReverseStreet1 = route + " " + streetnumber;
				// TODO maybe we need streetnumber and route both
				if (entityStreet1 != null && !(streetnumber == null && route == null)) {
					int distance1 = StringUtils.editDistance(entityStreet1.toLowerCase(), googleStreet1.toLowerCase(), entityStreet1.length(), googleStreet1.length());
					int distance2 = StringUtils
							.editDistance(entityStreet1.toLowerCase(), googleReverseStreet1.toLowerCase(), entityStreet1.length(), googleReverseStreet1.length());

					double dist = 1 - ((double) Math.min(distance1, distance2) / (double) Math.min(entityStreet1.length(), googleStreet1.length()));

					if (dist > 0) { 
						point = dist;
					}
				}

				// Settlement
				String entityCity = entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_CITY);
				if (entityCity != null && locality != null) {
					if (locality.equalsIgnoreCase(entityCity)) {
						point = point + 13;
					}
				}

				// State
				// TODO all state in US and Canada has 2-char name
				String entityState = entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_STATE);
				if (entityState != null && province != null) {
					if (province.equalsIgnoreCase(entityState)) {
						point = point + 13 * 13;
					}
				}

				// Zipcode
				String entityZipCode = entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_ZIPCODE);
				if (entityZipCode != null && postalcode != null) {
					if (postalcode.equalsIgnoreCase(entityZipCode)) {
						point = point + 13 * 13 * 13;
					}
				}
				// Country
				String entityCountry = entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_COUNTRY);
				if (entityCountry != null && country != null) {
					if (country.equalsIgnoreCase(entityCountry)) {
						point = point + 13 * 13 * 13 * 13;
					}
				}

				if (point > maxPoint) {
					maxPoint = point;
					lat = jsonNode.get("results").get(0).get("geometry").get("location").get("lat").toString();
					lng = jsonNode.get("results").get(0).get("geometry").get("location").get("lng").toString();
				}
			}
			
			googleLatLng.put("lat", lat);
			googleLatLng.put("lng", lng);
			
			return googleLatLng;
		}
		
		@Override
		public String whichBehavior(){
			return "algida";
		}
	}
	
	private static class AlgidaUploadBehaviour implements ResourceUploadBehaviour {

		@Override
		public String getFileEncoding() {
			return "ISO8859_2";
		}

		@Override
		public Map<String, String> getColumnTitleMapping(String fileName) {
			LinkedHashMap<String, String> columnTitleMap = new LinkedHashMap<String, String>();
			columnTitleMap.put("Csatorna", "code");
			columnTitleMap.put("Csatorna Szint1", "displayName");
			
			return columnTitleMap;
		}

		@Override
		public boolean hasCategoryNameColumn(String fileName) {
			return false;
		}

	}
	
	private void fileExistCheck() throws Exception{
		boolean fileExistError = false;
		
		Map<String, List<MetaData>> categoryFieldMap = new HashMap<String, List<MetaData>>();
		
		for (String resourceTypeName : resourceList) {
			logger.debug("Main check: resourceName:  " + resourceTypeName);
			
			ResourceType resourceType = null;
			resourceType = (ResourceType) configHolder.getQualifiedConfiguration(ResourceType.RESOURCE_CONFIGURATION_TYPE, AppContextHolder.getAppId(), resourceTypeName);
			String fileName = resourceType.getFileName();
			
			String file = csvUploadFolder + fileName + ".csv";
			File f = new File(file);
			
			if (!f.exists()){
				logger.error(file + " not exists!!!");
				fileExistError = true;
			}
			
			ResourceUploader.getColumnTitleMetaDataMap(resourceType, categoryFieldMap);
		}
		
		for (Iterator<Map.Entry<String, List<MetaData>>> iter = categoryFieldMap.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<String, List<MetaData>> entry = iter.next();
			
			String fileName = entry.getKey();
			String absoluteFileName = csvUploadFolder + fileName;
			
			File f = new File(absoluteFileName);
			
			if (!f.exists()){
				logger.error(absoluteFileName + " not exists!!!");
				fileExistError = true;
			}
		}
		
	/*	String promotionMapperFileName = "RADAR_AKCIO_V01.csv";
		
		String fileName = csvUploadFolder + promotionMapperFileName;
		File f = new File(fileName);
		
		if (!f.exists()){
			logger.error(fileName + " not exists!!!");
			fileExistError = true;
		}
*/
		if (fileExistError){
			logger.error("Some file not exist!");
			throw new Exception();
			
		} 

	}
}
