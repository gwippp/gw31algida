package com.gollywolly.persistance.tests;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.gollywolly.gw3utils.persistance.entity.EAVEntity;
import com.gollywolly.gw3utils.pluginloader.ConfigurationHolder;
import com.gollywolly.gw3utils.pluginloader.utils.AppContextHolder;
import com.gollywolly.gw3utils.pluginloader.utils.StringUtils;
import com.gollywolly.persistance.dao.ResourceService;
import com.gollywolly.persistance.dao.UserService;
import com.gollywolly.persistance.model.Resource;
import com.gollywolly.utils.GoogleGeodataUtils;
import com.gollywolly.utils.GoogleGeodataUtils.GeoCodingBehaviour;
import com.gollywolly.utils.uploader.resource.GWUploadErrorContainer;
import com.gollywolly.web.controllers.tests.GW3TestBase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/WebRoot/WEB-INF/context/gw3-algida-context.xml")
@TransactionConfiguration(transactionManager = "gw3TxManager")
@Transactional
public class TestReverseGeoCoding extends GW3TestBase {

	private Logger logger = Logger.getLogger(TestReverseGeoCoding.class);

	@Autowired
	private ResourceService resourceService;

	@Autowired
	private UserService userService;

	@Autowired
	ConfigurationHolder configHolder;

	private String userName = "2G-1S@gollywolly.com";
	private String userPass = "fagyika";

	private Object[] defaultParams = { "appId", "GW31-ROCK-EBE3-ALGI" };

	protected ApplicationContext appContext;
	
	public static String googleAPIKey = System.getProperty("google_api_key");

	@BeforeTransaction
	public void setAppId() {
		AppContextHolder.setAppId("GW31-ROCK-EBE3-ALGI");
	}

	@Override
	public String getUserName() {
		return userName;
	}

	@Override
	public String getUserPass() {
		return userPass;
	}

	@Override
	public Object[] getDefaultParams() {
		return defaultParams;
	}

	@Test
	//@Rollback(value = true)
	@Rollback(value=false)
	public void testRunMethod() {
		
		GeoCodingBehaviour beh = new TestGeoCodingBehaviour();

		GoogleGeodataUtils.setGeoCodingBehaviour(beh);
		
		//testReverseGeoCoding();
		//testMultipleReverseGeocoding();
		int limit = 2000;
		multipleReverseGeocodingForNotReverseGeoCodedPOS(limit);
		
	}

	private void multipleReverseGeocodingForNotReverseGeoCodedPOS(int limit) {
		
		logger.debug(googleAPIKey);

		GWUploadErrorContainer uploadErrorContainer = new  GWUploadErrorContainer();
		
		Map<Long, String> entityMapWithAspectName = new HashMap<Long, String>();
		 
		listOf2500NotReverseGeoCodedPOS(entityMapWithAspectName, limit); 
		 
		
		GoogleGeodataUtils.startGoogleAddressRequest(entityMapWithAspectName, resourceService, uploadErrorContainer, googleAPIKey);
	}

	private void listOf2500NotReverseGeoCodedPOS(Map<Long, String> entityMapWithAspectName, int limit) {
		List<Resource> poss = resourceService.getResourcesByResourceTypeAll("pos");
		
		for (int i = 0; i < poss.size(); i++){
			Resource pos = poss.get(i);
			
			resourceService.loadData(pos);
			String altaddressbygoogle = pos.getFirstValue("posaddressmap_altaddressbygoogle");
			
			if (StringUtils.isInitial(altaddressbygoogle)){
				entityMapWithAspectName.put(pos.getResourceId(), "posaddressmap");
		//		logger.debug(pos.getResourceId());
			}
			
			if (entityMapWithAspectName.size() > (limit-1)) break;
		}
		
		logger.debug(entityMapWithAspectName.size());
	}

	@SuppressWarnings("unused")
	private void testMultipleReverseGeocoding() {
		
		Map<Long, String> entityMapWithAspectName = new HashMap<Long, String>();
		
		 GWUploadErrorContainer uploadErrorContainer = new  GWUploadErrorContainer();
		
		entityMapWithAspectName.put(27918L, "posaddressmap");
		
		entityMapWithAspectName.put(27916L, "posaddressmap");
		
	/*	entityMapWithAspectName.put(8271L, "posaddressmap");
		entityMapWithAspectName.put(8272L, "posaddressmap");
		entityMapWithAspectName.put(8273L, "posaddressmap");
		entityMapWithAspectName.put(8274L, "posaddressmap");
		entityMapWithAspectName.put(8275L, "posaddressmap");
		entityMapWithAspectName.put(8276L, "posaddressmap");
		entityMapWithAspectName.put(8277L, "posaddressmap");
		entityMapWithAspectName.put(8278L, "posaddressmap");
		*/
		GoogleGeodataUtils.startGoogleAddressRequest(entityMapWithAspectName, resourceService, uploadErrorContainer);
		
		
	}

	@SuppressWarnings("unused")
	private void testReverseGeoCoding() {

		// Resource resource = resourceService.getResourceByResourceId(88950L);
		Resource resource = resourceService.getResourceByResourceId(99011L);

		// AlgidaGoogleGeodataUtils.proccessGoogleAddressRequest(resource,
		// "posaddressmap");
		GoogleGeodataUtils.proccessGoogleAddressRequest(resource, "posaddressmap", googleAPIKey);
	}

	@SuppressWarnings("unused")
	private void addMetaData(EAVEntity entity, String aspect, String name, Object... value) {
		addMetaDataWithSecurity(entity, aspect, name, null, value);
	}

	private void addMetaDataWithSecurity(EAVEntity entity, String aspect, String name, String security, Object... value) {
		String key = aspect + "_" + name;
		for (Object v : value) {
			entity.addFieldValue(key, v, false);
		}

		if (!StringUtils.isInitial(security))
			entity.addAspectSecurityToken(aspect, security);
	}

	public static class TestGeoCodingBehaviour extends GeoCodingBehaviour {

		@Override
		public String bestMatchGoogleGeoDataReverseRequest(EAVEntity entity, String mapAspectName, JsonNode jsonNode) {

			double minDistance = 20038;
			double entityLat = Double.parseDouble(entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_LATITUDE));
			double entityLng = Double.parseDouble(entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_LONGITUDE));
			String altAddressByGoogle = null;

			for (int i = 0; i < jsonNode.get("results").size(); i++) {

				JsonNode simpleResult = jsonNode.get("results").get(i);

				Double googleLat = new Double(simpleResult.get("geometry").get("location").get("lat").toString());
				Double googleLng = new Double(simpleResult.get("geometry").get("location").get("lng").toString());

				Double googledistance = calculateDistance(entityLat, entityLng, googleLat, googleLng);

				if (googledistance == null) {
					continue;
				}

				if (minDistance > googledistance) {
					minDistance = googledistance;
					altAddressByGoogle = simpleResult.get("formatted_address").toString();
				}
			}

			return altAddressByGoogle;
		}

		private Double calculateDistance(Double nelat, Double nelng, Double swlat, Double swlng) {
			if (nelat != null && nelng != null && swlat != null && swlng != null) {

				Double earthRadius = new Double(6371); // kilometers
				Double dLat = Math.toRadians(swlat - nelat);
				Double dLng = Math.toRadians(swlng - nelng);
				Double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(nelat)) * Math.cos(Math.toRadians(swlat)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
				Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
				Double distance = earthRadius * c;
				return distance;
			}

			return null;
		}

		@Override
		public Map<String, String> bestMatchGoogleGeoDataRequest(EAVEntity entity, String mapAspectName, boolean onlyZipCode, JsonNode jsonNode, Object object) {
			Map<String, String> googleLatLng = new HashMap<String, String>();

			double maxPoint = 0;
			String lat = null;
			String lng = null;

			for (int i = 0; i < jsonNode.get("results").size(); i++) {

				JsonNode simpleResult = jsonNode.get("results").get(i);

				String streetnumber = null;
				String route = null;
				String locality = null;
				String province = null;
				String postalcode = null;
				String country = null;

				for (int j = 0; j < simpleResult.get("address_components").size(); j++) {

					JsonNode addressComponent = simpleResult.get("address_components").get(j);

					String aCType = addressComponent.get("types").get(0).toString().replace("\"", "");

					if (aCType.equals("street_number")) {
						streetnumber = addressComponent.get("long_name").toString().replace("\"", "");
					} else if ("route".equals(aCType)) {
						route = addressComponent.get("long_name").toString().replace("\"", "");
					} else if ("locality".equals(aCType)) {
						locality = addressComponent.get("long_name").toString().replace("\"", "");
					} else if ("administrative_area_level_1".equals(aCType)) {
						province = addressComponent.get("long_name").toString().replace("\"", "");
					} else if ("postal_code".equals(aCType)) {
						postalcode = addressComponent.get("long_name").toString().replace("\"", "");
					} else if ("country".equals(aCType)) {
						country = addressComponent.get("short_name").toString().replace("\"", "");
					}
				}

				// Street1
				// TODO maybe replace fuzzy to equal
				double point = 0;
				String entityStreet1 = entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_STREET1);
				String googleStreet1 = streetnumber + " " + route;
				String googleReverseStreet1 = route + " " + streetnumber;
				// TODO maybe we need streetnumber and route both
				if (entityStreet1 != null && !(streetnumber == null && route == null)) {
					int distance1 = StringUtils.editDistance(entityStreet1.toLowerCase(), googleStreet1.toLowerCase(), entityStreet1.length(), googleStreet1.length());
					int distance2 = StringUtils
							.editDistance(entityStreet1.toLowerCase(), googleReverseStreet1.toLowerCase(), entityStreet1.length(), googleReverseStreet1.length());

					double dist = 1 - ((double) Math.min(distance1, distance2) / (double) Math.min(entityStreet1.length(), googleStreet1.length()));

					if (dist > 0) { 
						point = dist;
					}
				}

				// Settlement
				String entityCity = entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_CITY);
				if (entityCity != null && locality != null) {
					if (locality.equalsIgnoreCase(entityCity)) {
						point = point + 13;
					}
				}

				// State
				// TODO all state in US and Canada has 2-char name
				String entityState = entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_STATE);
				if (entityState != null && province != null) {
					if (province.equalsIgnoreCase(entityState)) {
						point = point + 13 * 13;
					}
				}

				// Zipcode
				String entityZipCode = entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_ZIPCODE);
				if (entityZipCode != null && postalcode != null) {
					if (postalcode.equalsIgnoreCase(entityZipCode)) {
						point = point + 13 * 13 * 13;
					}
				}
				// Country
				String entityCountry = entity.getFirstValue(mapAspectName + "_" + ADDRESSMAP_METADATA_COUNTRY);
				if (entityCountry != null && country != null) {
					if (country.equalsIgnoreCase(entityCountry)) {
						point = point + 13 * 13 * 13 * 13;
					}
				}

				if (point > maxPoint) {
					maxPoint = point;
					lat = jsonNode.get("results").get(0).get("geometry").get("location").get("lat").toString();
					lng = jsonNode.get("results").get(0).get("geometry").get("location").get("lng").toString();
				}
			}
			
			googleLatLng.put("lat", lat);
			googleLatLng.put("lng", lng);
			
			return googleLatLng;
		}
	}

}
