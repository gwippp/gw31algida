package com.gollywolly.web.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gollywolly.daemons.notifications.GWEmbeddedNotificationSenderEngine;
import com.gollywolly.gw3utils.logger.dao.LogService;
import com.gollywolly.gw3utils.logger.model.LogEventParameter;
import com.gollywolly.gw3utils.persistance.entity.EntityFactory;
import com.gollywolly.gw3utils.persistance.entity.IEntity;
import com.gollywolly.gw3utils.pluginloader.ConfigurationHolder;
import com.gollywolly.gw3utils.pluginloader.utils.StringUtils;
import com.gollywolly.persistance.dao.AlgidaResourceService;
import com.gollywolly.persistance.dao.MessageService;
import com.gollywolly.persistance.dao.ResourceService;
import com.gollywolly.persistance.dao.UserService;
import com.gollywolly.persistance.model.Message;
import com.gollywolly.persistance.model.MessageNotifData;
import com.gollywolly.persistance.model.Resource;
import com.gollywolly.persistance.model.User;

import au.com.bytecode.opencsv.CSVWriter;

@Controller
@RequestMapping(value = "/logger")
public class LoggerController extends ControllerBase {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ResourceService resourceService;
	
	@Autowired
	private AlgidaResourceService algidaResourceService;
	
	@Autowired(required=false)
	private MessageService messageService;
	
	@Autowired 
	private LogService loggerService;
	
	@Autowired
	ConfigurationHolder configHolder;
	
	SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 

	public static String csvUploadFolder = "/Users/gwhungary/Work/";
	
	public static String LOG_ID = "logid";
	public static String EVENT_TYPE = "eventtype";
	public static String RESOURCE_NAME = "resourcename";
	
	public static String LOG_DETAILS_PARAM_COUNT = "paramcount";
	public static String LOG_DETAILS_PARAM_NAME = "paramname";
	public static String LOG_DETAILS_CHANGE_TYPE = "changetype";
	public static String LOG_OLD_LATITUDE = "Old Latitude";
	public static String LOG_NEW_LATITUDE = "Latidude";
	public static String LOG_OLD_LONGITUDE = "Old longitude";
	public static String LOG_NEW_LONGITUDE = "Longitude";
	public static String LOG_OLD_HEADING = "Old heading";
	public static String LOG_NEW_HEADING = "Heading";
	public static String LOG_OLD_PITCH = "Old pitch";
	public static String LOG_NEW_PITCH = "Pitch";
	public static String LOG_OLD_ADDRESS = "Old address";
	public static String LOG_NEW_ADDRESS = "Address";
	public static String LOG_OLD_APPROVED = "Old status";
	public static String LOG_NEW_APPROVED = "Status";
	public static String LOG_DISTANCE = "Distance";
	public static String LOG_POS_INNER_ID = "Inner id";
	public static String LOG_USER_INNER_ID = "User id";
	public static String LOG_TIMESTAMP = "Timestamp";
	public static String LOG_POS_NAME = "POS name";
	
	public static String LOG_OLD_TITLE = "Old title";
	public static String LOG_NEW_TITLE = "Title";
	public static String LOG_OLD_DESCRIPTION = "Old description";
	public static String LOG_NEW_DESCRIPTION = "Description";
	
	public static String LOG_GPS_COORDINATES = "GPS Coordinates";
	public static String LOG_EMAIL = "Email";
	
	public static String LOG_UPDATES_TITLE = "Updates";
	public static String LOG_SEARCH_TITLE = "Searches";
	public static String LOG_DIRECTION_TITLE = "Asking directions";
	public static String LOG_FEEDBACK_TITLE = "Sent feedback";
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@Override
	public void afterPropertiesSet() throws Exception {
		super.afterPropertiesSet();
		//ResourceValidator resourceValidator = new ResourceValidator();
		//addValidator(resourceValidator);
	}
	
	@RequestMapping(value="/logroutesearch")
	public void logRouteSearch(
			@RequestParam(required=false, value="lats") String lats,
			@RequestParam(required=false, value="lngs") String lngs,
			@RequestParam(required=false, value="resourceid") String resourceId,
			@RequestParam(required=false, value="customAddress") String customAddress,
			HttpServletRequest request,
			HttpServletResponse response
			){
		
			algidaResourceService.routeSearch(lats, lngs, resourceId, customAddress);
			
			try {
				handleSuccessRepsonse(request, response, (IEntity) null, null, null);
			} catch (Exception e1) {
				e1.printStackTrace();
				handleFailureRepsonse(request, response, e1);
			}
	}
	
	@RequestMapping(value="/sendfeedback")
	public void sendFeedBack(
			@RequestParam(required=false, value="feedBackText") String feedBackText, 
			@RequestParam(required=false, value="feedBackEmail") String feedBackEmail,
			@RequestParam(required=false, value="address") String address,
			@RequestParam(required=false, value="lata") String lata,
			@RequestParam(required=false, value="longb") String longb,
			HttpServletRequest request,
			HttpServletResponse response
			){
    	
    	User user = getLoggedinUser(request);
    	
    	if(address == null){
			address = "nincs megadva";
		}
		
    	String feedBackEmailString = feedBackEmail;
		if(feedBackEmail == null || (feedBackEmail != null && feedBackEmail.trim() == "")){
			feedBackEmail = "nincs megadva";
			feedBackEmailString = "nincs megadva – emiatt nem tudunk rá válaszolni";
		}
		
		String gpsCoordinates = "";
		if(lata == null && longb == null){
			gpsCoordinates = "nincs megadva";
		}else{
			gpsCoordinates = lata + ", " + longb;
		}
    	
		Message message = (Message)EntityFactory.createEntityForModel(Message.class);
		message.setMessageType(GWEmbeddedNotificationSenderEngine.MESSAGE_TYPE_EMAIL);
		Date processingDate = new Date();
		message.setProcessingDate(processingDate);
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		message.setSubject("Algida Radar visszajelzés érkezett – email cím: " + feedBackEmail);
		
		Locale locale = LocaleContextHolder.getLocale();
		
		String messageText = messageSource.getMessage("mail.sender", null, locale) + ": " + feedBackEmailString + "\n";
		messageText += messageSource.getMessage("mail.time", null, locale) + ": " + df.format(processingDate) + "\n";
		messageText += "Küldéskor az Algida Radar-on mutatott GPS koordináta: ";
		messageText += gpsCoordinates + "\n";
		messageText += "Cím: " + address + "\n";
		messageText += "Üzenet szövege: \n";
		messageText += feedBackText;
		
		message.setMessageText(messageText);
		
		MessageNotifData mndRec1 = (MessageNotifData)EntityFactory.createEntityForModel(MessageNotifData.class);
		mndRec1.setRecipient("loveirobert@gmail.com");
		MessageNotifData mndRec2 = (MessageNotifData)EntityFactory.createEntityForModel(MessageNotifData.class);
		mndRec2.setRecipient("gfuresz@gollywolly.com");
		
		MessageNotifData mndRec3 = (MessageNotifData)EntityFactory.createEntityForModel(MessageNotifData.class);
		mndRec3.setRecipient("zsolt@gollywolly.com");
		
		MessageNotifData mndRec4 = (MessageNotifData)EntityFactory.createEntityForModel(MessageNotifData.class);
		mndRec4.setRecipient("Regina.Homoki@unilever.com");
		/*MessageNotifData mndRec1 = (MessageNotifData)EntityFactory.createEntityForModel(MessageNotifData.class);
		mndRec1.setRecipient("loveirobert@gmail.com");
		MessageNotifData mndRec2 = (MessageNotifData)EntityFactory.createEntityForModel(MessageNotifData.class);
		mndRec2.setRecipient("tamas.sonkoly@unilever.com");
		
		MessageNotifData mndRec3 = (MessageNotifData)EntityFactory.createEntityForModel(MessageNotifData.class);
		mndRec3.setRecipient("zsolte@gmail.com");
		*/
		//message.addMessageNotifData(mnd);
		message.addMessageNotifData(mndRec4);
		message.addMessageNotifData(mndRec3);
		
		message.addMessageNotifData(mndRec1);
		message.addMessageNotifData(mndRec2);
		
		messageService.insertMessage(message, user);
		
		Long messageId = message.getIdFieldAsLong();
		
		algidaResourceService.logFeedBack(feedBackText, feedBackEmailString, address, lata, longb, messageId);
		
    	try {
			handleSuccessRepsonse(request, response, (IEntity) null, null, null);
		} catch (Exception e1) {
			e1.printStackTrace();
			handleFailureRepsonse(request, response, e1);
		}
    	
    }
	
	@RequestMapping(value = "/downloadlogs")
	public void processDownloadLogs(
			@RequestParam(required=false, value="from") String fromDate,
			@RequestParam(required=false, value="to") String toDate,
			HttpServletRequest request, HttpServletResponse response) {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try {
			
			if(fromDate == null)
				fromDate = "2000-01-01";
			if(toDate == null)
				toDate = "2100-01-01";
			
			Date from = dateFormat.parse(fromDate);
			Date to = dateFormat.parse(toDate);

			Calendar fromCalendar = Calendar.getInstance();
			fromCalendar.setTime(from);
			
			Calendar toCalendar = Calendar.getInstance();
			toCalendar.setTime(to);
			toCalendar.add(Calendar.HOUR, 23);
			toCalendar.add(Calendar.MINUTE, 59);
			toCalendar.add(Calendar.SECOND, 59);
			
			logger.debug("fromDate: " +  fromCalendar.getTime());
			logger.debug("toDate: " + toCalendar.getTime());
			
			Date currentDate = new Date();
	        Long currentTimeStampNumber = currentDate.getTime();
	        String currentTimeStamp = Long.toString(currentTimeStampNumber);
			String generatedFileName = "logs_" + currentTimeStamp + ".csv";
		
			try {
				StringWriter writer = new StringWriter();
		        CSVWriter csvWriter = new CSVWriter(new OutputStreamWriter(baos), '\t');
		        List<String[]> csvdata  = toStringArrayLog(fromCalendar.getTime(), toCalendar.getTime());
		        csvWriter.writeAll(csvdata);
		        csvWriter.close();
		        System.out.println(writer);
			} catch (IOException e) {
				generatedFileName = "";
				e.printStackTrace();
			}
			
			 if (baos.toByteArray() != null) {
				    response.setHeader("Cache-Control", "max-age=0");
				    response.setDateHeader("Expires", System.currentTimeMillis());
				    response.setHeader("Content-Length", Integer.toString(baos.toByteArray().length));
				    //response.setDateHeader("Expires", 0);
				    response.setContentType("text/csv");
				    if(!StringUtils.isInitial(generatedFileName)){
				    	response.setContentType("text/csv ");
				    	String fileName = generatedFileName;
				    
				    	fileName = fileName.replaceAll(" ", "_");
				    	response.setHeader("Content-Disposition","attachment;filename="+fileName);
				    }
				    ServletOutputStream responseOutputStream;
					try {
						responseOutputStream = response.getOutputStream();
				        responseOutputStream.write(baos.toByteArray());
				        responseOutputStream.flush();
				        responseOutputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			 }
		} catch (Exception e) {
			e.printStackTrace();
		}

    	
	}

	private List<String[]> toStringArrayLog(Date from, Date to) {
		List<String[]> records = new ArrayList<String[]>();
		
		String[] emptyRow = new String[] { "" };

		// Update
		records.add(new String[] { LOG_UPDATES_TITLE });
		records.add(emptyRow);
		records.add(new String[] { LOG_USER_INNER_ID, LOG_TIMESTAMP, LOG_POS_INNER_ID, LOG_OLD_TITLE, LOG_NEW_TITLE, LOG_OLD_DESCRIPTION, LOG_NEW_DESCRIPTION, LOG_OLD_HEADING,
				LOG_NEW_HEADING, LOG_OLD_PITCH, LOG_NEW_PITCH, LOG_OLD_LATITUDE, LOG_NEW_LATITUDE, LOG_OLD_LONGITUDE, LOG_NEW_LONGITUDE, LOG_OLD_ADDRESS, LOG_NEW_ADDRESS,
				LOG_OLD_APPROVED, LOG_NEW_APPROVED });
		records.addAll(updateLogs(from, to));
		records.add(emptyRow);
		records.add(emptyRow);
		records.add(emptyRow);

		// Search
		records.add(new String[] { LOG_SEARCH_TITLE });
		records.add(emptyRow);
		records.add(new String[] { LOG_TIMESTAMP, LOG_NEW_LATITUDE, LOG_NEW_LONGITUDE, LOG_DISTANCE });
		records.addAll(searchLogs(from, to));
		records.add(emptyRow);
		records.add(emptyRow);
		records.add(emptyRow);

		// Route
		records.add(new String[] { LOG_DISTANCE });
		records.add(emptyRow);
		records.add(new String[] { LOG_TIMESTAMP, LOG_POS_INNER_ID, LOG_POS_NAME, LOG_NEW_LATITUDE, LOG_NEW_LONGITUDE });
		records.addAll(routeLogs(from, to));
		records.add(emptyRow);
		records.add(emptyRow);
		records.add(emptyRow);

		// Feedback
		records.add(new String[] { LOG_FEEDBACK_TITLE });
		records.add(emptyRow);
		records.add(new String[] { LOG_TIMESTAMP, LOG_EMAIL, LOG_NEW_LATITUDE, LOG_NEW_LONGITUDE });
		records.addAll(feedbackLogs(from, to));

		return records;
		
	}
	
	private List<String[]> updateLogs(Date from, Date to) {
		final List<String[]> updateRecords = new ArrayList<String[]>();
		
		loggerService.csvFileWriteLogs("update", from, to, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				Long logId = rs.getLong("logid"); 
				Long userId = rs.getLong("userid");
				Long resourceKey = rs.getLong("resourcekey");
				String createdOn = rs.getString("createdon");
				String formattedDate = formatAndAdd5HoursToDate(createdOn);

				Map<String, LogEventParameter> detailMap = new HashMap<String, LogEventParameter>();
	        	
				List<LogEventParameter> details = loggerService.getLogDetailsRecords(logId);
	            if(details != null && details.size()>0){
	            	for(LogEventParameter detail : details){
	            		detailMap.put(detail.getParamName(), detail);
	            	}
	            }
				
	            List<String> detailColumnValues = new ArrayList<String>();
	            boolean writeIntoFile = false;
	            
	            if(userId != null && userId != 0){
	            	User user = userService.getUserById(userId);
	            	if(user != null){
	            		String userName =user.getUserName();
	            		detailColumnValues.add(userName);
	            	} else {
	            		detailColumnValues.add("");
	            	}
	            }else
					detailColumnValues.add("");
	            
				detailColumnValues.add(formattedDate);
				
					
					if(resourceKey != null && resourceKey != 0){
						Resource resource = resourceService.getResourceByResourceId(resourceKey);
						detailColumnValues.add(resource.getFirstValue(resource.getResourceInnerIdFieldName(configHolder)));
					}else
						detailColumnValues.add("");
					
					if(detailMap.containsKey("title")){
						writeIntoFile = true;
		            	detailColumnValues.add(detailMap.get("title").getValue());
		            	detailColumnValues.add(detailMap.get("title").getNewValue());
		            }else{
		            	detailColumnValues.add("");detailColumnValues.add("");
		            }
					if(detailMap.containsKey("description")){
						writeIntoFile = true;
		            	detailColumnValues.add(detailMap.get("description").getValue());
		            	detailColumnValues.add(detailMap.get("description").getNewValue());
		            }else{
		            	detailColumnValues.add("");detailColumnValues.add("");
		            }
					if(detailMap.containsKey("posaddressmap_heading")){
						writeIntoFile = true;
		            	detailColumnValues.add(detailMap.get("posaddressmap_heading").getValue());
		            	detailColumnValues.add(detailMap.get("posaddressmap_heading").getNewValue());
		            }else{
		            	detailColumnValues.add("");detailColumnValues.add("");
		            }
		            if(detailMap.containsKey("posaddressmap_pitch")){
		            	writeIntoFile = true;
		            	detailColumnValues.add(detailMap.get("posaddressmap_pitch").getValue());
		            	detailColumnValues.add(detailMap.get("posaddressmap_pitch").getNewValue());
		            }else{
		            	detailColumnValues.add("");detailColumnValues.add("");
		            }
		            if(detailMap.containsKey("posaddressmap_latitude")){
		            	writeIntoFile = true;
		            	detailColumnValues.add(detailMap.get("posaddressmap_latitude").getValue());
		            	detailColumnValues.add(detailMap.get("posaddressmap_latitude").getNewValue());
		            }else{
		            	detailColumnValues.add("");detailColumnValues.add("");
		            }
		            if(detailMap.containsKey("posaddressmap_longitude")){
		            	writeIntoFile = true;
		            	detailColumnValues.add(detailMap.get("posaddressmap_longitude").getValue());
		            	detailColumnValues.add(detailMap.get("posaddressmap_longitude").getNewValue());
		            }else{
		            	detailColumnValues.add("");detailColumnValues.add("");
		            }
		            if(detailMap.containsKey("posalternativeaddressmap_altaddress")){
		            	writeIntoFile = true;
		            	detailColumnValues.add(detailMap.get("posalternativeaddressmap_altaddress").getValue());
		            	detailColumnValues.add(detailMap.get("posalternativeaddressmap_altaddress").getNewValue());
		        	}else{
		            	detailColumnValues.add("");detailColumnValues.add("");
		            }
		        	if(detailMap.containsKey("posaddressmap_approved")){
		        		writeIntoFile = true;
		            	detailColumnValues.add(detailMap.get("posaddressmap_approved").getValue());
		            	detailColumnValues.add(detailMap.get("posaddressmap_approved").getNewValue());
		        	}else{
		            	detailColumnValues.add("");detailColumnValues.add("");
		            }
				
	        	String[] nextLine = detailColumnValues.toArray(new String[detailColumnValues.size()]);

	        	if(writeIntoFile){
	        		updateRecords.add(nextLine);
	        	}
			}
		});
		return updateRecords;

	}
	
	private List<String[]> searchLogs(Date from, Date to) {
		final List<String[]> searchRecords = new ArrayList<String[]>();
		loggerService.csvFileWriteLogs("search", from, to, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				Long logId = rs.getLong("logid"); 
				
				String createdOn = rs.getString("createdon");
				String formattedDate = formatAndAdd5HoursToDate(createdOn);
				
				Map<String, LogEventParameter> detailMap = new HashMap<String, LogEventParameter>();
	        	
				List<LogEventParameter> details = loggerService.getLogDetailsRecords(logId);
	            if(details != null && details.size()>0){
	            	for(LogEventParameter detail : details){
	            		detailMap.put(detail.getParamName(), detail);
	            	}
	            }
				
	            List<String> detailColumnValues = new ArrayList<String>();
	            
				detailColumnValues.add(formattedDate);
				
				DecimalFormat decimalFormat = new DecimalFormat();
				Double nelat = null; Double nelng = null;
				Double swlat = null; Double swlng = null;
				
				if (detailMap.containsKey("latitude")) {
					detailColumnValues.add(detailMap.get("latitude").getNewValue());
				} else if (detailMap.containsKey("nelat") && detailMap.containsKey("swlat")) {
					try {
						nelat = (Double) decimalFormat.parse(detailMap.get("nelat").getNewValue());
						swlat = (Double) decimalFormat.parse(detailMap.get("swlat").getNewValue());

						Double sum = nelat + swlat;
						Double lat = sum / 2;

						String latitude = lat.toString();
						detailColumnValues.add(latitude);

					} catch (ParseException e) {
						detailColumnValues.add("");
					}

				} else {
					detailColumnValues.add("");
				}
				
				if (detailMap.containsKey("longitude")) {
					detailColumnValues.add(detailMap.get("longitude").getNewValue());
				} else if (detailMap.containsKey("nelng") && detailMap.containsKey("swlng")) {
					try {
						nelng = (Double) decimalFormat.parse(detailMap.get("nelng").getNewValue());
						swlng = (Double) decimalFormat.parse(detailMap.get("swlng").getNewValue());

						Double sum = nelng + swlng;
						Double lng = sum / 2;

						String longitude = lng.toString();
						detailColumnValues.add(longitude);

					} catch (ParseException e) {
						detailColumnValues.add("");
					}

				} else {
					detailColumnValues.add("");
				}
	            
	            if(detailMap.containsKey("distance")){
	            	detailColumnValues.add(detailMap.get("distance").getNewValue());
	            } else if (detailMap.containsKey("nelat") && detailMap.containsKey("nelng") &&
	            		detailMap.containsKey("swlat") && detailMap.containsKey("swlng")){
	           
	            	Double distance1 = calculateDistanceFromNEAndSWPoints(nelat, nelng, nelat, swlng);
	            	Double distance2 = calculateDistanceFromNEAndSWPoints(nelat, nelng, swlat, nelng);
	            	
	            	if(distance1 != null && distance2 != null){
	            		
	            		//Distance is half of the shortest side 
	            		Double distance = Double.min(distance1, distance2)/2;
	            		
	            		DecimalFormat df = new DecimalFormat("#.##");
	            		
	            		detailColumnValues.add(Double.valueOf(df.format(distance)).toString());
	            		detailColumnValues.add(nelat.toString());
	            		detailColumnValues.add(nelng.toString());
	            		detailColumnValues.add(swlat.toString());
	            		detailColumnValues.add(swlng.toString());
	            	}
	            	
	            } else {
	            	detailColumnValues.add("");detailColumnValues.add("");detailColumnValues.add("");
	            	detailColumnValues.add("");detailColumnValues.add("");
	            }
				
	        	String[] nextLine = detailColumnValues.toArray(new String[detailColumnValues.size()]);
	        	
	        	searchRecords.add(nextLine);
				
			}
		});

		return searchRecords;
	}
	
	private List<String[]> routeLogs(Date from, Date to) {
		final List<String[]> routeRecords = new ArrayList<String[]>();
		loggerService.csvFileWriteLogs("route", from, to, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				Long logId = rs.getLong("logid"); 
				Long resourceKey = rs.getLong("resourcekey");
				String createdOn = rs.getString("createdon");
				String formattedDate = formatAndAdd5HoursToDate(createdOn);
				
				Map<String, LogEventParameter> detailMap = new HashMap<String, LogEventParameter>();
	        	
				List<LogEventParameter> details = loggerService.getLogDetailsRecords(logId);
	            if(details != null && details.size()>0){
	            	for(LogEventParameter detail : details){
	            		detailMap.put(detail.getParamName(), detail);
	            	}
	            }
				
	            List<String> detailColumnValues = new ArrayList<String>();

	            detailColumnValues.add(formattedDate);

				if(resourceKey != null && resourceKey != 0){
					
					Resource resource = resourceService.getResourceByResourceId(resourceKey);
					resourceService.loadData(resource);
					detailColumnValues.add(resource.getFirstValue(resource.getResourceInnerIdFieldName(configHolder)));
					detailColumnValues.add(resource.getFirstValue("title"));
				}
				
				if(detailMap.containsKey("latitude")){
	            	detailColumnValues.add(detailMap.get("latitude").getNewValue());
	            }
	            if(detailMap.containsKey("longitude")){
	            	detailColumnValues.add(detailMap.get("longitude").getNewValue());
	            }
	            if(detailMap.containsKey("distance")){
	            	detailColumnValues.add(detailMap.get("distance").getNewValue());
	            }
	            
				
	        	String[] nextLine = detailColumnValues.toArray(new String[detailColumnValues.size()]);
	        	
	        	routeRecords.add(nextLine);
		
			}
		});
		return routeRecords;

	}
	
	private List<String[]> feedbackLogs(Date from, Date to) {
		final List<String[]> feedbackRecords = new ArrayList<String[]>();
		loggerService.csvFileWriteLogs("feedback", from, to, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				Long logId = rs.getLong("logid"); 
				String createdOn = rs.getString("createdon");
				String formattedDate = formatAndAdd5HoursToDate(createdOn);
						
				Map<String, LogEventParameter> detailMap = new HashMap<String, LogEventParameter>();
	        	
				List<LogEventParameter> details = loggerService.getLogDetailsRecords(logId);
	            if(details != null && details.size()>0){
	            	for(LogEventParameter detail : details){
	            		detailMap.put(detail.getParamName(), detail);
	            	}
	            }
				
	            List<String> detailColumnValues = new ArrayList<String>();
	            
				detailColumnValues.add(formattedDate);
				
				if(detailMap.containsKey("feedbackemail")){
					detailColumnValues.add(detailMap.get("feedbackemail").getNewValue());
				}else
					detailColumnValues.add("nincs megadva");
				
				if(detailMap.containsKey("latitude")  && detailMap.containsKey("longitude")){
	            	detailColumnValues.add(detailMap.get("latitude").getNewValue());
	            	detailColumnValues.add(detailMap.get("longitude").getNewValue());
				}else{
					detailColumnValues.add("nincs megadva");
				}
				
	        	String[] nextLine = detailColumnValues.toArray(new String[detailColumnValues.size()]);
	        	
	        	feedbackRecords.add(nextLine);
				
			}
		});
		
		return feedbackRecords;

	}
	
	private String formatAndAdd5HoursToDate(String createdOn){
		
		Long hoursToAdd = 5L;
		Calendar c = Calendar.getInstance();
		int offset = c.get(Calendar.DST_OFFSET);
		
		if(offset > 0){
			hoursToAdd = 6L;
		}
		
		Date logDate = null;
		try {
			logDate = dt.parse(createdOn);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		final long hoursInMillis = 60L * 60L * 1000L;
		
		Date newDate = new Date(logDate.getTime() + (hoursToAdd * hoursInMillis));
		
		String formattedDate = dt.format(newDate);
		
		return formattedDate;
	}

	private static Double calculateDistanceFromNEAndSWPoints(Double nelat, Double nelng, Double swlat, Double swlng) {
		if(nelat != null && nelng != null && swlat != null && swlng != null){
			
			
			Double earthRadius = new Double(6371); //kilometers
			Double dLat = Math.toRadians(swlat-nelat);
			Double dLng = Math.toRadians(swlng-nelng);
			Double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
					Math.cos(Math.toRadians(nelat)) * Math.cos(Math.toRadians(swlat)) *
					Math.sin(dLng/2) * Math.sin(dLng/2);
			Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			Double distance = earthRadius * c;
			return distance;
		}
		
		return null;
	}
}
