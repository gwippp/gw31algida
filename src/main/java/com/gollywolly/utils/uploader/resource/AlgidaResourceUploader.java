package com.gollywolly.utils.uploader.resource;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.ui.context.support.DelegatingThemeSource;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.CsvToBean;

import com.gollywolly.gw3utils.persistance.entity.EntityFactory;
import com.gollywolly.persistance.dao.ResourceService;
import com.gollywolly.persistance.model.Errors;
import com.gollywolly.persistance.model.Resource;
import com.gollywolly.persistance.model.ResourceMapping.Cardinality;
import com.gollywolly.utils.GoogleGeodataUtils;
import com.gollywolly.utils.StringUtils;
import com.gollywolly.utils.uploader.resource.bean.GWTrimmedHeaderColumnNameMappingStrategy;

public class AlgidaResourceUploader extends ResourceUploader {

	static Errors errors = (Errors) EntityFactory.createEntityForModel(Errors.class);
	static GWUploadErrorContainer uploadErrorContainer = new GWUploadErrorContainer();

	static Set<Long> activePOSList = new TreeSet<Long>();

	static Map<Long, List<Long>> targetIdListBySourceIds = new HashMap<>();
	//static Map<Long, String> deleteSourcesOfTypeByTargetId = new HashMap<Long, String>();
	static Map<Long, Boolean> posPromotionActivityStatus = new HashMap<>();

	public static void separatedGeoCoding(Map<Long, String> geodataUpdateResourceId, ResourceService resourceService) {

		GoogleGeodataUtils.startGooleGeoDataRequest(geodataUpdateResourceId, resourceService, uploadErrorContainer);

	}

	public static void promotionMapping(Map<String, String> promotionInnernameFilenameMap,
			ResourceService resourceService, String csvUploadFolder) {

		Map<Long, String> deleteSourcesOfTypeByTargetId = new HashMap<Long, String>(); 
		List<Long> dbPOSIds = resourceService.getAllResourcesIdbyResourceTypeName("pos");

		for (Map.Entry<String, String> entry : promotionInnernameFilenameMap.entrySet()) {
			String innerId = entry.getKey();
			String fileName = entry.getValue();

			Resource promotion = resourceService.getResourceByInnerId("promotion", innerId, errors);
			String fileFullName = csvUploadFolder + fileName;

			promotionFileProcessor(promotion, fileFullName, resourceService);
		}
		
		// Deactivate all POS which is not in AKCIO file
		dbPOSIds.removeAll(activePOSList);
		Long[] deactivatePOSsId = dbPOSIds.toArray(new Long[dbPOSIds.size()]);
		if (deactivatePOSsId != null && deactivatePOSsId.length > 0) {
			logger.info("Deactivate " + deactivatePOSsId.length + " records! ");
			resourceService.deactivateResources(deactivatePOSsId);
		}
		
		for (Map.Entry<Long, Boolean> entry : posPromotionActivityStatus.entrySet()){
			if (!entry.getValue()){
				deleteSourcesOfTypeByTargetId.put(entry.getKey(), "promotion");
			}
		}
		
		resourceService.bulkUpdateResourceMapping(targetIdListBySourceIds, Cardinality.ManyToMany,
				deleteSourcesOfTypeByTargetId);
		
	}

	private static void promotionFileProcessor(Resource promotion, String fileFullName, ResourceService resourceService) {
		int notExistsPOSCounter = 0;
		Long promotionId = promotion.getResourceId();
		
		CSVReader reader = null;

		List<Long> targetIdListForPromotion = new ArrayList<Long>();

		FileInputStream fis;
		try {
			fis = new FileInputStream(fileFullName);

			byte[] fileBytes = removeLeadingBytes(fis, 0);
			fis.close();
			ByteArrayInputStream bis = new ByteArrayInputStream(fileBytes);
			Reader r = new InputStreamReader(bis, "ISO8859_2");
			reader = new CSVReader(r, '\t', '"', 0);
			
			String[] firstLine = reader.readNext();
			
			if(firstLine.length != 2){
				logger.info(fileFullName + " has no valid data!");
				return;
			}
			
			logger.info("Start promotion mapping for " + promotion.getResourceTitle() + " promotion via " + fileFullName
					+ " file.");

			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				boolean hasActivePromotion = false;
				String posInnerId = nextLine[0];
				
				// Empty row is the end of the upload file
				if (StringUtils.isInitial(posInnerId)) {
					break;
				}

				String promotionCellValue = nextLine[1];
				
				Resource pos = resourceService.getResourceByInnerId("pos", posInnerId, errors);

				if (pos == null) {
					logger.error("Not found POS with " + posInnerId + " innerid!");
					notExistsPOSCounter++;
					continue;
				}
				
				Long posId = pos.getResourceId();
				activePOSList.add(posId);
				
				if (!StringUtils.isInitial(promotionCellValue)) {
					promotionCellValue = StringUtils.removeSpecialCharFrom09Number(promotionCellValue);
					int promotionCellInt = Integer.parseInt(promotionCellValue.trim());
					if (promotionCellInt > 0) {
						targetIdListForPromotion.add(posId);
						hasActivePromotion = true;
					}
				}

				if (hasActivePromotion){
					posPromotionActivityStatus.put(posId, true);
				} else {
					if(!posPromotionActivityStatus.containsKey(posId)){
						posPromotionActivityStatus.put(posId, false);
					} 
				}
			}

			targetIdListBySourceIds.put(promotionId, targetIdListForPromotion);
		
			if (notExistsPOSCounter > 0) {
				logger.info("There is " + notExistsPOSCounter + " POS in " + fileFullName
						+ " file whose do not exist!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
}
