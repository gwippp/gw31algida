package com.gollywolly.utils.uploader.resource;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.CsvToBean;

import com.gollywolly.gw3utils.persistance.entity.EntityFactory;
import com.gollywolly.persistance.dao.ResourceService;
import com.gollywolly.persistance.model.Errors;
import com.gollywolly.persistance.model.Resource;
import com.gollywolly.persistance.model.ResourceMapping.Cardinality;
import com.gollywolly.utils.GoogleGeodataUtils;
import com.gollywolly.utils.StringUtils;
import com.gollywolly.utils.uploader.resource.bean.GWTrimmedHeaderColumnNameMappingStrategy;

public class AlgidaResourceUploader_old_20160301 extends ResourceUploader{
	
	static Errors errors = (Errors) EntityFactory.createEntityForModel(Errors.class);
	static GWUploadErrorContainer uploadErrorContainer = new GWUploadErrorContainer();

	public static void separatedGeoCoding(Map<Long, String>geodataUpdateResourceId, ResourceService resourceService){
		
		GoogleGeodataUtils.startGooleGeoDataRequest(geodataUpdateResourceId, resourceService, uploadErrorContainer);
		
	}
	public static void promotionMapping(String promotionMapperFileName, ResourceService resourceService, String csvUploadFolder) {
		
		List<Long> dbPOSIds = resourceService.getAllResourcesIdbyResourceTypeName("pos");
		List<Long> akcioPOSList = new ArrayList<Long>();
		
		Map<Long, List<Long>> targetIdListBySourceIds = new HashMap<Long, List<Long>>();
		Map<Long, String> deleteSourcesOfTypeByTargetId = new HashMap<Long, String>(); 
		
		LinkedHashMap<String, String> columnTitleMap = new LinkedHashMap<String, String>();
		columnTitleMap.put("Vevo", "innerid");
		columnTitleMap.put("Karton (Akc2015_Cornetto)", "prom1");
		
		columnTitleMap.put("Karton (Akc2015_Scooping)", "scooping"); 
		columnTitleMap.put("Karton (Akc2015_Impulse)", "impulse");
		
//		columnTitleMap.put("Karton (Akc2015_Magnum)", "prom2");
//		columnTitleMap.put("Karton (Akc2015_BigMilk)", "prom3");
//		columnTitleMap.put("Karton (Akc2015_IC)", "prom4");

		GWTrimmedHeaderColumnNameMappingStrategy<PromoMappingValue> strategy = new GWTrimmedHeaderColumnNameMappingStrategy<AlgidaResourceUploader_old_20160301.PromoMappingValue>();
		strategy.setType(PromoMappingValue.class);
		strategy.setColumnMapping(columnTitleMap);

		String fileName = csvUploadFolder + promotionMapperFileName;
		CsvToBean<PromoMappingValue> csvProcessor = new CsvToBean<AlgidaResourceUploader_old_20160301.PromoMappingValue>();

		Resource prom1 = resourceService.getResourceByInnerId("promotion", "prom1", errors);
		Long prom1Id = prom1.getResourceId();
		
		Resource scooping = resourceService.getResourceByInnerId("promotion", "scooping", errors);
		Long scoopingId = scooping.getResourceId();
		
		Resource impulsive = resourceService.getResourceByInnerId("promotion", "impulsive", errors);
		Long impulsiveId = impulsive.getResourceId();
/*		
		Resource prom2 = resourceService.getResourceByInnerId("promotion", "prom2", errors);
		Long prom2Id = prom2.getResourceId();
		
		Resource prom3 = resourceService.getResourceByInnerId("promotion", "prom3", errors);
		Long prom3Id = prom3.getResourceId();
		
		Resource prom4 = resourceService.getResourceByInnerId("promotion", "prom4", errors);
		Long prom4Id = prom4.getResourceId();
*/		
		CSVReader reader = null;
		
		List<Long> targetIdListForProm1 = new ArrayList<Long>();
		List<Long> targetIdListForScooping = new ArrayList<Long>();
		List<Long> targetIdListForImpulsive = new ArrayList<Long>();
/*		List<Long> targetIdListForProm2 = new ArrayList<Long>();
		List<Long> targetIdListForProm3 = new ArrayList<Long>();
		List<Long> targetIdListForProm4 = new ArrayList<Long>();
*/		int notExistsPOSCounter = 0;

		FileInputStream fis;
		try {
			fis = new FileInputStream(fileName);

			byte[] fileBytes = removeLeadingBytes(fis, 0);
			fis.close();
			ByteArrayInputStream bis = new ByteArrayInputStream(fileBytes);
			Reader r = new InputStreamReader(bis, "ISO8859_2");
			reader = new CSVReader(r, '\t', '"', 0);

			List<PromoMappingValue> rows = csvProcessor.parse(strategy, reader);

			logger.info("Start promotion mapping");
			
			
			for (PromoMappingValue promoMappingValue : rows) {

				boolean hasActivePromotion = false;
				
				String innerid = promoMappingValue.getInnerid();
				
				//Empty row is the end of the upload file
				if (StringUtils.isInitial(innerid)){
					break;
				}

				Resource vevo = resourceService.getResourceByInnerId("pos", innerid, errors);

				if (vevo==null){
					logger.error("Not found POS with " + innerid + " innerid!");
					notExistsPOSCounter++;
					continue;
				}
				
				Long vevoId = vevo.getResourceId();
				akcioPOSList.add(vevoId);
				
				String prom1Data = promoMappingValue.getProm1();
				String scoopingData = promoMappingValue.getScooping();
				String impulsiveData = promoMappingValue.getImpulse();
/*				String prom2Data = promoMappingValue.getProm2().trim();
				String prom3Data = promoMappingValue.getProm3().trim();
				String prom4Data = promoMappingValue.getProm4().trim();
	*/			
				if (!StringUtils.isInitial(prom1Data)) {
					prom1Data = StringUtils.removeSpecialCharFrom09Number(prom1Data);
					int prom1int = Integer.parseInt(prom1Data.trim());
					if (prom1int > 0) {
						targetIdListForProm1.add(vevoId);
						hasActivePromotion = true;
					}
				}
				
				if (!StringUtils.isInitial(scoopingData)) {
					scoopingData = StringUtils.removeSpecialCharFrom09Number(scoopingData);
					int scoopingint = Integer.parseInt(scoopingData.trim());
					if (scoopingint > 0) {
						targetIdListForScooping.add(vevoId);
						hasActivePromotion = true;
					}
				}
				
				if (!StringUtils.isInitial(impulsiveData)) {
					impulsiveData = StringUtils.removeSpecialCharFrom09Number(impulsiveData);
					int impulsiveint = Integer.parseInt(impulsiveData.trim());
					if (impulsiveint > 0) {
						targetIdListForImpulsive.add(vevoId);
						hasActivePromotion = true;
					}
				}
				
/*				if (!StringUtils.isInitial(prom2Data)) {
					int prom2int = Integer.parseInt(prom2Data);
					if (prom2int > 0) {
						targetIdListForProm2.add(vevoId);
						hasActivePromotion = true;
					}
				}
				
				if (!StringUtils.isInitial(prom3Data)) {
					int prom3int = Integer.parseInt(prom3Data);
					if (prom3int > 0) {
						targetIdListForProm3.add(vevoId);
						hasActivePromotion = true;
					}
				}
				
				if (!StringUtils.isInitial(prom4Data)) {
					int prom4int = Integer.parseInt(prom4Data);
					if (prom4int > 0) {
						targetIdListForProm4.add(vevoId);
						hasActivePromotion = true;
					}
				}
	*/			
				if(!hasActivePromotion){
					deleteSourcesOfTypeByTargetId.put(vevoId, "promotion");
				}
	
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
		if (notExistsPOSCounter > 0){
			logger.info("There is " + notExistsPOSCounter +" POS in " +  promotionMapperFileName +" file whose do not exist!");
		}
		
		targetIdListBySourceIds.put(prom1Id, targetIdListForProm1);
		targetIdListBySourceIds.put(scoopingId, targetIdListForScooping);
		targetIdListBySourceIds.put(impulsiveId, targetIdListForImpulsive);
/*		targetIdListBySourceIds.put(prom2Id, targetIdListForProm2);
		targetIdListBySourceIds.put(prom3Id, targetIdListForProm3);
		targetIdListBySourceIds.put(prom4Id, targetIdListForProm4);
	*/	
		
		//Deactivate all POS which is not in AKCIO file
		dbPOSIds.removeAll(akcioPOSList);
		Long[] deactivatePOSsId = dbPOSIds.toArray(new Long[dbPOSIds.size()]);
		if (deactivatePOSsId != null && deactivatePOSsId.length>0){
			logger.info("Deactivate " + deactivatePOSsId.length + " records! ");
			resourceService.deactivateResources(deactivatePOSsId);
		}

		//processResourceMappings(resourceService, promotionRelationshipMap, Cardinality.ManyToMany);
		
		resourceService.bulkUpdateResourceMapping(targetIdListBySourceIds, Cardinality.ManyToMany, deleteSourcesOfTypeByTargetId);
	}

	public static class PromoMappingValue {
		private String innerid;
		private String prom1;
		private String scooping;
		private String impulse;
		private String prom2;
		private String prom3;
		private String prom4;
		
		public String getInnerid() {
			return innerid;
		}
		public void setInnerid(String innerid) {
			this.innerid = innerid;
		}
		public String getProm1() {
			return prom1;
		}
		public void setProm1(String prom1) {
			this.prom1 = prom1;
		}
		public String getScooping() {
			return scooping;
		}
		public void setScooping(String scooping) {
			this.scooping = scooping;
		}
		public String getImpulse() {
			return impulse;
		}
		public void setImpulse(String impulse) {
			this.impulse = impulse;
		}
		public String getProm2() {
			return prom2;
		}
		public void setProm2(String prom2) {
			this.prom2 = prom2;
		}
		public String getProm3() {
			return prom3;
		}
		public void setProm3(String prom3) {
			this.prom3 = prom3;
		}
		public String getProm4() {
			return prom4;
		}
		public void setProm4(String prom4) {
			this.prom4 = prom4;
		}
	

	}
}
