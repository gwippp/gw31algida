package com.gollywolly.utils.validation;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.gollywolly.persistance.dao.ResourceService;

public class AlgidaValidationCenter extends GWValidationCenter {

	public static Long getClassIdToName(String name) {
		return theCenter.classService.getClassByName(name).getClassId();
	}
	
	public static String getClassFullCodeToId(Long id) {
		return theCenter.classStructureService.getClassStructure().getFullCode(id);
	}
	
	public static String getClassFullCodeToName(String name) {
		return getClassFullCodeToId(getClassIdToName(name));
	}
	
	public static ResourceService getResourceService() {
		return GWValidationCenter.theCenter.resourceService;
	}
	
	public static boolean timePeriod1IsInPeriod2(Time startTime1, Time endTime1, Time startTime2, Time endTime2) {
		return (!startTime1.before(startTime2) && !endTime1.after(endTime2));
	}

}
