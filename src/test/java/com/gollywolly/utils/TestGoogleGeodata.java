package com.gollywolly.utils;


import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.gollywolly.gw3utils.pluginloader.ConfigurationHolder;
import com.gollywolly.gw3utils.pluginloader.context.ZemReloadableResourceBundleMessageSource;
import com.gollywolly.gw3utils.pluginloader.utils.AppContextHolder;
import com.gollywolly.persistance.GW3AlgidaTestBase;
import com.gollywolly.persistance.dao.ResourceService;
import com.gollywolly.persistance.model.Resource;
import com.gollywolly.utils.uploader.resource.GWUploadErrorContainer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/WebRoot/WEB-INF/context/gw3-algida-context.xml")
@TransactionConfiguration(transactionManager = "gw3TxManager")
@Transactional
public class TestGoogleGeodata extends GW3AlgidaTestBase implements ApplicationContextAware {

	@SuppressWarnings("unused")
	private Logger logger = Logger.getLogger(TestGoogleGeodata.class);

	@Autowired
	private ZemReloadableResourceBundleMessageSource messageSource;

	protected ApplicationContext appContext;

	@Autowired
	private ResourceService resourceService;

	@Autowired
	ConfigurationHolder configHolder;

	@BeforeTransaction
	public void setAppId() {
		AppContextHolder.setAppId("GW31-ROCK-EBE3-ALGI");
	}

	@Test
	@Rollback(true)
	//@Rollback(false)
	public void MainTestMethod() {
		googleGeoCoding();
		//reverseGeoCoding();
	}



	@SuppressWarnings("unused")
	private void reverseGeoCoding() {
		String posMapAspectName = "posaddressmap";
		Long resourceId = 88944L;
		Resource resource = resourceService.getResourceByResourceId(resourceId);
		
		GoogleGeodataUtils.proccessGoogleAddressRequest(resource, posMapAspectName, "AIzaSyApnuX-NoBeh6RV6sV_ERO3wm6_7GkPx8I");
		
	}

	private void googleGeoCoding() {
		Map<Long, String> geodataUpdateResourceId = new  HashMap<Long, String>();
		GWUploadErrorContainer uploadErrorContainer = new GWUploadErrorContainer();
		
		String posMapAspectName = "posaddressmap";
		
		//88837,88838,88839,88840,88841,88842,88843
		geodataUpdateResourceId.put(88837L, posMapAspectName); //   nem ott van meg a cím végén van egy KA
		//geodataUpdateResourceId.put(88838L, posMapAspectName);
		//geodataUpdateResourceId.put(88954L, posMapAspectName);
		
		GoogleGeodataUtils.startGooleGeoDataRequest(geodataUpdateResourceId, resourceService, uploadErrorContainer);
		
				
	}

	@SuppressWarnings("unused")
	private void resourcePOSByAgentSearch() {

	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}
}
