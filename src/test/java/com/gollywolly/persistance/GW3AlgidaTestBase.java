package com.gollywolly.persistance;

import com.gollywolly.web.controllers.tests.GW3TestBase;

public class GW3AlgidaTestBase extends GW3TestBase{
	private String userName = "2G-1S@gollywolly.com";
	private String userPass = "fagyi";
	
	private static String gw3URL = "http://localhost:8080/gw3algida/";
	
	private Object[] defaultParams = { "appId", "GW31-ROCK-EBE3-ALGI" };

	@Override
	public String getUserName() {
		return userName;
	}

	@Override
	public String getUserPass() {
		return userPass;
	}

	@Override
	public Object[] getDefaultParams() {
		return defaultParams;
	}
	
	public String getGw3URL() {
		return gw3URL;
	}
}
