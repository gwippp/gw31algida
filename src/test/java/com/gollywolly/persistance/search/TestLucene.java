package com.gollywolly.persistance.search;


import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.FlashMap;

import com.gollywolly.gw3utils.persistance.entity.EntityFactory;
import com.gollywolly.gw3utils.pluginloader.ConfigurationHolder;
import com.gollywolly.gw3utils.pluginloader.context.ZemReloadableResourceBundleMessageSource;
import com.gollywolly.gw3utils.pluginloader.utils.AppContextHolder;
import com.gollywolly.persistance.GW3AlgidaTestBase;
import com.gollywolly.persistance.dao.ResourceService;
import com.gollywolly.persistance.model.Errors;
import com.gollywolly.persistance.model.User;
import com.gollywolly.persistance.search.behaviour.GWPOSSearch;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/WEB-INF/context/gw3-algida-context.xml")
@TransactionConfiguration(transactionManager = "gw3TxManager")
@Transactional
public class TestLucene extends GW3AlgidaTestBase implements ApplicationContextAware {

	private Logger logger = Logger.getLogger(TestLucene.class);

	@Autowired
	private ZemReloadableResourceBundleMessageSource messageSource;

	protected ApplicationContext appContext;

	@Autowired
	private ResourceService resourceService;

	@Autowired
	ConfigurationHolder configHolder;

	@BeforeTransaction
	public void setAppId() {
		AppContextHolder.setAppId("GW31-ROCK-EBE3-ALGI");
	}

	@Test
	@Rollback(true)
	public void MainTestMethod() {
		try {
			resourceProximityByType();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testContextController() throws Exception {
		MockHttpServletRequest httpRequest = new MockHttpServletRequest("POST", "/contexts");
		httpRequest.addParameter("agentid", "2R-MTT");
		httpRequest.addParameter("appId", "GW31-ROCK-EBE3-ALGI");

		httpRequest.setAttribute(DispatcherServlet.OUTPUT_FLASH_MAP_ATTRIBUTE, new FlashMap());
		//MockHttpServletResponse response = new MockHttpServletResponse();
		Errors errors = (Errors) EntityFactory.createEntityForModel(Errors.class);

		errors.setMessageSource(messageSource);

		GWPOSSearch testSearch = new GWPOSSearch(resourceService, appContext, httpRequest, errors);

		resourceService.search(testSearch, false, errors);
	}
	
	private void resourceProximityByType() {
		User user = login();
		
		
/*
		String nelat="46.040817358215214";//nelat = "40.915257";
		String swlng="18.211367990612974";//swlng = "-73.70027";
		String swlat="46.040005427288258";//swlat = "40.495995";
		String nelng ="18.22185798084737";//nelng = "-74.25909";
*/
		String nelat="47.7237311418115";
		String swlng="18.037644088745117";
		String swlat="47.49649599145184";
		String nelng ="18.837094039916992";
		
 
		logger.debug(user == null ? "NULL" : user.toString());
		if (user != null) {
			
			postForObjects("resource/parcelproximitysearch", 
					getParameters(new String[]{"start","0", "limit", "10", "nelat", nelat, "swlng", swlng, "swlat", swlat, "nelng", nelng
							, "loadaspect", "posalternativeaddressmap", "loadaspect", "posbasicdata", "relatedid", "106201"}), true);
		}
	}

	@SuppressWarnings("unused")
	private void resourcePOSByAgentSearch() {

	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}
}
