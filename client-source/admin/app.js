/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

Ext.application({
    name: 'algida',

    requires: [
        'algida.util.Helper',
        'Ext.MessageBox',
        'algida.util.Config',
        'algida.util.LocationHelper',
        'algida.util.RequestCenter',
        'algida.store.LocationStore',
        'algida.store.ContactAddressMapStore',
        'algida.store.LocationBasicDataStore',
        'algida.model.GWResource',
        'Ext.data.proxy.JsonP',
        'Ext.Map'
    ],
    
    views: [
        'Main'
    ],
    
    controllers: [
        'algida.controller.MainController',
        'algida.controller.MapsController',
        'algida.controller.AuthController',
        'algida.controller.SuperController'
    ],

    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function() {
        Ext.override(Ext.util.SizeMonitor, {
            constructor: function(config) {
                var namespace = Ext.util.sizemonitor;
                if (Ext.browser.is.Firefox) {
                    return new namespace.OverflowChange(config);
                } else if (Ext.browser.is.WebKit) {
                if (!Ext.browser.is.Silk && Ext.browser.engineVersion.gtEq('535') && !Ext.browser.engineVersion.ltEq('537.36')) {
                    return new namespace.OverflowChange(config);
                } else {
                    return new namespace.Scroll(config);
                }
                } else if (Ext.browser.is.IE11) {
                    return new namespace.Scroll(config);
                } else {
                    return new namespace.Scroll(config);
                }
            }
        });
        
        Ext.override(Ext.util.PaintMonitor, {
            constructor: function (config) {
                if (Ext.browser.is.Firefox || (Ext.browser.is.WebKit && Ext.browser.engineVersion.gtEq('536') && !Ext.browser.engineVersion.ltEq('537.36') && !Ext.os.is.Blackberry)) {
                    return new Ext.util.paintmonitor.OverflowChange(config);
                }
                else {
                    return new Ext.util.paintmonitor.CssAnimation(config);
                }
            }
        });
        
        history.pushState({}, 'login', "#");
        //console.log(history.length);
        
        //console.log(this.getApplication());
        
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();
        
        window.addEventListener('hashchange', function(){
            
        });
        
        // Initialize the main view
        Ext.Viewport.add(Ext.create('algida.view.Main'));
        
    },

    onUpdated: function() {
        Ext.Msg.show({
            title: "Alkalmazás Frissítés",
            cls: 'updateText',
            message: "Az alkalmazás újabb verziója érhető el. Frissíti most?",
            multiLine: true,
            buttons: [{
                    itemId: 'yes',
                    text: 'Igen',
                    ui: 'action'
                }, {
                    itemId: 'cancel',
                    text: 'Mégsem'
                }],
            prompt: false,
            fn: function(text, btn) {
                //console.log(text,btn);
                if (text === 'yes') {
                    window.location.reload();
                }
            }
        });
    }
});
