

Ext.define('algida.form.ListFilterForm', {
    extend: 'Ext.form.Panel',
    xtype: 'listfilterform',
    requires: [
        'Ext.field.Select'
    ],
    initialize: function() {
        this.callParent();
    },
    config: {
        cls: 'listfilterform',
        scrollable: false,
        items: [
            {
                xtype: 'container',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'textfield',
                        name: 'name',
                        alias: 'keywordField',
                        label: 'Név',
                        width: '25%'
                    },
                    {
                        xtype: 'selectfield',
                        label: 'Jóváhagyás típusa',
                        labelWidth: '0%',
                        labelCls: 'approveLabel',
                        name: 'approved',
                        alias: 'approved',
                        width: '25%',
                        options: [
                            {text: 'Mind',  value: ''},
                            {text: 'Jóváhagyott',  value: '30'},
                            {text: 'Nem jóváhagyott', value: '20'},
                            {text: 'Nem találtam', value: '40'}
                        ]
                    },
                    {
                        width: '18%',
                        xtype: 'button',
                        alias: 'locationListFormSubmitButton',
                        text: 'Keresés!',
                        cls: 'locationListFormSubmitButton'
                    }
                ]
            }
        ]
    }
});

