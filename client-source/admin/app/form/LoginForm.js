

Ext.define('algida.form.LoginForm', {
    extend: 'Ext.form.Panel',
    xtype: 'loginform',
    requires: [
        'Ext.field.Password',
        'Ext.field.Hidden'
    ],
    initialize: function() {
        this.callParent();
    },
    config: {
        cls: 'loginForm',
        scrollable: null,
        items: [
            {
                xtype: 'textfield',
                name: 'j_username',
                label: 'Felhasználónév'
            },
            {
                xtype: 'passwordfield',
                name: 'j_password',
                label: 'Jelszó'
            },
            {
                xtype: 'hiddenfield',
                name: 'appId',
                value: algida.util.Config.getAppId()
            },
            {
                xtype: 'button',
                text: 'Bejelentkezek',
                alias: 'submitloginbutton'
            }
        ]
    }
});

