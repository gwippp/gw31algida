


Ext.define('algida.controller.AuthController', {
        
	extend: 'Ext.app.Controller',
        
        requires: [
            'algida.model.UserModel',
            'algida.util.RequestCenter',
            'algida.util.Helper'
        ],
        
	config: {
            
                refs: {
                        submitLoginButton: 'button[alias=submitloginbutton]',
                        signOutButton: 'button[alias=signOutButton]',
                        loginForm: 'loginform',
                        mainTabPanel: 'maintabpanel',
                        locationList: 'locationlist',
                        keywordField: 'textfield[alias=keywordField]',
                        approved: 'selectfield[alias=approved]'
		},

		control: {
                        submitLoginButton: {
                            tap: 'onSubmitLoginButtonTap'
                        },
                        loginform: {
                            action: 'onSubmitLoginButtonTap'
                        },
                        signOutButton: {
                            tap: 'onSignOutButtonTap'
                        }
		},
                
		routes: {
			'': 'actionAuth'
		}
                
	},

	init: function() {
            
                
            
		this.callParent(arguments);
	},
        
        onSignOutButtonTap: function(){
            
            var signOutUrl = algida.util.Config.getServer() + '/j_spring_security_logout';
            var scope = this;
            Ext.Ajax.request({
                url: signOutUrl,
                success: function(response) {
                    scope.getLocationList().getStore().currentPage = 1;
                    algida.util.Config.setUser(null);
                    algida.app.redirectTo('');
                },
                failure: function(response, opts) {
                    //console.log('failure' + response);
                }
            });
            
        },
        
        onSubmitLoginButtonTap: function(){
            Ext.Ajax.setUseDefaultXhrHeader(false);
            
            var form = this.getLoginForm();
            
            var formValues = form.getValues();
            
            var requestObject = {
                url: algida.util.Config.getServer() + '/j_spring_security_check',
                values: formValues,
                success: this.successfulLogin,
                failure: this.failedLogin,
                scope: this
                //masked: main
            };
            
            algida.util.RequestCenter.doRequest(requestObject);
        },
        
        successfulLogin: function(response){
            this.getKeywordField().setValue('');
            this.getApproved().setValue('');
            
            var mainController = this.getApplication().getController('MainController');
            mainController.setCurrentDepo(false);
            
            var scope = this;
            
            response = Ext.decode(response.responseText);
            
            var user = Ext.create('algida.model.UserModel', response.payload[0]);

            user.set('userrole', algida.util.Config.getUserRole(response.payload));

            var currentUserRole = user.get('userroles');

            algida.util.Config.setUser(user);
            
            if(!algida.util.Helper.arrayContains(currentUserRole, 'ROLE_SUPERADMIN') && !algida.util.Helper.arrayContains(currentUserRole, 'ROLE_CARELINE')){
                scope.getApplication().redirectTo('locationList');
            }
            
            if(algida.util.Helper.arrayContains(currentUserRole, 'ROLE_SUPERADMIN') || algida.util.Helper.arrayContains(currentUserRole, 'ROLE_CARELINE')){
                scope.getApplication().redirectTo('depolist');
            }
        },
        
        failedLogin: function(response){
            algida.util.RequestCenter.generateErrorWindow(algida.util.RequestCenter.getMessages(), 'Sikertelen Bejelentkezés');
        },
        
        actionAuth: function(){
            var mainTabPanel = this.getMainTabPanel();
            var activeItem = mainTabPanel.getActiveItem();
            var activeItemIndex = mainTabPanel.items.indexOf(activeItem);
            
            var payload = {};
            
            var scope = this;
            
            Ext.Ajax.request({
                url: algida.util.Config.getServer() + '/profile/get',
                method: 'POST',
                params: {
                    data: Ext.encode( {
                        id: new Date().getTime(),
                        payload: [
                            payload
                        ]
                    }),
                    appId: algida.util.Config.getAppId()
                },
                scope:this,
                success: function(response){
                    if(response.status === 200){
                        
                        var user = algida.util.Config.getUser();
                        
                        if(user !== false){
                            var currentUserRole = user.get('userroles');
                            
                            if(!algida.util.Helper.arrayContains(currentUserRole, 'ROLE_SUPERADMIN') && !algida.util.Helper.arrayContains(currentUserRole, 'ROLE_CARELINE')){
                                scope.getApplication().redirectTo('locationList');
                            }

                            if(algida.util.Helper.arrayContains(currentUserRole, 'ROLE_SUPERADMIN') || algida.util.Helper.arrayContains(currentUserRole, 'ROLE_CARELINE')){
                                scope.getApplication().redirectTo('depolist');
                            }
                        }
                        
                    }else{
                        if(activeItemIndex !== 0){
                            //mainTabPanel.setActiveItem(0);
                        }
                    }
                },
                failure: function(response){
                    if(activeItemIndex !== 0){
                        var user = algida.util.Config.getUser();
                        
                        if(user !== false){                        
                            var currentUserRole = user.get('userrole');

                            if(!algida.util.Helper.arrayContains(currentUserRole, 'ROLE_SUPERADMIN') && !algida.util.Helper.arrayContains(currentUserRole, 'ROLE_CARELINE')){
                                scope.getApplication().redirectTo('locationList');
                            }

                            if(algida.util.Helper.arrayContains(currentUserRole, 'ROLE_SUPERADMIN') || algida.util.Helper.arrayContains(currentUserRole, 'ROLE_CARELINE')){
                                scope.getApplication().redirectTo('depolist');
                            }
                        }
                        
                        if(response.status === 403){
                            mainTabPanel.setActiveItem(0);
                        }
                        if(response.status === 0){
                            mainTabPanel.setActiveItem(0);
                        }
                    }
                }
            });
        }
        
});




