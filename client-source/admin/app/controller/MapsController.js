
Ext.define('algida.controller.MapsController', {
        
	extend: 'Ext.app.Controller',

	config: {
                refs: {
                        map: 'map',
                        mainTabPanel: 'maintabpanel',
                        backToMainButton: 'button[alias=backToMain]',
                        saveNewLocationButton: 'button[alias=saveNewLocation]',
                        mapsTitleContainer: 'container[alias=mapsTitleContainer]',
                        approveField: 'togglefield[alias=approveField]',
                        infoButton: 'button[alias=infoButton]',
                        addCustomAddressButton: 'button[alias=addCustomAddress]',
                        infoContainer: 'container[alias=infoContainer]',
                        customAddressInputContainer: 'container[alias=customAddressInputContainer]',
                        customAddressInputField: 'textfield[alias=customAddressInputField]',
                        resetButton: 'button[alias=resetButton]',
                        locateButton: 'button[alias=locateButton]',
                        notFoundCheck: 'checkboxfield[alias=notFound]',
                        searchOnGoogleButton: 'button[alias=searchOnGoogleButton]'
                },
                control: {
                        backToMainButton: {
                            tap: 'onBackToMainButtonTap'
                        },
                        saveNewLocationButton: {
                            tap: 'onSaveNewLocationButtonTap'
                        },
                        infoButton: {
                            tap: 'onInfoButtonTap'
                        },
                        addCustomAddressButton: {
                            tap: 'onAddCustomAddressButtonTap'
                        },
                        resetButton: {
                            tap: 'onResetButtonTap'
                        },
                        locateButton: {
                            tap: 'onLocateButtonTap'
                        },
                        customAddressInputField: {
                            action: 'onCustomAddressInputFieldAction'
                        },
                        approveField: {
                            change: 'onApproveFieldChange'
                        },
                        notFoundCheck: {
                            check: 'onNotFoundCheck',
                            uncheck: 'onNotFoundUnCheck'
                        },
                        searchOnGoogleButton: {
                            'tap': 'onSearchOnGoogleButtonTap'
                        }
                },
		routes: {
			'maps': 'actionMaps'
		},
                currentRecord: false,
                originalPosition: false,
                panorama: false,
                marker: false,
                streetViewSensitivity: 20,
                enterCounter: 0,
                geocoder: new google.maps.Geocoder()
	},

	init: function() {
		this.callParent(arguments);
	},
        
        onSearchOnGoogleButtonTap: function(){
            
            //keressen rá a cégnév + pontos cím keresésre (illetve a felülírt címre, ha már átírva az admin kézzel    
            var currentRecord = this.getCurrentRecord();
            
            var alternateAddress = currentRecord.data.posalternativeaddressmap;
            
            var addressToSearch = null;
            var title = currentRecord.get('posbasicdata').name[0];
            
            if (typeof alternateAddress !== 'undefined') {
                var address = currentRecord.data.posalternativeaddressmap.address[0]['altaddress'];
                addressToSearch = address;
            } else {
                var address = currentRecord.get('posaddressmap').address[0];
                var fullAddress = address.zipcode + ', ' + address.settlement + ', ' + address.streetaddress1;
                addressToSearch = fullAddress;
            }
            
            if(Ext.browser.name !== 'Safari'){
                var win = window.open('http://www.google.com/#q=' + title + ' ' + addressToSearch, '_blank');
                win.focus();
            }else{
                var win = window.open('http://www.google.com/?q=' + title + ' ' + addressToSearch, '_blank');
                win.focus();                
            }
        },
        
        onApproveFieldChange: function(){
            var approveField = this.getApproveField();
            if(approveField.getValue() === 1){
                this.getNotFoundCheck().setChecked(false);
            }
        },
        
        onNotFoundCheck: function(){
            var approveField = this.getApproveField();
            if(approveField.getValue() === 1){
                
            }
            approveField.setValue(0);
            approveField.addCls('red');
        },
        
        onNotFoundUnCheck: function(){
            var approveField = this.getApproveField();
            approveField.removeCls('red');
        },
        
        onCustomAddressInputFieldAction: function(){
            this.onLocateButtonTap();
        },
        
        onLocateButtonTap: function(){
            var customAddress = this.getCustomAddressInputField().getValue();
            algida.util.LocationHelper.geoCodeAddress(customAddress);
        },
        
        onCustomAddressLocated: function(location){
            var currentRecord = this.getCurrentRecord();
            
            currentRecord.set('latitude', location.lat());
            currentRecord.set('longitude', location.lng());
            
            this.actionMaps();
        },
        
        onResetButtonTap: function(){
            var originalPosition = this.getOriginalPosition();
            var currentRecord = this.getCurrentRecord();
            
            currentRecord.set('latitude', originalPosition.latitude);
            currentRecord.set('longitude', originalPosition.longitude);
            this.actionMaps();
        },
                
        onAddCustomAddressButtonTap: function(){
            var fieldContainer = this.getCustomAddressInputContainer();
            
            if(Ext.get(Ext.query('.customAddressInputContainer')[0]).getHeight() > 0){
                Ext.Animator.run({
                    element: fieldContainer.element,
                    duration: 400,
                    easing: 'ease-in',
                    preserveEndState: true,
                    autoClear: true,
                    from: {
                        height: Ext.get(Ext.query('.customAddressInputContainer')[0]).originalHeight
                    },
                    to: {
                        height: 0
                    }
                });
            }else{
                Ext.Animator.run({
                    element: fieldContainer.element,
                    duration: 400,
                    easing: 'ease-in',
                    preserveEndState: true,
                    autoClear: true,
                    from: {
                        height: 0
                    },
                    to: {
                        height: Ext.get(Ext.query('.customAddressInputContainer')[0]).originalHeight 
                    }
                });
            }
        },
                
        onInfoButtonTap: function(){
            var infoContainer = this.getInfoContainer();
            
            if(Ext.get(Ext.query('.helpText')[0]).getHeight() > 0){
                Ext.Animator.run({
                    element: infoContainer.element,
                    duration: 400,
                    easing: 'ease-in',
                    preserveEndState: true,
                    autoClear: true,
                    from: {
                        height: Ext.get(Ext.query('.helpText')[0]).originalHeight
                    },
                    to: {
                        height: 0
                    }
                });
            }else{
                Ext.Animator.run({
                    element: infoContainer.element,
                    duration: 400,
                    easing: 'ease-in',
                    preserveEndState: true,
                    autoClear: true,
                    from: {
                        height: 0
                    },
                    to: {
                        height: Ext.get(Ext.query('.helpText')[0]).originalHeight 
                    }
                });
            }
        },
                
        onBackToMainButtonTap: function(){
            this.getCustomAddressInputField().setValue('');
            this.getApplication().redirectTo('locationList');
        },
        
        onSaveNewLocationButtonTap: function(){
            var currentRecord = this.getCurrentRecord();
            var approveField = this.getApproveField();
            var notFoundCheck = this.getNotFoundCheck();
            
            var marker = this.getMarker();
            var currentCustomAddress = Ext.util.Format.trim(this.getCustomAddressInputField().getValue());
            
            var address = currentRecord.data.posaddressmap.address[0];
            var fullAddress = address.zipcode + ', ' + address.settlement + ', ' + currentRecord.data.posaddressmap.street1;
            
            try{            
                if(marker !== false){
                    currentRecord.data.posaddressmap.latitude = String(marker.getPosition().lat());
                    currentRecord.data.posaddressmap.longitude = String(marker.getPosition().lng());
                }else{
                    currentRecord.set('locationalternativeaddressmap_altaddress', currentCustomAddress);
                    
                    currentRecord.data.posaddressmap.latitude = String(this.getPanorama().getPosition().lat());
                    currentRecord.data.posaddressmap.longitude = String(this.getPanorama().getPosition().lng());
                }
            }catch(err){
                Ext.Msg.show({
                    title: "Próbáld geolokálni a helyet!",
                    cls: 'updateText',
                    //message: "Az alkalmazás újabb verziója érhető el. Frissíti most?",
                    multiLine: true,
                    buttons: [{
                            itemId: 'ok',
                            text: 'OK',
                            ui: 'action'
                        }],
                    prompt: false,
                    fn: function(text, btn) {
                        Ext.Msg.hide();
                    }
                });
                return;
            }
            
            var newApprovedValue = String(20 + approveField.getValue() * 10);
            
            if(approveField.getValue() === 0 && notFoundCheck.getChecked() === true){
                newApprovedValue = String(40);
            }
            
            var resourceObject = {};
            
            var payload =  [
                            resourceObject
                            ];
            
            if(Math.round(currentRecord.data.posaddressmap.heading) !== null && Math.round(currentRecord.data.posaddressmap.pitch)){
                currentRecord.data.posaddressmap.heading = String(Math.round(currentRecord.data.posaddressmap.heading));
                currentRecord.data.posaddressmap.pitch = String(Math.round(currentRecord.data.posaddressmap.pitch));
            }
            
            for(i in currentRecord.data){
                var currentPropertyValue = currentRecord.data[i];
                
                if(i === 'posaddressmap'){
                    currentPropertyValue['approved'] = String(newApprovedValue);
                }
                
                if(i === 'posbasicdata' || i === 'posaddressmap'){
                    currentPropertyValue['!op'] = 'UPDATE';
                    currentPropertyValue['!OP'] = 'UPDATE';
                    payload.push(currentPropertyValue);
                }
                
                if(i === 'aspects'){
                    if(fullAddress !== currentCustomAddress && Ext.String.trim(currentCustomAddress) !== ''){
                        
                        var altaddressAspectObject = {
                                "uniqueid": "posalternativeaddressmap_" + currentRecord.get('resourceId'),
                                "address": [
                                    {
                                        "altaddress": currentCustomAddress
                                    }
                                ],
                                "aspectname": "posalternativeaddressmap",
                                "!id": "GWAspectPosalternativeaddressmap:posalternativeaddressmap_" + currentRecord.get('resourceId'),
                                "object": "GWResource:" + currentRecord.get('resourceId')
                            };
                        
                        if(algida.util.Helper.arrayContains(currentRecord.data[i], "GWAspectPosalternativeaddressmap:posalternativeaddressmap_" + currentRecord.get('resourceId'))){
                            altaddressAspectObject["!op"] = 'UPDATE';
                            altaddressAspectObject["!OP"] = 'UPDATE';
                        }else{
                            currentRecord.data[i].push("GWAspectPosalternativeaddressmap:posalternativeaddressmap_" + currentRecord.get('resourceId'));
                            altaddressAspectObject["!op"] = 'INSERT';
                            altaddressAspectObject["!OP"] = 'INSERT';
                        }
                        
                        payload.push(
                            altaddressAspectObject
                        );

                        try{
                                var deccentedCustomAddress = algida.util.LocationHelper.removeDiacritics(currentCustomAddress).toLowerCase();
                                var deccentedGoogleAddress = algida.util.LocationHelper.removeDiacritics(Ext.get('currentAddressP').dom.innerText).toLowerCase();
                                var distance = algida.util.LocationHelper.getLevenshteinDistance(deccentedCustomAddress, deccentedGoogleAddress);
                                var percentDist = 1 - (distance / Math.min(deccentedCustomAddress.length, deccentedGoogleAddress.length));
                                var minDist = 0.85;

                                if(percentDist < minDist){    
                                    Ext.Msg.show({
                                        title: "Probléma",
                                        cls: 'updateText higher',
                                        message: "Az általad beírt cím jelentősen eltér a google által javasolttól. Kérlek ellenőrizd!",
                                        multiLine: true,
                                        buttons: [{
                                                itemId: 'ok',
                                                text: 'OK',
                                                ui: 'action'
                                            }],
                                        prompt: false,
                                        fn: function(text, btn) {
                                            Ext.Msg.hide();
                                        }
                                    });
                                    return;

                                }
                        }catch(err){

                        }
                    }
                }
                
                if(typeof currentPropertyValue !== 'undefined' && i !== 'posbasicdata' && i !== 'posaddressmap'){
                    resourceObject[i] = currentPropertyValue;
                }
            }
            
            resourceObject['!op'] = 'UPDATE';
            resourceObject['!OP'] = 'UPDATE';
            
            console.log('payload::', payload);
            
            Ext.Ajax.request({
                url: algida.util.Config.getServer() + '/resource/update',
                method: 'POST',
                params: {
                    data: Ext.encode( {
                        id: new Date().getTime(),
                        payload: payload
                    }),
                    appId: algida.util.Config.getAppId()
                },
                scope:this,
                success: function(response){
                    algida.util.RequestCenter.generateErrorWindow(algida.util.RequestCenter.getMessages(), '');
                },                                    
                failure: function(response){
                    algida.util.RequestCenter.generateErrorWindow(algida.util.RequestCenter.getMessages(), 'Sikertelen Mentés');
                }
            });
        },
        
        actionMaps: function(){
            
            algida.util.LocationHelper.setApplication(this.getApplication());
            
            var map = new google.maps.Map(document.getElementById("mapView"), {center: new google.maps.LatLng(47.498056, 19.04), zoom: 14});
            
            var mainTabPanel = this.getMainTabPanel();
            var activeItem = mainTabPanel.getActiveItem();
            
            var activeItemIndex = mainTabPanel.items.indexOf(activeItem);
            var currentRoute = algida.util.Config.getCurrentRoute(this.getApplication());
            
            var marker = this.getMarker();
            
            if(marker !== false){
                marker.setMap(null);
            }
            
            this.setMarker(false);
            var currentRecord = this.getCurrentRecord();
            
            if(typeof currentRecord.data.posaddressmap.latitude === 'undefined'
             || typeof currentRecord.data.posaddressmap.longitude === 'undefined'){
                
                var currentRecordPosition = new google.maps.LatLng(47.498056, 19.04);
                if(marker === false){
                    marker = new google.maps.Marker({
                                map: map,
                                draggable: true,
                                animation: google.maps.Animation.DROP,
                                position: currentRecordPosition,
                                optimized: false,
                                zIndex: 5
                           });
                    this.setMarker(marker);
                    marker.setMap(map);
                }
                
                var scope = this;
                var sv = new google.maps.StreetViewService();
                
                google.maps.event.clearListeners(marker, "dragend");
                google.maps.event.addListener(marker, "dragend", function(event) {
                    var lat = event.latLng.lat();
                    var lng = event.latLng.lng();

                    var newLatLng = new google.maps.LatLng(lat, lng);

                    sv.getPanoramaByLocation(newLatLng, scope.getStreetViewSensitivity(), function(data, status) {
                        scope.handlePanoramaResult(data, status, scope, map, sv);
                    });

                    currentRecord.data.posaddressmap.latitude = String(marker.getPosition().lat());
                    currentRecord.data.posaddressmap.longitude = String(marker.getPosition().lng());

                });
                
                try{
                    if(currentRoute !== mainTabPanel.getAt(activeItemIndex).config.expectedUrl){
                        var items = mainTabPanel.getItems().items;
                        for(var i in items){
                            var item = items[i];
                            if(item.config.expectedUrl === currentRoute){
                                mainTabPanel.setActiveItem(item);
                            }
                        }
                    }
                }catch(err){
                }

                if(currentRecord === false){
                    this.getApplication().redirectTo('locationList');
                    return;
                }
                
                document.getElementById('streetView').innerHTML = '<p class="noStreetView">Nem tudtunk lokációt meghatározni, kérem húzza a jelölőt a megfelelő helyre!</p>';
                
                var address = currentRecord.data.posaddressmap.address[0];
                
                var fullAddress = address.zipcode + ', ' + address.settlement + ', ' + currentRecord.data.posaddressmap.street1[0] + '//';
                
                var locname = '';
                
                if(typeof currentRecord.data.posbasicdata.locname !== 'undefined'){
                    locname = ', ' + currentRecord.data.posbasicdata.locname[0] + '';
                }
                
                var loctypeName = '';
                
                if(typeof currentRecord.data.posbasicdata.loctypename !== 'undefined' && currentRecord.data.posbasicdata.loctypename[0] !== ''){
                    loctypeName = '<br/><span class="fullAddress">' + currentRecord.data.posbasicdata.loctypename[0] + locname + '</span>';
                }
                
                this.getMapsTitleContainer().setHtml(currentRecord.get('title') + '<br/><span class="fullAddress">' + fullAddress + '</span>' + loctypeName);
                
                var field = this.getCustomAddressInputField();

                var alternateAddress = currentRecord.data.posalternativeaddressmap;

                if(typeof alternateAddress !== 'undefined'){
                    var address = currentRecord.data.posalternativeaddressmap.address[0]['altaddress'];

                    if(Ext.String.trim(field.getValue()) !== '' && field.getValue() !== address){

                    }else{
                        field.setValue(address);
                    }

                    Ext.get(Ext.query('.customAddressInputContainer')[0]).setHeight(Ext.get(Ext.query('.customAddressInputContainer')[0]).originalHeight);
                }else{
                    var address = currentRecord.data.posaddressmap.address[0];
                    var fullAddress = address.zipcode + ', ' + address.settlement + ', ' + currentRecord.data.posaddressmap.street1[0];

                    if(Ext.String.trim(field.getValue()) !== '' && field.getValue() !== alternateAddress){

                    }else{                    
                        field.setValue(fullAddress);
                        var originalHeight = Ext.get(Ext.query('.customAddressInputContainer')[0]).getHeight();

                        if(typeof Ext.get(Ext.query('.customAddressInputContainer')[0]).originalHeight === 'undefined'){
                            Ext.get(Ext.query('.customAddressInputContainer')[0]).originalHeight = originalHeight;   
                        }

                        Ext.get(Ext.query('.customAddressInputContainer')[0]).setHeight(0);
                    }
                }

                if(this.getOriginalPosition() === false){
                    var originalPosition = {};
                    originalPosition.latitude = currentRecord.data.posaddressmap.latitude;
                    originalPosition.longitude = currentRecord.data.posaddressmap.longitude;
                    this.setOriginalPosition(originalPosition);
                }

                //Ext.get('streetView').setHtml('');

                if(typeof currentRecord.data.posaddressmap.latitude !== 'undefined'){
                    this.getNotFoundCheck().setChecked(false);

                    if(currentRecord.data.posaddressmap.approved[0] === '40'){
                       this.getNotFoundCheck().setChecked(true);
                       this.getApproveField().setValue(0);
                       this.getApproveField().addCls('red');
                    }else{

                    }

                    if(currentRecord.data.posaddressmap.approved[0] === '30'){
                       this.getApproveField().setValue(1);
                    }else{
                        this.getApproveField().setValue(0);
                    }

                    var position = new google.maps.LatLng(currentRecord.data.posaddressmap.latitude, currentRecord.data.posaddressmap.longitude);

                    var scope = this;
                    var sv = new google.maps.StreetViewService();

                    sv.getPanoramaByLocation(position, this.getStreetViewSensitivity(), function(data, status){
                        scope.handlePanoramaResult(data, status, scope, map, sv);
                    });

                    google.maps.event.clearListeners(marker, "dragend");
                    google.maps.event.addListener(marker, "dragend", function(event) {
                        var lat = event.latLng.lat();
                        var lng = event.latLng.lng();

                        var newLatLng = new google.maps.LatLng(lat, lng);

                        sv.getPanoramaByLocation(newLatLng, scope.getStreetViewSensitivity(), function(data, status) {
                            scope.handlePanoramaResult(data, status, scope, map, sv);
                        });
                        
                        currentRecord.data.posaddressmap.latitude = String(marker.getPosition().lat());
                        currentRecord.data.posaddressmap.longitude = String(marker.getPosition().lng());
                        
                    });
                }   

                if(this.getEnterCounter() === 0){
                    this.setEnterCounter(this.getEnterCounter() + 1);
                    this.actionMaps();
                }
                //
                
            }else{
                var currentRecordPosition = new google.maps.LatLng(currentRecord.data.posaddressmap.latitude, currentRecord.data.posaddressmap.longitude);

                if(marker === false){
                    marker = new google.maps.Marker({
                                map: map,
                                draggable: true,
                                animation: google.maps.Animation.DROP,
                                position: currentRecordPosition,
                                optimized: false,
                                zIndex: 5
                           });
                    this.setMarker(marker);
                    marker.setMap(map);
                }

                try{
                    if(currentRoute !== mainTabPanel.getAt(activeItemIndex).config.expectedUrl){
                        var items = mainTabPanel.getItems().items;
                        for(var i in items){
                            var item = items[i];
                            if(item.config.expectedUrl === currentRoute){
                                mainTabPanel.setActiveItem(item);
                            }
                        }
                    }
                }catch(err){
                }

                if(currentRecord === false){
                    this.getApplication().redirectTo('locationList');
                    return;
                }
                
                var field = this.getCustomAddressInputField();

                var alternateAddress = currentRecord.data.posalternativeaddressmap;

                if(typeof alternateAddress !== 'undefined'){
                    var address = currentRecord.data.posalternativeaddressmap.address[0]['altaddress'];

                    if(Ext.String.trim(field.getValue()) !== '' && field.getValue() !== address){

                    }else{
                        field.setValue(address);
                    }

                    Ext.get(Ext.query('.customAddressInputContainer')[0]).setHeight(Ext.get(Ext.query('.customAddressInputContainer')[0]).originalHeight);
                }else{
                    var address = currentRecord.data.posaddressmap.address[0];
                    var fullAddress = address.zipcode + ', ' + address.settlement + ', ' + currentRecord.data.posaddressmap.street1;

                    if(Ext.String.trim(field.getValue()) !== '' && field.getValue() !== alternateAddress){

                    }else{                    
                        field.setValue(fullAddress);
                        var originalHeight = Ext.get(Ext.query('.customAddressInputContainer')[0]).getHeight();

                        if(typeof Ext.get(Ext.query('.customAddressInputContainer')[0]).originalHeight === 'undefined'){
                            Ext.get(Ext.query('.customAddressInputContainer')[0]).originalHeight = originalHeight;   
                        }

                        Ext.get(Ext.query('.customAddressInputContainer')[0]).setHeight(0);
                    }
                }

                if(this.getOriginalPosition() === false){
                    var originalPosition = {};
                    originalPosition.latitude = currentRecord.data.posaddressmap.latitude;
                    originalPosition.longitude = currentRecord.data.posaddressmap.longitude;
                    this.setOriginalPosition(originalPosition);
                }

                Ext.get('streetView').setHtml('');

                if(typeof currentRecord.data.posaddressmap.latitude !== 'undefined'){
                    this.getNotFoundCheck().setChecked(false);
                    
                    if(typeof currentRecord.data.posaddressmap.approved === 'undefined'){
                        currentRecord.data.posaddressmap.approved = '20';
                    }
                    
                    if(currentRecord.data.posaddressmap.approved[0] === '40'){
                       this.getNotFoundCheck().setChecked(true);
                       this.getApproveField().setValue(0);
                       this.getApproveField().addCls('red');
                    }else{

                    }

                    if(currentRecord.data.posaddressmap.approved[0] === '30'){
                       this.getApproveField().setValue(1);
                    }else{
                        this.getApproveField().setValue(0);
                    }

                    var position = new google.maps.LatLng(currentRecord.data.posaddressmap.latitude, currentRecord.data.posaddressmap.longitude);

                    var scope = this;
                    var sv = new google.maps.StreetViewService();

                    sv.getPanoramaByLocation(position, this.getStreetViewSensitivity(), function(data, status){
                        scope.handlePanoramaResult(data, status, scope, map, sv);
                    });

                    google.maps.event.clearListeners(marker, "dragend");
                    google.maps.event.addListener(marker, "dragend", function(event) {
                        var lat = event.latLng.lat();
                        var lng = event.latLng.lng();

                        var newLatLng = new google.maps.LatLng(lat, lng);

                        sv.getPanoramaByLocation(newLatLng, scope.getStreetViewSensitivity(), function(data, status) {
                            scope.handlePanoramaResult(data, status, scope, map, sv);
                        });

                        currentRecord.data.posaddressmap.latitude = String(marker.getPosition().lat());
                        currentRecord.data.posaddressmap.longitude = String(marker.getPosition().lng());
                        
                    });
                }   

                if(this.getEnterCounter() === 0){
                    this.setEnterCounter(this.getEnterCounter() + 1);
                    this.actionMaps();
                }
            }
        },
        
        handlePanoramaResult: function(data, status, scope, map, sv){
            
            var currentRecord = scope.getCurrentRecord();
            var position = new google.maps.LatLng(currentRecord.data.posaddressmap.latitude, currentRecord.data.posaddressmap.longitude);
            
            google.maps.event.trigger(map, 'resize');
            
            if (status === google.maps.StreetViewStatus.OK) {
                
                var marker = scope.getMarker();
                
                if(marker !== false){
                    //marker.setMap(null);
                }
                
                var panoramaOptions = {
                    position: position,
                    pov: {
                      heading: 34,
                      pitch: 10
                    }
                };
                if(typeof currentRecord.data.posaddressmap.heading !== 'undefined' && typeof currentRecord.data.posaddressmap.pitch !== 'undefined'){
                    panoramaOptions = {
                        position: position,
                        pov: {
                          heading: Math.round(currentRecord.data.posaddressmap.heading),
                          pitch: Math.round(currentRecord.data.posaddressmap.pitch)
                        }
                    };                    
                }
                
                var panorama = new google.maps.StreetViewPanorama(document.getElementById("streetView"), panoramaOptions);
                
                map.setCenter(new google.maps.LatLng(currentRecord.data.posaddressmap.latitude, currentRecord.data.posaddressmap.longitude));

                var address = currentRecord.data.posaddressmap.address[0];
                
                var fullAddress = address.zipcode + ', ' + address.settlement + ', ' + currentRecord.data.posaddressmap.street1[0] + '//';
                
                var locname = '';
                
                if(typeof currentRecord.data.posbasicdata.locname !== 'undefined'){
                    locname = ', ' + currentRecord.data.posbasicdata.locname[0] + '';
                }
                
                var loctypeName = '';
                
                if(typeof currentRecord.data.posbasicdata.loctypename !== 'undefined' && currentRecord.data.posbasicdata.loctypename[0] !== ''){
                    loctypeName = '<br/><span class="fullAddress">' + currentRecord.data.posbasicdata.loctypename[0] + locname + '</span>';
                }
                
                scope.getMapsTitleContainer().setHtml(currentRecord.get('title') + '<br/><span class="fullAddress">' + fullAddress + '</span>' + loctypeName);
                
                //setting up custom address
                Ext.get(Ext.query('.customAddressInputContainer')[0]).setHeight(0);
                
                var field = scope.getCustomAddressInputField();
                var alternateAddress = currentRecord.data.posalternativeaddressmap;
                
                if(typeof alternateAddress !== 'undefined'){
                    var address = currentRecord.data.posalternativeaddressmap.address[0]['altaddress'];
                    if(Ext.String.trim(field.getValue()) !== '' && field.getValue() !== address){
                        
                    }else{
                        field.setValue(address);
                    }
                    Ext.get(Ext.query('.customAddressInputContainer')[0]).setHeight(Ext.get(Ext.query('.customAddressInputContainer')[0]).originalHeight);
                }else{
                    var address = currentRecord.data.posaddressmap.address[0];
                    var fullAddress = address.zipcode + ', ' + address.settlement + ', ' + currentRecord.data.posaddressmap.street1;
                    
                    if(Ext.String.trim(field.getValue()) !== '' && field.getValue() !== alternateAddress){
                        
                    }else{
                        field.setValue(fullAddress);
                    }
                }
                
                //setting up custom address
                
                google.maps.event.addListener(panorama, 'position_changed', function(){
                    scope.panoramaPositionChanged(scope, panorama, map, sv);
                });
                
                google.maps.event.addListener(panorama, 'pov_changed', function() {
                    currentRecord.data.posaddressmap.heading = panorama.pov.heading;
                    currentRecord.data.posaddressmap.pitch = panorama.pov.pitch;
                });
                
            }else{
                
                var panorama = new google.maps.StreetViewPanorama(document.getElementById("streetView"), panoramaOptions);
                //panorama.setVisible(false);
                document.getElementById('streetView').innerHTML = '<p class="noStreetView">Sajnos ezen a környéken nincs Google Streetview 3D. A felülnézeti térkép alapján keresd meg az eladási helyet.</p>';
                var marker = scope.getMarker();
                if(marker !== false){
                    //marker.setMap(null);
                }
                
                try{
                    map.setOptions({streetViewControl: false, streetView: false, streetViewControlOptions: false, mapTypeId: google.maps.MapTypeId.ROADMAP});
                }catch(error){
                    
                }
                
                map.setCenter(new google.maps.LatLng(currentRecord.data.posaddressmap.latitude, currentRecord.data.posaddressmap.longitude));
                
                if(marker === false){
                    var marker = new google.maps.Marker({
                         map: map,
                         draggable: true,
                         animation: google.maps.Animation.DROP,
                         position: position,
                         optimized: false,
                         zIndex: 5
                    });
                    scope.setMarker(marker);
                }
                
                var address = currentRecord.get('posaddressmap').address[0];
                var fullAddress = address.zipcode + ', ' + address.settlement + ', ' + address.streetaddress1;
                
                var locname = '';
                
                if(typeof currentRecord.get('posbasicdata').locname !== 'undefined'){
                    locname = ', ' + currentRecord.get('posbasicdata').locname[0] + '';
                }
                
                var loctypeName = '';
                if(currentRecord.get('posbasicdata').loctypename[0] !== ''){
                    loctypeName = '<br/><span class="fullAddress">' + currentRecord.get('posbasicdata').loctypename[0] + locname + '</span>';
                }
                
                scope.getMapsTitleContainer().setHtml(currentRecord.get('title') + '<br/><span class="fullAddress">' + fullAddress + '</span>' + loctypeName);
                
                google.maps.event.clearListeners(marker, "dragend");
                google.maps.event.addListener(marker, "dragend", function(event) { 
                    var lat = event.latLng.lat();
                    var lng = event.latLng.lng();

                    var newLatLng = new google.maps.LatLng(lat, lng);
                    
                    sv.getPanoramaByLocation(newLatLng, scope.getStreetViewSensitivity(), function(data, status){
                        scope.handlePanoramaResult(data, status, scope, map, sv);
                    });
                    
                    currentRecord.data.posaddressmap.latitude = String(marker.getPosition().lat());
                    currentRecord.data.posaddressmap.longitude = String(marker.getPosition().lng());
                    
                });
            }
            
        },
        
        setAltAddress: function(){
            var field = this.getCustomAddressInputField();
            field.setValue(Ext.get('currentAddressP').dom.innerText);
        },
        
        panoramaPositionChanged: function(scope, panorama, map, sv) {
            var marker = scope.getMarker();
            var currentRecord = scope.getCurrentRecord();
            
            google.maps.event.clearListeners(panorama, 'visible_changed');
            if (marker === false) {
                var marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                    position: panorama.getPosition(),
                    optimized: false,
                    zIndex: 5
                });
                scope.setMarker(marker);
            } else {
                scope.getMarker().setMap(null);
                var oldMarker = scope.getMarker();
                oldMarker = null;
                scope.setMarker(null);
                var marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    animation: false,
                    position: panorama.getPosition(),
                    optimized: false,
                    zIndex: 5
                });
                scope.setMarker(marker);
                //marker.setPosition(panorama.getPosition());
                
                var markerLatLng = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
                
                var geocoder = scope.getGeocoder();
                    
                geocoder.geocode({
                    latLng: markerLatLng
                }, function(responses) {
                    var decodedAddress = null;
                    if (responses && responses.length > 0) {
                        decodedAddress = responses[0].formatted_address.replace(', Magyarország', '');
                    } else {
                        decodedAddress = 'Ezen a pozíción nem határozható meg cím.';
                    }
                    
                    var infowindow = new google.maps.InfoWindow({
                            content: '<p id="currentAddressP" class="currentAddress">' + decodedAddress + '</p><a href="javascript:algida.app.getController(\'MapsController\').setAltAddress();"><b id="copyToAlt">Másolás egyedi címnek!</b></a>',
                            size: new google.maps.Size(300, 300),
                            width: 200
                    });

                    infowindow.open(map, marker);
                });
                
                console.log('position set 02');
            }
            
            google.maps.event.clearListeners(marker, "dragend");
            google.maps.event.addListener(marker, "dragend", function(event) { 
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();

                var newLatLng = new google.maps.LatLng(lat, lng);

                sv.getPanoramaByLocation(newLatLng, scope.getStreetViewSensitivity(), function(data, status){
                    scope.handlePanoramaResult(data, status, scope, map, sv);
                });
                
                currentRecord.data.posaddressmap.latitude = String(marker.getPosition().lat());
                currentRecord.data.posaddressmap.longitude = String(marker.getPosition().lng());
            });

        }
        
});




