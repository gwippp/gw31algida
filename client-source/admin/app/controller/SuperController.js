

Ext.define('algida.controller.SuperController', {
        
	extend: 'Ext.app.Controller',
        
        requires: [
            'algida.view.components.DepoList'
        ],
        
	config: {
                
		routes: {
			'depolist': 'actionDepolist'
		},

		refs: {
                        depoList: 'depolist',
                        mainTabPanel: 'maintabpanel'
		},

		control: {
                        depoList: {
                            itemtap: 'onDepoListItemTap'
                        }
		}
	},
        
	init: function() {
            this.callParent(arguments);
	},
        
        onLocationListShow: function(){
            
        },
        
        actionDepolist: function(){
            
            var mainTabPanel = this.getMainTabPanel();
            var activeItem = mainTabPanel.getActiveItem();
            var activeItemIndex = mainTabPanel.items.indexOf(activeItem);
            var scope = this;
            var currentRoute = algida.util.Config.getCurrentRoute(this.getApplication());
            
            if(activeItemIndex !== 4){
                mainTabPanel.setActiveItem(4);
                if(algida.util.Config.getUser() === false){
                    this.getApplication().redirectTo('');
                    return;
                }
                
                var extraParams = this.getDepoList().getStore().getProxy().getExtraParams();
                
                if(algida.util.Config.getObjectSize(extraParams) === 0){                
                    this.getDepoList().getStore().getProxy().setExtraParams ({
                        appId: algida.util.Config.getAppId()
                    });
                }else{
                    this.getDepoList().getStore().getProxy().setExtraParams(extraParams);
                }                
                
                this.getDepoList().getStore().load();
                
            }
            
        },
        
        onDepoListItemTap: function(depoList, index, target, record){
            var mainController = this.getApplication().getController('MainController');
            mainController.setCurrentDepo(record);
            this.getApplication().redirectTo('locationList');
        }
        
});
