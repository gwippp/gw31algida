

Ext.define('algida.controller.MainController', {
        
	extend: 'Ext.app.Controller',

	config: {
                
		routes: {
			'locationList': 'actionIndex'
		},

		refs: {
			locationList: 'locationlist',
                        mainTabPanel: 'maintabpanel',
                        locationListFormSubmitButton: 'button[alias=locationListFormSubmitButton]',
                        listFilterForm: 'listfilterform',
                        keywordField: 'textfield[alias=keywordField]',
                        bottomListButton: 'button[alias=bottomListButton]',
                        topListButton: 'button[alias=topListButton]',
                        algidaLogo: 'container[alias=algidaLogo]',
                        backToDepoListButton: 'button[alias=backToDepoList]',
                        csvDownloader: 'container[alias=csv-downloader]',
                        generateReportButton: 'button[alias=generateReportButton]',
                        generateFrom: 'datepickerfield[alias=generateFrom]',
                        generateUntil: 'datepickerfield[alias=generateUntil]'
		},

		control: { 
                        mainTabPanel: {
                            activeitemchange: 'onMainTabPanelActiveItemChange'
                        },
			locationList: {
                            itemtap: 'onLocationListItemTap',
                            show: 'onLocationListShow'
                        },
                        listFilterForm: {
                            action: 'onLocationListFormSubmitButtonTap'
                        },
                        keywordField: {
                            change: 'onKeywordFieldChange'
                        },
                        locationListFormSubmitButton: {
                            tap: 'onLocationListFormSubmitButtonTap'
                        },
                        bottomListButton: {
                            tap: 'onBottomListButtonTap'
                        },
                        topListButton: {
                            tap: 'onTopListButtonTap'
                        },
                        backToDepoListButton: {
                            tap: 'onBackToDepoListButtonTap'
                        },
                        generateReportButton: {
                            tap: 'onGenerateReportButtonTap'
                        }
		},
                currentDepo: false
	},
        
	init: function() {
            this.callParent(arguments);
	},
        
        onLocationListShow: function(){
            
        },
        
        onGenerateReportButtonTap: function(){
            var generateFrom = this.getGenerateFrom();
            var generateUntil = this.getGenerateUntil();
            
            var from = generateFrom.getValue().getFullYear() + '-' + (generateFrom.getValue().getMonth() + 1) + '-' + generateFrom.getValue().getDate();
            var until = generateUntil.getValue().getFullYear() + '-' + (generateUntil.getValue().getMonth() + 1) + '-' + generateUntil.getValue().getDate();
            
            if(Ext.browser.name !== 'Safari'){
                var win = window.open(algida.util.Config.getServer() + '/logger/downloadlogs' + '?appId=' + algida.util.Config.getAppId() + '&from=' + from + '&to=' + until, '_blank');
                win.focus();
            }else{
                var win = window.open(algida.util.Config.getServer() + '/logger/downloadlogs' + '?appId=' + algida.util.Config.getAppId() + '&from=' + from + '&to=' + until, '_blank');
                win.focus();                
            }
        },
                
        onBottomListButtonTap: function(){
            var locationListStore = this.getLocationList().getStore();
            locationListStore.nextPage();
            
            if(this.getLocationList().getStore().currentPage < algida.util.Config.getCurrentMaxPages()){
                this.getBottomListButton().show();
            }else{
                this.getBottomListButton().hide();
            }
            
            if(this.getLocationList().getStore().currentPage === 1){
                this.getTopListButton().hide();
            }else{
                this.getTopListButton().show();
            }
        },
        
        onTopListButtonTap: function(){
            var locationListStore = this.getLocationList().getStore();
            locationListStore.previousPage();
            
            if(this.getLocationList().getStore().currentPage === 1){
                this.getTopListButton().hide();
            }else{
                this.getTopListButton().show();
            }
            
            if(this.getLocationList().getStore().currentPage < algida.util.Config.getCurrentMaxPages()){
                this.getBottomListButton().show();
            }else{
                this.getBottomListButton().hide();
            }
        },
        
        onKeywordFieldChange: function(keywordField, newValue, oldValue, eOpts){
            if(newValue === ''){
                var formData = this.getListFilterForm().getValues();
                
                var extraParams = {
                    appId: algida.util.Config.getAppId(),
                    locstat: formData.approved,
                    search: formData.name
                };

                if(this.getCurrentDepo() !== false){
                    extraParams.depoid = this.getCurrentDepo().get('resourceId');
                }
                
                this.getLocationList().getStore().getProxy().setExtraParams (extraParams);
                this.getLocationList().getStore().currentPage = 1;
                this.getTopListButton().hide();
                this.getLocationList().getStore().load();
            }
        },
        
        onLocationListFormSubmitButtonTap: function(){
            var formData = this.getListFilterForm().getValues();
            
            var extraParams = {
                appId: algida.util.Config.getAppId(),
                locstat: formData.approved,
                search: formData.name
            };
            
            if(this.getCurrentDepo() !== false){
                extraParams.depoid = this.getCurrentDepo().get('resourceId');
            }
            
            this.getLocationList().getStore().getProxy().setExtraParams (extraParams);
            this.getLocationList().getStore().currentPage = 1;
            this.getTopListButton().hide();
            this.getLocationList().getStore().load();
        },
        
        onMainTabPanelActiveItemChange: function(mainTabPanel, activeItem, oldActiveItem){
            var activeItemIndex = mainTabPanel.items.indexOf(activeItem);
            var currentRoute = algida.util.Config.getCurrentRoute(this.getApplication());
            
            try{
                if(currentRoute !== mainTabPanel.getAt(activeItemIndex).config.expectedUrl){
                    var items = mainTabPanel.getItems().items;
                    for(var i in items){
                        var item = items[i];
                        if(item.config.expectedUrl === currentRoute){
                            mainTabPanel.setActiveItem(item);
                            return;
                        }
                    }
                }                
            }catch(err){
                
            }
            
            if(activeItemIndex === 3){
                if(currentRoute !== 'maps'){
                    this.getApplication().redirectTo('maps');
                    return;
                }
            }
            
            if(activeItemIndex === 2){
                if(currentRoute !== 'locationList'){
                    this.getApplication().redirectTo('locationList');
                    return;
                }
            }
        },
        
        actionIndex: function(){
            var mainTabPanel = this.getMainTabPanel();
            var activeItem = mainTabPanel.getActiveItem();
            var activeItemIndex = mainTabPanel.items.indexOf(activeItem);
            var scope = this;
            var currentRoute = algida.util.Config.getCurrentRoute(this.getApplication());
            var generateFrom = this.getGenerateFrom();
            var generateUntil = this.getGenerateUntil();
            generateFrom.setValue(new Date());
            generateUntil.setValue(new Date());
            
            this.getLocationList().deselect(this.getLocationList().getSelection());
            
            var formData = this.getListFilterForm().getValues();
            
            if(activeItemIndex !== 1){
                mainTabPanel.setActiveItem(1);
                if(algida.util.Config.getUser() === false){
                    this.getApplication().redirectTo('');
                    return;
                }
                
                var extraParams = this.getLocationList().getStore().getProxy().getExtraParams();
                
                extraParams.locstat = formData.approved;
                extraParams.search = formData.name;
                extraParams.appId = algida.util.Config.getAppId();
                
                if(algida.util.Config.getObjectSize(extraParams) === 0
                   && this.getCurrentDepo() === false){             
                    var customParams = {
                        agentid: algida.util.Config.getUser().data.username,
                        appId: algida.util.Config.getAppId(),
                        locstat: formData.approved,
                        search: formData.name
                    };
                    this.getLocationList().getStore().getProxy().setExtraParams (customParams);
                    this.getAlgidaLogo().show();
                    this.getBackToDepoListButton().hide();
                }else if(this.getCurrentDepo() !== false){
                    this.getAlgidaLogo().hide();
                    this.getBackToDepoListButton().show();
                    var customParams = {
                        appId: algida.util.Config.getAppId(),
                        depoid: this.getCurrentDepo().get('resourceId'),
                        locstat: formData.approved,
                        search: formData.name
                    };
                    this.getLocationList().getStore().getProxy().setExtraParams(customParams);
                }else{
                    this.getAlgidaLogo().show();
                    this.getBackToDepoListButton().hide();
                    this.getLocationList().getStore().getProxy().setExtraParams (extraParams);
                }
                //superuser has to setup a depo id to see this list
                var user = algida.util.Config.getUser();

                var currentUserRole = user.get('userroles');
                
                if((algida.util.Helper.arrayContains(currentUserRole, 'ROLE_SUPERADMIN') || algida.util.Helper.arrayContains(currentUserRole, 'ROLE_CARELINE')) && this.getCurrentDepo() === false){
                    this.getApplication().redirectTo('depolist');
                    return;
                }
                
                if(!algida.util.Helper.arrayContains(currentUserRole, 'ROLE_SUPERADMIN')){
                    var csvDownloader = this.getCsvDownloader();
                    csvDownloader.setHidden(true);
                }
                
                if(algida.util.Helper.arrayContains(currentUserRole, 'ROLE_SUPERADMIN')){
                    var csvDownloader = this.getCsvDownloader();
                    csvDownloader.setHidden(false);
                    
                }
                //superuser has to setup a depo id to see this list
                
                this.getLocationList().getStore().load();
                
                this.getLocationList().getStore().on('load', function(){
                    if(scope.getLocationList().getStore().currentPage < algida.util.Config.getCurrentMaxPages()){
                        scope.getBottomListButton().show();
                    }else{
                        scope.getBottomListButton().hide();
                    }
                });
                
                if(this.getLocationList().getStore().currentPage === 1){
                    this.getTopListButton().hide();
                }
            }
        },
        
        onBackToDepoListButtonTap: function(){
            var locationListStore = this.getLocationList().getStore();
            locationListStore.currentPage = 1;
            this.getApplication().redirectTo('depolist');
        },
        
        onLocationListItemTap: function(locationList, index, target, record){
            var mapsController = this.getApplication().getController('MapsController');
            mapsController.setOriginalPosition(false);
            mapsController.setCurrentRecord(record);
            this.getApplication().redirectTo('maps');
        }
        
});
