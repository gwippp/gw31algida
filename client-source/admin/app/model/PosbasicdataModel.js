
Ext.define('algida.model.PosbasicdataModel', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: '!id',
        fields: [
            'agentid',
            {name: '!id', convert: function(value, record){
                    return value;
            }},
            'loctypename',
            'ownername',
            'loctypecode',
            'active',
            'innerid',
            'security',
            'aspectname',
            'depoid',
            'locname',
            'uniqueid',
            'object'
        ],
        belongsTo: [{ model: 'algida.model.GWResource', associationKey: 'object' }]
    }
});
