
Ext.define('algida.model.LocationBasicDataModel', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: '!id',
        fields: [
            'uniqueid',
            'aspectname',
            'description',
            'loctypename',
            'name',
            {name: '!id', convert: function(value, record){
                    return value;
            }},
            'object',
            'security',
            'innerid',
            'locname'
        ],
        belongsTo: [{ model: 'algida.model.LocationModel', associationKey: 'object' }]
    }
});
