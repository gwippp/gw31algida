

Ext.define('algida.model.PosaddressmapModel', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: '!id',
        fields: [
            'uniqueid',
            'aspectname',
            'address',
            {name: '!id', convert: function(value, record){
                    return value;
            }},
            'object',
            'security',
            'approved',
            'address',
            'heading',
            'latitude',
            'street1',
            'pitch',
            'longitude'
        ],
        belongsTo: [{ model: 'algida.model.GWResource', associationKey: 'object' }]
    }
});
