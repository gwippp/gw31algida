



Ext.define('algida.model.GWError', {
    extend: 'algida.model.BaseModel',
    
    config: {
        //idProperty: 'resourceId',
        fields: [
            'errorlevel',
            'message',
            '!id',
            'errorcode',
            'path'
        ]
    },
    
    constructor: function(config) {
        this.initConfig(config);
        this.callParent(arguments);
    }
});




