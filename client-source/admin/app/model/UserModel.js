

Ext.define('algida.model.UserModel', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: '!id',
        fields: [
            '!id',
            'aspects',
            'createdon',
            'email',
            'enabled',
            'firstname',
            'language',
            'lastlogin',
            'lastname',
            'mustchangepassword',
            'userid',
            'username',
            'usertype',
            'userroles'
        ]
    }
});

