

Ext.define('algida.model.BaseModel', {
    extend: 'Ext.data.Model',
    
    
    constructor: function(config) {
        this.initConfig(config);
        
        this.callParent(arguments);
    }
});

