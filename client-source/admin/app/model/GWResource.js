

Ext.define('algida.model.GWResource', {
    extend: 'Ext.data.Model',
    config: {
        //idProperty: 'resourceId',
        fields: [
            '!id',
            'aspects',
            'relationship',
            'createdon',
            'resourcetype',
            'title',
            'resourceId',
            'resourcetype',
            'shortDescription',
            {name: 'description', convert: function(value, record){
                    if(typeof value !== 'undefined'){
                        record.set('shortDescription', algida.util.Helper.shorten(value, 100, true));
                    }
                    return value;
            }},
            'active',
            'resourceimageinfo',
            {name: 'resourceimage', convert: function(value, record){
                    return value;
            }},
            'posbasicdata',
            'posaddressmap'
        ]
    },
    
    constructor: function(config) {
        this.initConfig(config);
        
        this.callParent(arguments);
    }
});