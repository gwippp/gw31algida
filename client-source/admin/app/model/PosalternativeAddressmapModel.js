

Ext.define('algida.model.PosalternativeAddressmapModel', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: '!id',
        fields: [
            "!id",
            "address",
            "aspectname",
            "object",
            "security",
            "uniqueid"
        ]
    }
});

