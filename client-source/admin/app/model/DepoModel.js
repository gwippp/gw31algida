



Ext.define('algida.model.DepoModel', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: '!id',
        fields: [
            'approved',
            'locationtype',
            'locationId',
            'aspects',
            '!id'
        ]
    }
});


