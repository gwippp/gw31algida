

Ext.define('algida.util.Helper', {
    requires: [
    ],
    
    singleton: true,
    
    getAspect: function(object, aspectName){
        var aspect = false;
        
        for(i in object.aspects){
            var currentAspect = object.aspects[i];
            if(currentAspect['!id'].indexOf(aspectName + ':') !== -1){
                aspect = currentAspect;
            }
        }
        
        return aspect;
    },
    
    shorten: function(string, n, useWordBoundary){
        var toLong = string.length > n,
                s_ = toLong ? string.substr(0, n - 1) : string;
        s_ = useWordBoundary && toLong ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
        return  toLong ? s_ + '&hellip;' : s_;
    },
    
    deserializeFlatData: function (flatObjects) {
        var objects = {};

        for (i in flatObjects) {
            var currentFlatObject = flatObjects[i];
            objects[currentFlatObject['!id']] = currentFlatObject;
        }

        this.iterateRecursively(flatObjects, objects);

        return flatObjects[0];
    },
    iterateRecursively: function (flatObjects, objects) {
        for (j in flatObjects) {
            if (j !== '!id') {
                var currentObject = flatObjects[j];
                if (this.isArray(currentObject) || this.isObject(currentObject)) {
                    this.iterateRecursively(currentObject, objects);
                } else if (this.isString(currentObject) && objects.hasOwnProperty(currentObject)) {
                    flatObjects[j] = objects[currentObject];
                }
            }
        }
    },
    isArray: function (element) {
        if (Object.prototype.toString.call(element) === '[object Array]') {
            return true;
        }
        return false;
    },
    isObject: function (element) {
        if (Object.prototype.toString.call(element) === '[object Object]') {
            return true;
        }
        return false;
    },
    isString: function (element) {
        if (Object.prototype.toString.call(element) === '[object String]') {
            return true;
        }
        return false;
    },
    
    arrayContains: function(array, element){
        try{
            for (var i = 0; i < array.length; i++) {
                if (array[i] === element) {
                    return true;
                }
            }
        }catch(err){
            console.log(err);
        }
        return false;
    }


});

