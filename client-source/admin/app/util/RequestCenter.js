
Ext.define('algida.util.RequestCenter', {
    
    requires: [
        
    ],
    
    singleton : true,
    
    config : {
        
        messages: null
        
    },
    
    constructor: function(config) {
        this.initConfig(config);
        
        this.callParent(arguments);
    },
    
    generateErrorWindow: function(errorStore, title){
        var message = "";
        
        if(errorStore !== null){        
            errorStore.each(function(record){
                message += '<p class="' + record.get('errorlevel') + '">' + record.get("message") + '</p>' + "\n";
            });
            
            //Ext.Msg.alert(title, message);
            
            Ext.Msg.show({
                title: title,
                message: message,
                //cls: 'x-container x-unsized x-msgbox-dark x-msgbox x-floating messagepopup',
                masked: false,
                buttons: [{
                        id: 'alertCancelBtn',
                        text: 'Ok!'
                    }],
                showAnimation: false
            });
            
        }
        var maskCmp = Ext.getCmp('ext-mask-1');
        if(typeof maskCmp !== 'undefined'){
            maskCmp.destroy();
        }
    },
    
    addErrorClassToForm: function(form, errorStore){
        if(errorStore !== null){
            errorStore.each(function(record){
                if(typeof record.get('path') !== 'undefined'){
                    var formFieldName = record.get('path');
                    if(typeof formFieldName.split !== 'undefined'){                
                        var formFieldNameParts = formFieldName.split('_');

                        var remainingPath = null;

                        if(formFieldNameParts.length > 1){
                            formFieldName = 'GWAspect' + formFieldNameParts[0].charAt(0).toUpperCase() + formFieldNameParts[0].slice(1);
                            remainingPath = formFieldNameParts;
                            remainingPath.shift();
                        }

                        form.getItems().each(function(item){
                            if(typeof item.getName !== 'undefined'){
                                if(item.getName() === formFieldName){
                                    if(typeof record.get('errorlevel') !== 'undefined'){
                                        item.addCls(record.get('errorlevel'));
                                        item.config.currentErrorLevel = record.get('errorlevel');
                                    }
                                }
                            }
                            if(remainingPath !== null && typeof item.config.model !== 'undefined' && item.config.model === formFieldName){
                                record.set('path', remainingPath.join('_'));
                                algida.util.RequestCenter.digWithMessage(record, item);
                            }
                        });
                    }
                }
            });
        }
    },
    
    digWithMessage: function(errorRecord, item){
        var found = false;
        if(typeof item.getItems !== 'undefined'){
            item.getItems().each(function(innerItem){
                var formFieldName = errorRecord.get('path');
                if(typeof innerItem.getName !== 'undefined'){
                    if(innerItem.getName() === formFieldName){
                        if(typeof errorRecord.get('errorlevel') !== 'undefined'){
                            innerItem.addCls(errorRecord.get('errorlevel'));
                            innerItem.config.currentErrorLevel = errorRecord.get('errorlevel');
                            found = true;
                        }
                    }
                }
            });
            if(!found){
                item.getItems().each(function(innerItem){
                    algida.util.RequestCenter.digWithMessage(errorRecord, innerItem);
                });
            }
        }else{
            
            if(typeof item.getName !== 'undefined' && item.getName() === errorRecord.get('path')){
                if(typeof errorRecord.get('errorlevel') !== 'undefined'){
                    item.addCls(errorRecord.get('errorlevel'));
                    item.config.currentErrorLevel = errorRecord.get('errorlevel');
                    found = true;
                }
            }
        }
    },
    
    removeErrorsFromForm: function(form){
        form.getItems().each(function(item){
            if(typeof item.getName !== 'undefined' && typeof item.config.currentErrorLevel !== 'undefined'){
                item.removeCls(item.config.currentErrorLevel);
                delete item.config.currentErrorLevel;
            }
        });
    },
    
    doRequest: function(request){
        if(typeof request.masked !== 'undefined'){
            request.masked.setMasked({
                xtype: 'loadmask',
                message: 'Loading...'
            });
        }
        request.values.appId = algida.util.Config.getAppId();
        
        var async = true;
        
        if(typeof request.async !== 'undefined'){
            async = request.async;
        }
        
        Ext.Ajax.request({
            url: request.url,
            async: async,
            params: request.values,
            success: function(response){
                if(typeof request.masked !== 'undefined'){
                    request.masked.setMasked(false);
                }
                
                var processedResponse = algida.util.RequestCenter.processResponseMessages(response);
                
                if(processedResponse === true){               
                    if(typeof request.success !== 'undefined'){
                        request.success.call(request.scope, response);
                    }
                }else{
                    if(typeof request.success !== 'undefined'){
                        request.failure.call(request.scope, response);
                    }
                }
                
                algida.util.RequestCenter.setMessages(null);
            },
            failure: function(response){
                
                if(response.status !== 0){
                    if(typeof request.masked !== 'undefined'){
                        request.masked.setMasked(false);
                    }
                    if(typeof request.success !== 'undefined'){
                        request.failure.call(request.scope, response);
                    }
                }else if(response.status === 0){
                    request.masked.setMasked(false);
                    Ext.Msg.show({
                        title: 'Server is unreachable',
                        showAnimation: false
                    });
                }
                if(response.status === 403){
                    //algida.app.getController('BaseController').setRequestBeforeLogin(window.location.hash);
                    //algida.app.getController('AuthenticationController').actionLogin();
                }
                
            }
        });
    },
    
    getErrorLevel: function(level){
        var errorlevels = {
            'INFO': 0,
            'WARN': 1,
            'ERROR': 2,
            'FATAL': 3
        };
        return errorlevels[level];
    },
    
    checkForErrors: function(responseObject){
        var error = false;
        
        if(typeof responseObject.errors !== 'undefined'){
            var errorLevel = 0;
            var fieldErrors = responseObject.errors[0].fielderrors;
            var globalErrors = responseObject.errors[0].globalerrors;
            for(i in responseObject.errors){
                var currentErrorResponseObject = responseObject.errors[i];
                if(typeof currentErrorResponseObject.errorlevel !== 'undefined' && this.getErrorLevel(currentErrorResponseObject.errorlevel) > errorLevel){
                    errorLevel = this.getErrorLevel(currentErrorResponseObject.errorlevel);
                }
            }
            
            if(errorLevel > 1){
                error = true;
            }
        }
        
        return error;
    },
    
    processResponseMessages: function(response){
        var errors = false;
        var responseObject = Ext.decode(response.responseText);
        
        if(this.checkForErrors(responseObject) === true){        
            var rootStore = null;
            if(typeof responseObject.errors !== 'undefined' && responseObject.errors.length > 0){
                var error = algida.util.Helper.deserializeFlatData(responseObject.payload);
                var errorStore = Ext.create('algida.store.GWErrorStore');
                errorStore.add(error);
                this.setMessages(errorStore);
            }
        }else{
            errors = true;
        }
        
        return errors;
    }
    
});
