
Ext.define('algida.util.Config', {
    
    requires: [
        
    ],
    
    singleton : true,
    
    config : {
        defaultLang: 'hu',
        user: false,
        //server: 'http://localhost:8080/gw3',
        //server: 'http://' + document.location.host + '/gw3',
        server: (function(){var protocol = 'http'; if(location.protocol.indexOf('https') !== -1){protocol = 'https';}; return protocol;})() + '://' + document.location.host + '/gw3algida',
        appId: 'GW31-ROCK-EBE3-ALGI',
        currentMaxPages: 1,
        numHits: 0,
        pageSize: 0,
        startIdx: 0
    },

    constructor: function(config) {
        this.initConfig(config);
        this.callParent(arguments);
    },
            
    setUser: function(user){
        if(user !== false && user !== null){
            this.config.user = user;
            localStorage.setItem('currentUser', Ext.encode(user.data));
        }
        if(user === null){
            localStorage.setItem('currentUser', null);
            this.config.user = false;
        }
    },
            
    getUser: function(){
        if(this.config.user !== false){
            return this.config.user;
        }else{
            var storedUser = Ext.decode(localStorage.getItem('currentUser'));
            if(storedUser !== null){
                var user = Ext.create('algida.model.UserModel', storedUser);
                algida.util.Config.setUser(user);
            }
        }
        return this.config.user;
    },
            
    getCurrentRoute: function(app){
        var history = app.getHistory(); 
        var actions = history.getActions();
        var currentRoute = '';
        try{
            var currentRoute = actions[actions.length - 1].config.url;
        }catch(err){
            
        }
        return currentRoute;
    },
    
    getObjectSize: function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key))
                size++;
        }
        return size;
    },
            
    getUserRole: function(payload){
        var userRole = false;
        for(var i in payload){
            if(payload[i]['!id'].indexOf("GWUserRole") !== -1){
                userRole = payload[i].rolename;
            }
        }
        
        return userRole;
    },
            
    clone: function(myObj) {
        if (this.isNullOrUndefined(myObj))
            return myObj;
        var objectClone = new myObj.constructor();
        for (var property in myObj)
            if (typeof myObj[property] === 'object')
                objectClone[property] = this.Clone(myObj[property]);
            else
                objectClone[property] = myObj[property];
        return objectClone;
    },
    
    isNullOrUndefined: function(element){
        return (this.isUndefined(element) || element === null );
    },
            
    isUndefined: function(a){
        return (typeof a === 'undefined');
    }
    
});
