

Ext.define('algida.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'maintabpanel',
    requires: [
        'Ext.TitleBar',
        'algida.view.components.LocationList',
        'algida.view.components.DepoList',
        'algida.form.LoginForm',
        'Ext.field.Toggle'
    ],
    
    initialize: function(){
        this.getTabBar().hide();
    },
    
    config: {
        tabBarPosition: 'bottom',
        
        items: [
            {
                expectedUrl: '',
                title: 'Bejelentkezés',
                iconCls: 'user' ,
                scrollable: null,
                items: [
                        {
                            docked: 'top',
                            xtype: 'titlebar',
                            cls: 'algidaHeader',
                            title: 'Bejelentkezés'
                        },
                        {
                            xtype: 'loginform'
                        }
                ]
            },
            {
                expectedUrl: 'locationList',
                title: 'POS Lista',
                iconCls: 'home',
                
                scrollable: null,
                
                layout: 'vbox',
                
                items: [
                        {
                            docked: 'top',
                            xtype: 'titlebar',
                            cls: 'algidaHeader',
                            title: 'Algida Radar',
                            items: [
                                        {
                                                align: 'left',
                                                xtype: 'button',
                                                cls: 'back',
                                                text: 'Vissza',
                                                alias: 'backToDepoList'
                                        },
                                        {
                                            align: 'right',
                                            xtype: 'button',
                                            cls: 'signOut',
                                            text: 'Kijelentkezés',
                                            alias: 'signOutButton'
                                        }
                                    ]
                        },
                        {
                            xtype: 'container',
                            cls: 'algidaLogo',
                            docked: 'top',
                            alias: 'algidaLogo'
                        },
                        {
                            xtype: 'container',
                            left: '50%',
                            alias: 'csv-downloader',
                            cls: 'whiteContainer',
                            margin: '0 0 0 -314',
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'datepickerfield',
                                    name: 'generateFrom',
                                    alias: 'generateFrom',
                                    dateFormat: 'Y-m-d',
                                    picker: {
                                        yearFrom: 2014,
                                        yearTo: 2020
                                    }
                                },
                                {
                                    xtype: 'datepickerfield',
                                    name: 'generateUntil',
                                    alias: 'generateUntil',
                                    dateFormat: 'Y-m-d',
                                    picker: {
                                        yearFrom: 2014,
                                        yearTo: 2020
                                    }
                                },
                                {
                                    xtype: 'button',
                                    cls: 'generateReportButton',
                                    alias: 'generateReportButton',
                                    text: 'Ok!'
                                }
                            ]
                        },
                        {
                            xtype: 'locationlist'
                        }
                ]
            },
            {
                expectedUrl: 'maps',
                title: 'Maps',
                iconCls: 'maps',
                layout: 'fit',
                items: [
                    
                    {
                        layout: 'fit',
                        type: 'vbox',
                        items: [
                                {
                                type: 'vbox',
                                layout: 'fit',
                                flex: 1,
                                items:  [
                                    {
                                        docked: 'top',
                                        xtype: 'titlebar',
                                        cls: 'algidaHeader',
                                        title: 'Maps',
                                        items: [
                                            {
                                                align: 'left',
                                                xtype: 'button',
                                                text: 'Vissza',
                                                alias: 'backToMain'
                                            },
                                            {
                                                align: 'right',
                                                xtype: 'button',
                                                text: 'Mentés',
                                                alias: 'saveNewLocation'
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'container',
                                        
                                        layout:{
                                            align: 'center',
                                            type: 'vbox'
                                        },
                                        
                                        cls: 'mapsSubTitleBar',
                                        
                                        docked: 'top',
                                        
                                        items: [
                                            {
                                                xtype: 'container',
                                                alias: 'infoContainer',
                                                layout: {
                                                    align: 'center',
                                                    type: 'hbox'
                                                },
                                                config: {
                                                    originalHeight: false
                                                },
                                                originalHeight: false,
                                                listeners: {
                                                    painted: function(element){
                                                        var originalHeight = Ext.get(Ext.query('.helpText')[0]).getHeight();
                                                        
                                                        if(typeof Ext.get(Ext.query('.helpText')[0]).originalHeight === 'undefined'){
                                                            Ext.get(Ext.query('.helpText')[0]).originalHeight = originalHeight;
                                                            Ext.get(Ext.query('.helpText')[0]).setHeight(0);
                                                        }
                                                    }
                                                },
                                                cls: 'helpText',
                                                html: '<ul>\n\
                                                        <li>Ha ez az eladási hely tényleg itt van a térképen, akkor állítsd át zöldre a szürke gombot és nyomj Mentés-t!</li>\n\
                                                        <li>Ha az eladási hely kicsit odébb van, akkor a bal oldali térképen mozgasd át a jelet a jó helyszínre, és utána állítsd át a gombot zöldre és nyomj Mentés-t!</li>\n\
                                                        <li>Ha bármilyen technikai kérdésed van, hívd Sonkoly Tamást (Unilever marketing): 0620-579-0414</li>\n\
                                                      </ul>'
                                            },
                                            {
                                                xtype: 'container',
                                                layout: {
                                                    type: 'hbox',
                                                    align: 'center'
                                                },
                                                flex: 1,
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        cls: 'mapsTitleContainer',
                                                        alias: 'mapsTitleContainer',
                                                        html: ''
                                                    },
                                                    {
                                                        xtype: 'button',
                                                        alias: 'addCustomAddress',
                                                        cls: 'addCustomAddress'
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'container',
                                                cls: 'customAddressInputContainer',
                                                alias: 'customAddressInputContainer',
                                                listeners: {
                                                    painted: function(element){
                                                        
                                                        var originalHeight = Ext.get(Ext.query('.customAddressInputContainer')[0]).getHeight();
                                                        
                                                        if(typeof Ext.get(Ext.query('.customAddressInputContainer')[0]).originalHeight === 'undefined'){
                                                            Ext.get(Ext.query('.customAddressInputContainer')[0]).originalHeight = originalHeight;
                                                            Ext.get(Ext.query('.customAddressInputContainer')[0]).setHeight(0);
                                                        }
                                                        
                                                    }
                                                },
                                                layout: {
                                                    type: 'hbox',
                                                    align: 'center'
                                                },
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        cls: 'customAddressAlignContainer',
                                                        layout: {
                                                            type: 'hbox'
                                                        },
                                                        items: [
                                                            {
                                                                xtype: 'textfield',
                                                                cls: 'customAddressInputField',
                                                                alias: 'customAddressInputField',
                                                                width: '100%',
                                                                label: 'Egyedi cím: '                                                                
                                                            },
                                                            {
                                                                xtype: 'button',
                                                                cls: 'locateButton',
                                                                alias: 'locateButton'
                                                            }
                                                        ]
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'container',
                                                layout: {
                                                    type: 'hbox',
                                                    align: 'center'
                                                },
                                                cls: 'toggleContainer',
                                                items: [
                                                    {
                                                        xtype: 'checkboxfield',
                                                        label: 'Nem találtam',
                                                        labelAlign: 'left',
                                                        cls: 'notFoundChecker',
                                                        labelWidth: 115,
                                                        alias: 'notFound'
                                                    },
                                                    {
                                                       xtype: 'togglefield',
                                                       label: 'Jóváhagyva',
                                                       alias: 'approveField',
                                                       width: '6em',
                                                       labelWidth: '50%'
                                                    },
                                                    {
                                                       xtype: 'button',
                                                       cls: 'infoBox',
                                                       alias: 'infoButton'
                                                    },
                                                    {
                                                       xtype: 'button',
                                                       cls: 'resetButton',
                                                       alias: 'resetButton'
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'button',
                                                alias: 'searchOnGoogleButton',
                                                cls: 'searchOnGoogleButton'
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'map',
                                        width: '50%',
                                        id: 'mapView',
                                        flex: 1
                                    },
                                    {
                                        xtype: 'container',
                                        id: 'streetView',
                                        width: '50%',
                                        flex: 1,
                                        left: '50%',
                                        height: '100%'
                                    }
                                ]
                            }
                        ]
                    
                    }
                    
                ]
            },
            
            {
                expectedUrl: 'depolist',
                title: 'DEPO lista',
                iconCls: 'home',
                
                scrollable: null,
                
                layout: 'vbox',
                
                items: [
                        {
                            docked: 'top',
                            xtype: 'titlebar',
                            cls: 'algidaHeader',
                            title: 'DEPO lista',
                            items: [
                                        {
                                            align: 'right',
                                            xtype: 'button',
                                            text: 'Kijelentkezés',
                                            alias: 'signOutButton'
                                        }
                                    ]
                        },
                        {
                            xtype: 'container',
                            cls: 'algidaLogo',
                            docked: 'top'
                        },
                        {
                            xtype: 'depolist'
                        }
                ]
            }
        ]
    }
});
