

Ext.define('algida.view.components.LocationList', {
    extend: 'Ext.List',
    requires: [
        'algida.form.ListFilterForm',
        'algida.store.LocationStore'
    ],
    xtype: 'locationlist',
    initialize : function(list) {
        var scope = this;
        var store = this.getStore();
        
        store.on('refresh', function(){
            scope.refresh();
            if(Ext.os.deviceType === 'Desktop'){
                Ext.query('.locationlist .x-scroll-container.x-size-monitored.x-paint-monitored')[0].style.overflowY = 'scroll';
                Ext.query('.locationlist .x-scroll-container.x-size-monitored.x-paint-monitored')[0].style.overflowX = 'hidden';
                scope.setScrollable(false);
            }
        });
        
        this.callParent();
    },
    config: {
        flex: 1,
        cls: 'locationlist',
        emptyText: 'Nem sikerült betölteni a listaelemeket',
        listeners: {
            painted: function(element){
                //console.log('listening to list paint');
            }
        },
        itemTpl: new Ext.XTemplate(
                '<p>{title}</p>\n\
                <tpl for="posaddressmap">\n\
                <span><tpl for="address">\n\
                    {country}\n\
                    {zipcode}\n\
                    {settlement}</tpl> {street1}</span></tpl>\n\
             <tpl for="posaddressmap">\n\
                <div class="approved<tpl if="approved==\'30\'"> green</tpl>"></div>\n\
             </tpl>'
        ),
        store: {
            xtype: 'locationstore'
        },
        items: [
            {
                docked: 'top',
                xtype: 'component',
                cls: 'storeList',
                html: 'Értékesítési Pontok'
            },        
            {
                xtype: 'listfilterform',
                scrollDock: 'top',
                height: '4.5em',
                width: 'auto',
                docked: 'top'
            },
            {
                xtype: 'container',
                layout: {
                   type: 'hbox',
                   pack: 'center',
                   autoSize: true
                },
                items: [
                    {
                        xtype: 'button',
                        defaultText: 'Előző',
                        text: 'Előző',
                        alias: 'topListButton',
                        cls: 'topListButton'
                    }
                ],
                scrollDock: 'top'
            },
            {
                xtype: 'container',
                layout: {
                   type: 'hbox',
                   pack: 'center',
                   autoSize: true
                },
                scrollDock: 'bottom',
                items: [
                    {
                        xtype: 'button',
                        defaultText: 'Következő',
                        text: 'Következő',
                        alias: 'bottomListButton',
                        cls: 'bottomListButton'
                    }
                ]
            }
        ]
    },

    // destroy retained elements
    destroy: function() {
        this.getStore().destroy();
        this.callParent(arguments);
    }
});

