Ext.define('algida.view.components.DepoList', {
    extend: 'Ext.List',
    xtype: 'depolist',
    requires: ['algida.store.DepoStore'],
    initialize : function(list) {
        var scope = this;
        var store = this.getStore();
        
        store.on('refresh', function(){
            scope.refresh();
            if(Ext.os.deviceType === 'Desktop'){
                Ext.query('.locationlist.depos .x-scroll-container.x-size-monitored.x-paint-monitored')[0].style.overflowY = 'scroll';
                Ext.query('.locationlist.depos .x-scroll-container.x-size-monitored.x-paint-monitored')[0].style.overflowX = 'hidden';
                scope.setScrollable(false);
            }
        });
        
        this.callParent();
    },
    config: {
        flex: 1,
        cls: 'locationlist depos',
        emptyText: 'Nem sikerült betölteni a listaelemeket',
        listeners: {
            painted: function(element){
                //console.log('listening to depo list paint');
            }
        },
        itemTpl: new Ext.XTemplate(
            '<p>{title}</p>'
        ),
        store: {
            xtype: 'depostore'
        },
        items: [
            {
                docked: 'top',
                xtype: 'component',
                cls: 'storeList',
                html: 'DEPO lista'
            }
        ]
    },

    // destroy retained elements
    destroy: function() {
        this.getStore().destroy();
        this.callParent(arguments);
    }
});