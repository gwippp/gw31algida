Ext.define('algida.store.BaseStore', {
    extend: 'Ext.data.Store',
    xtype: 'basestore',
    
    requires: [
    ],
    
    config: {
        model: 'algida.model.BaseModel'
    },
    
    modelFactory: function(data){
        if(typeof data !== 'undefined'){
            this.addData(data);
        }
    }
});