
Ext.define('algida.store.LocationStore', {
    extend: 'Ext.data.Store',
    xtype: 'locationstore',

    requires: [
        'algida.model.LocationModel',
        'algida.model.GWSearchResultModel',
        'algida.model.GWResource'
    ],

    config: {
        pageSize: 20,
        autoLoad : false,
        model: 'algida.model.GWResource',
        listeners: {
            beforeload: function(store){
                
            },
            load: function(store, records, successful, operation, eOpts){
                //console.log('working class', operation);
                if(typeof operation.error !== 'undefined'){                
                    if((operation.error.status === 403)
                       || (operation.error.status === 0)){
                        algida.app.redirectTo('');
                        return;
                    }
                }
                
                var items = [];
                var resultObjects = [];
                
                store.each(function(item) {
                    
                    if(item.get('!id').indexOf('GWSearchResult:') !== -1){
                        var searchResults = Ext.create('algida.model.GWSearchResultModel', item.raw);
                        
                        algida.util.Config.setCurrentMaxPages(Math.ceil(item.raw.numHits / item.raw.pageSize));
                        algida.util.Config.setNumHits(item.raw.numHits);
                        algida.util.Config.setPageSize(item.raw.pageSize);
                        algida.util.Config.setStartIdx(item.raw.startIdx);
                        
                        var bottomListButton = algida.app.getController('MainController').getBottomListButton();
                        var topListButton = algida.app.getController('MainController').getTopListButton();
                        
                        var nextLow = (Number(item.raw.startIdx) + 1 + Number(item.raw.pageSize)).toString();
                        var nextHigh = (Number(item.raw.startIdx) + Number(item.raw.pageSize) * 2).toString();
                        
                        var prevHigh = (Number(item.raw.startIdx)).toString();
                        var prevLow = (Number(item.raw.startIdx) + 1 - Number(item.raw.pageSize)).toString();
                        
                        bottomListButton.setText(bottomListButton.config.defaultText + '  ' + nextLow + ' - ' + nextHigh + ' / ' + item.raw.numHits);
                        topListButton.setText(topListButton.config.defaultText + '  ' + prevLow + ' - ' + prevHigh + ' / ' + item.raw.numHits);
                        
                        resultObjects = searchResults.get('objects');
                        
                        for(i in resultObjects){
                            var locationId = resultObjects[i];
                            var record = store.findRecord('!id', locationId);
                            
                            if(record.get('!id').indexOf('GWResource:') !== -1){
                                var aspects = record.get('aspects');
                                for(j in aspects){
                                    var aspectId = aspects[j];
                                    var aspectRecord = store.findRecord('!id', aspectId);
                                    if(aspectId.indexOf('GWAspectPosbasicdata:') !== -1){
                                        var locationBasicData = Ext.create('algida.model.PosbasicdataModel', aspectRecord.raw);
                                        record.set('posbasicdata', locationBasicData.getData());
                                    }
                                    if(aspectId.indexOf('GWAspectPosaddressmap:') !== -1){
                                        var contactAddressMap = Ext.create('algida.model.PosaddressmapModel', aspectRecord.raw);
                                        record.set('posaddressmap', contactAddressMap.getData());
                                    }
                                    if(aspectId.indexOf('GWAspectPosalternativeaddressmap') !== -1){
                                        var locationAlternativeAddressMap = Ext.create('algida.model.PosalternativeAddressmapModel', aspectRecord.raw);
                                        record.set('posalternativeaddressmap', locationAlternativeAddressMap.getData());
                                    }
                                }
                                items.push(record);
                            }   
                        }
                    }
                });
                
                store.removeAll();
                
                for(i in items){
                    store.add(items[i]);
                }
                
            }
        },
        proxy: {
            type: 'ajax',
            withCredentials: true,
            url: algida.util.Config.getServer() + '/resource/search/posbyagentid',
            method: 'GET',
            reader: {
                type: 'json',
                rootProperty: 'payload'
            }
        }
    }
    
});
