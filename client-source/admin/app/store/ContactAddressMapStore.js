
Ext.define('algida.store.ContactAddressMapStore', {
    extend: 'Ext.data.Store',
    xtype: 'contactaddressmapstore',

    requires: [
        'algida.model.ContactAddressMapModel'
    ],

    config: {
        autoLoad : false,
        model: 'algida.model.ContactAddressMapModel',
        proxy: {
            type: 'jsonp',
            url: algida.util.Config.getServer() + '/location/getlocation?longitude=3023&latitude=3223&appId=GWA3-ROCK-EBE3-ALGI',
            reader: {
                type: 'json',
                rootProperty: 'payload'
            }
        }
    }
    
});
