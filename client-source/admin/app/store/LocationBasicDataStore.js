
Ext.define('algida.store.LocationBasicDataStore', {
    extend: 'Ext.data.Store',
    xtype: 'locationbasicdatastore',

    requires: [
        'algida.model.LocationBasicDataModel'
    ],

    config: {
        autoLoad : false,
        model: 'algida.model.LocationBasicDataModel',
        proxy: {
            type: 'jsonp',
            url: algida.util.Config.getServer() + '/location/getlocation?longitude=3023&latitude=3223&appId=GWA3-ROCK-EBE3-ALGI',
            reader: {
                type: 'json',
                rootProperty: 'payload'
            }
        }
    }
    
});
