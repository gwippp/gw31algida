
Ext.define('algida.store.DepoStore', {
    extend: 'Ext.data.Store',
    xtype: 'depostore',

    requires: [
        'algida.model.DepoModel'
    ],

    config: {
        pageSize: 20,
        autoLoad : false,
        model: 'algida.model.GWResource',
        listeners: {
            beforeload: function(store){
                
            },
            load: function(store, records, successful, operation, eOpts){

                //console.log('working class', operation);
                if(typeof operation.error !== 'undefined'){                
                    if((operation.error.status === 403)
                       || (operation.error.status === 0)){
                        algida.app.redirectTo('');
                        return;
                    }
                }
                
                var items = [];
                var resultObjects = [];
                
                store.each(function(item) {
                    
                    if(item.get('!id').indexOf('GWSearchResult:') !== -1){
                        var searchResults = Ext.create('algida.model.GWSearchResultModel', item.raw);
                        
                        algida.util.Config.setCurrentMaxPages(Math.ceil(item.raw.numHits / item.raw.pageSize));
                        
                        resultObjects = searchResults.get('objects');
                        
                        for(i in resultObjects){
                            
                            var locationId = resultObjects[i];
                            var record = store.findRecord('!id', locationId);
                            
                            if(record.get('!id').indexOf('GWResource:') !== -1){
                                console.log(record);
                                var aspects = record.get('aspects');
                                for(j in aspects){
                                    var aspectId = aspects[j];
                                    var aspectRecord = store.findRecord('!id', aspectId);
                                    if(aspectId.indexOf('GWAspectPosbasicdata:') !== -1){
                                        var locationBasicData = Ext.create('algida.model.PosbasicdataModel', aspectRecord.raw);
                                        record.set('posbasicdata', locationBasicData.getData());
                                    }
                                }
                                items.push(record);
                            }
                            
                        }
                    }
                    
                });
                
                store.removeAll();
                
                for(i in items){
                    store.add(items[i]);
                }
                
            }
        },
        proxy: {
            type: 'ajax',
            withCredentials: true,
            url: algida.util.Config.getServer() + '/resource/search/depo',
            method: 'GET',
            reader: {
                type: 'json',
                rootProperty: 'payload'
            }
        }
    }
    
});
