



Ext.define('algida.store.GWErrorStore', {
    extend: 'algida.store.BaseStore',
    xtype: 'gwerrorsstore',
    
    requires: [
        'algida.model.GWError'
    ],
    
    config: {
        autoLoad: false,
        model: 'algida.model.GWError',
        listeners: {
            beforeload: function(store){
                
            },
            load: function(store, records, successful, operation, eOpts){
                
            }
        }
    }
    
});

