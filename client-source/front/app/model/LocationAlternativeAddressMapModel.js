

Ext.define('algidaFront.model.LocationAlternativeAddressMapModel', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: '!id',
        fields: [
            "!id",
            "address",
            "aspectname",
            "object",
            "security",
            "uniqueid"
        ]
    }
});

