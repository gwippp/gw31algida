

Ext.define('algidaFront.model.LocationModel', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: '!id',
        fields: [
            'cluster',
            'numOfElements',
            'swLat',
            'swLon',
            'neLat',
            'neLon',
            'aspects',
            'longitude',
            'locationId',
            {name: '!id', convert: function(value, record){
                    return value;
            }},
            'latitude',
            'heading',
            'pitch',
            'locationtype',
            'locationbasicdata',
            'contactaddressmaps',
            'approved',
            'locationalternativeaddressmap_altaddress'
        ]
    }
});

