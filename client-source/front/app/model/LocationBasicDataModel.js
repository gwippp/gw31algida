
Ext.define('algidaFront.model.LocationBasicDataModel', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: '!id',
        fields: [
            'uniqueid',
            'aspectname',
            'description',
            'name',
            'loctypename',
            {name: '!id', convert: function(value, record){
                    return value;
            }},
            'object',
            'security',
            'innerid',
            'locname'
        ],
        belongsTo: [{ model: 'algidaFront.model.LocationModel', associationKey: 'object' }]
    }
});
