


Ext.define('algidaFront.util.i18n', {
    requires: [
        
    ],
    
    singleton : true,
    
    config : {
        lang: 'hu_HU'
    },
    
    hu_HU: {
        settings: 'beállítások',
        enter_current_position: 'jelenlegi pozíciód megadása',
        current_position: 'jelenlegi pozíciód',
        type_current_position: 'írd be, hogy hol vagy',
        radar: 'radar',
        algida: 'algida',
        points: 'pontok',
        in_metres: 'méteren belül',
        localization: 'helymeghatározás',
        gps: 'GPS',
        our_address: 'címünk',
        send_feedback: 'visszajelzés küldése',
        beta_test: 'bétateszt',
        your_email_address: 'E-mail címed',
        if_you_require_response: 'ha kérsz visszajelzést',
        message: 'üzenet',
        send_your_feedback: 'küldd el nekünk észrevételeidet',
        help_: 'segíts',
        the: 'az',
        in_development_of_it: 'fejlesztésében',
        i_send_it: 'elküldöm',
        error: 'hiba',
        you_didnt_enter_feedback_text: 'nem adtad meg a visszajelzés szövegét',
        wrong_email_address: 'helytelen e-mail címet adtál meg',
        disclaimer: 'jogi nyilatkozat',
        privacy_policy: 'adatvédelmi nyilatkozat',
        cookie_policy: 'cookie szabályzat',
        your_message_was_sent: 'az üzenete küldésre került',
        accept_terms_before_sending_message: 'Fogadd el a feltételeket az üzenet elküldése előtt',
        determining_gps_location: 'GPS helymeghatározás folyamatban',
        determining_scene: 'helyszínmeghatározás folyamatban',
        determining_current_location: 'aktuális lokációd lekérdezése',
        application_update: 'alkalmazás frissítése',
        application: 'alkalmazás',
        update: 'frissítése',
        new_version_is_available: 'az alkalmazás újabb verziója érhető el',
        do_you_want_to_upgrade: 'frissítesz most',
        news: 'hírek',
        view: 'tovább'
    },
    
    get: function(wordkey, uppercase){
        var translation = '□□□□□□□□□□□□□□□□';
        if(typeof this[this.getLang()][wordkey] !== 'undefined'){
            translation = this[this.getLang()][wordkey];
        }
        if(typeof uppercase !== 'undefined' && uppercase === true){
            translation = this.capitaliseFirstLetter(translation);
        }
        return translation;
    },
    
    capitaliseFirstLetter: function(string){
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

    constructor: function(config) {
        this.initConfig(config);
        this.callParent(arguments);
    }
});


