

Ext.define('algidaFront.view.FeedBackPopUp', {
    extend: 'Ext.Container',
    xtype: 'feedbackpopup',
    cls: 'feedBackPopUp',
    config:
            {
                cls: 'rights',
                modal: true,
                width: '340px',
                layout: {
                    type: 'vbox',
                    align : 'stretch',
                    pack  : 'start'
                },
                items: [
                    /*
                    {
                        flex: 2.5,
                        xtype: 'container',
                        scrollable: true,
                        html: 'Az adatkezelés célja, hogy az Unilever Magyarország Kft. (1138 Budapest, Váci út 182.) (továbbiakban: Unilever) a honlapot üzemeltesse, a honlapon feltett kérdésekre válaszoljon. Az űrlap beküldésével hozzájárul a lokációja megosztásához is. Adataidat az Unilever bizalmasan, a személyes adatok védelmére vonatkozó hatályos jogszabályok előírásainak megfelelően (az információs önrendelkezési jogról és az információszabadságról szóló 2011. évi CXII. törvény, valamint a további vonatkozó jogszabályok rendelkezései szerint) kezeli. Az adatok feldolgozását az Unilever megbízásából a Gollywolly Hungary Kft. (címe: 1065 Budapest, Andrássy út 32.) végzi. Az Unilever személyes adataidat további harmadik személyeknek – ez irányú, megfelelő tájékoztatáson alapuló előzetes hozzájárulásod nélkül – nem adja át. Adatkezelési nyilvántartási szám: 00364-0247. Személyes adataid az adatkezelés céljának megszűnésével egyidejűleg, illetve az esetleges törlésre vonatkozó kérésedet követően haladéktalanul törlésre kerülnek. Az adatszolgáltatás önkéntes, arról tájékoztatást kérhetsz, illetve adataid helyesbítését, törlését bármikor indoklás nélkül kérheted az Unilever Magyarország Kft., 1138 Budapest, Váci út 182. címen; vagy a következő e-mail címen: vevoinfo@unilever.hu.'
                    },
                    */
                    {
                        flex: 1,
                        xtype: 'container',
                        cls: 'feedBackPopUpControls',
                        height: 100,
                        items: [
                            {
                                xtype: 'togglefield',
                                cls: 'feedBackToggleContainer',
                                label: 'Elfogadom',
                                alias: 'feedBackRightsToggle',
                                labelWidth: '75%',
                                value: 0.5,
                                hidden: true
                            },
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                centered: true,
                                cls: 'feedBackButtonContainer',
                                items: [
                                    {
                                        xtype: 'button',
                                        cls: 'feedBackNextButton',
                                        alias: 'feedBackNextButton',
                                        text: 'Elküldöm'
                                    }
                                    /*,
                                    {
                                        xtype: 'button',
                                        cls: 'feedBackNextButton',
                                        alias: 'feedBackCancelButton',
                                        text: 'Mégsem'
                                    }
                                    */
                                ]
                            }
                        ]
                    }
                ]
            }
});
