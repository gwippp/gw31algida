

Ext.define('algidaFront.view.MainMenu', {
    extend: 'Ext.Container',
    xtype: 'mainmenu',
    requires: [
        'Ext.field.Slider'
    ],
    config: {
        cls: 'mainMenu',
        scrollable: true,
        items: [
            {
                docked: 'top',
                xtype: 'titlebar',
                alias: 'settingsTitleDesktop',
                cls: 'algidaHeader settingsHeader',
                title: 'Beállítások',
                items: [
                ]
            },
            {
                xtype: 'container',
                html: 'Beállítások',
                alias: 'settingsTitleMobile',
                cls: 'menuTitle'
            },
            {
                xtype: 'container',
                layout: 'fit',
                cls: 'menuContentContainer',
                items: [
                        {
                            xtype: 'container',
                            html: 'Jelenlegi pozíciód megadása:',
                            cls: 'customAddressLabel'
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            width: '100%',
                            items: [
                                {
                                    xtype: 'textfield',
                                    alias: 'customAddress',
                                    cls: 'customAddress',
                                    placeHolder: 'Írd be, hogy hol vagy!',
                                    flex: 1
                                },
                                {
                                    xtype: 'button',
                                    alias: 'settingsSubmitButton',
                                    cls: 'settingsSubmitButton',
                                    text: 'Ok'
                                }
                            ]
                        },
                        /*
                        {
                            xtype: 'container',
                            cls: 'distanceText',
                            html: 'Algida pontok <span>2000</span> méteren belül',
                            alias: 'distanceText'
                        },
                        {
                            xtype: 'sliderfield',
                            alias: 'distanceSlider',
                            cls: 'distanceSlider',
                            value: 500,
                            minValue: 100,
                            maxValue: 5000
                        },
                        */
                        {
                            xtype: 'container',
                            layout: {
                                        type: 'hbox',
                                        align: 'middle',
                                        pack: 'center'
                                    },
                            items: [
                                {
                                    xtype: 'button',
                                    alias: 'gpsLocateMe',
                                    cls: 'gpsLocateMe',
                                    flex: 0,
                                    text: 'GPS helymeghatározás'
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            items: [
                                {
                                    xtype: 'container',
                                    cls: 'letItBeRed',
                                    html: 'Címünk: <a href="mailto:info@algida.hu">info@algida.hu</a>'
                                },
                                {
                                    xtype: 'label',
                                    cls: 'feedBackTitle',
                                    html: 'Visszajelzés küldése – Algida Radar bétateszt'
                                },
                                {
                                    xtype: 'label',
                                    cls: 'feedBackLabel',
                                    html: 'E-mail címed (ha kérsz visszajelzést):'
                                },
                                {
                                    xtype: 'textfield',
                                    cls: 'feedBackEmail',
                                    alias: 'feedBackEmail'
                                },
                                {
                                    xtype: 'label',
                                    cls: 'feedBackLabel second',
                                    html: 'Üzenet:'
                                },
                                {
                                    xtype: 'textareafield',
                                    cls: 'feedBackText',
                                    alias: 'feedBackText',
                                    maxRows: 3,
                                    value: 'Küldd el nekünk észrevételeidet, segíts az Algida Radar fejlesztésében!'
                                },
                                {
                                    xtype: 'togglefield',
                                    cls: 'feedBackLocation',
                                    label: 'Lokáció küldése',
                                    value: 1,
                                    alias: 'feedBackLocationToggle',
                                    labelWidth: '73%',
                                    hidden: true
                                },
                                {
                                    xtype: 'button',
                                    text: 'Elküldöm',
                                    cls: 'feedBackSubmit',
                                    alias: 'feedBackSubmitButton'
                                },
                                {
                                    xtype: 'container',
                                    cls: 'letItBeRed',
                                    html: '<a href="http://www.algida.hu/hu_hu/LegalPage.aspx">Jogi nyilatkozat</a>'
                                },
                                {
                                    xtype: 'container',
                                    cls: 'letItBeRed',
                                    html: '<a href="http://www.unileverprivacypolicy.com/hu_HU/Policy.aspx">Adatvédelmi elvek</a>'
                                },
                                {
                                    xtype: 'container',
                                    cls: 'letItBeRed',
                                    html: '<a href="http://www.unilevercookiepolicy.com/hu_HU/Policy.aspx">Cookie szabályzat</a>'
                                }
                            ]
                        }
                ]
            }            
        ]
    }
});

