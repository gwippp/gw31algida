/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

Ext.application({
    name: 'algidaFront',

    requires: [
        'Ext.MessageBox',
        'algidaFront.util.LocationHelper',
        'algidaFront.view.FeedBackPopUp',
        'algidaFront.util.Helper',
        'algidaFront.util.Config',
        
        'algidaFront.store.LocationStore'
    ],
    
    controllers: [
        'algidaFront.controller.MainController',
        'algidaFront.controller.MainMenuController',
        'algidaFront.controller.FeedBackController'
    ],

    views: [
        'Main'
    ],

    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function() {
        
        var getParams = document.URL.split("?");
        
        if(getParams.length > 1){
            var params = Ext.urlDecode(getParams[getParams.length - 1]);
            
            if(typeof params['promocio'] !== 'undefined'){
                algidaFront.app.getController('MainController').setSelectedPromotion(params['promocio']);
            }
        }
        
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();
        
        Ext.Msg.defaultAllowedConfig.showAnimation = false;
        
        // Initialize the main view
        Ext.Viewport.add(Ext.create('algidaFront.view.Main'));
        
        
        
        
        
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Alkalmazás Frissítése",
            "Az alkalmazás újabb verziója érhető el. Frissítesz most?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});


document.addEventListener('mousewheel', function(e) {
    var el = e.target;
    var results = [];
    while (el !== document.body) {
        if (el && el.className && el.className.indexOf('x-container') >= 0) {
            var cmp = Ext.getCmp(el.id);
            if (cmp && typeof cmp.getScrollable === 'function' && cmp.getScrollable()) {
                var scroller = cmp.getScrollable().getScroller();
                if (scroller) {
                    var offset = {
                        x: 0,
                        y: -e.wheelDelta * 0.5
                    };
                    scroller.fireEvent('scrollstart', scroller, scroller.position.x, scroller.position.y, e);
                    scroller.scrollBy(offset.x, offset.y);
                    scroller.snapToBoundary();
                    scroller.fireEvent('scrollend', scroller, scroller.position.x, scroller.position.y - offset.y);
                    break;
                }
                results.push(el = el.parentNode);
            }
            return results;
        }
    }
}, false);

