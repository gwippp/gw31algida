
Ext.define('algidaFront.view.Main', {
    extend: 'Ext.Container',
    xtype: 'main',
    requires: [
        'Ext.TitleBar'
    ],
    config: {
        layout: 'fit',
        items: [
            {
                xtype: 'container',
                cls: 'mainContainer',
                alias: 'mainContainer',
                layout: 'fit',
                items: [
                            {
                                docked: 'top',
                                xtype: 'titlebar',
                                alias: 'mainTitleBar',
                                cls: 'algidaHeader',
                                title: 'Algida Radar',
                                items: [
                                    {
                                        align: 'right',
                                        xtype: 'button',
                                        cls: 'refreshButton',
                                        alias: 'refreshButton'
                                    },
                                    {
                                        align: 'left',
                                        xtype: 'button',
                                        cls: 'menuButton',
                                        alias: 'menuButton',
                                        width: '2em'
                                    }
                                ]
                            },
                            {
                                xtype: 'container',
                                cls: 'algidaLogo',
                                docked: 'top',
                                alias: 'algidaLogo'
                            },
                            {
                                xtype: 'container',
                                cls: 'locationInfo',
                                alias: 'locationInfo',
                                hidden: true,
                                layout: {
                                    type: 'hbox',
                                    align: 'middle',
                                    pack: 'center'
                                },
                                items: [
                                    {
                                        xtype: 'container',
                                        alias: 'locationInfoText',
                                        html: 'Helyszínmeghatározás folyamatban...',
                                        cls: 'locationInfoText'
                                    }
                                ]
                            },
                            {
                                xtype: 'container',
                                cls: 'mapContainer',
                                layout: 'fit',
                                alias: 'mapContainer',
                                masked: {
                                    xtype: 'loadmask',
                                    message: 'Aktuális lokációd lekérdezése...'
                                },
                                items: [
                                    {
                                        xtype: 'map',
                                        layout: 'fit',
                                        alias: 'mainMap',
                                        useCurrentLocation: false,
                                        mapOptions: {
                                            navigationControl: true,
                                            center: new google.maps.LatLng(47.4984056, 19.040757799999938)
                                        },
                                        listeners: {
                                            maprender: function(){
                                                algidaFront.app.getController('MainController').addMapListeners();
                                            }
                                        },
                                        items: [
                                            {
                                                xtype: 'container',
                                                cls: 'promotionSelector',
                                                layout: {
                                                    type: 'hbox',
                                                    pack: 'center'
                                                },
                                                maxHeight: 60,
                                                items: [
                                                    {
                                                        xtype: 'selectfield',
                                                        alias: 'promotion-selector',
                                                        value: (function(){                                                            
                                                            var getParams = document.URL.split("?");
                                                            
                                                            if (getParams.length > 1) {
                                                                var params = Ext.urlDecode(getParams[getParams.length - 1]);
                                                                if (typeof params['promocio'] !== 'undefined') {
                                                                    return Number(params['promocio']);
                                                                }
                                                            }
                                                            return 0;
                                                        })(),
                                                        options: [
                                                            {
                                                                text: 'Minden fagylalt lelőhely',
                                                                value: 0
                                                            },
                                                            /*{
                                                                text: 'Ajándék Cornetto lelőhelyek',
                                                                value: 26816
                                                            },*/
                                                            {
                                                                text: 'Minden gombócos fagyi lelőhely',
                                                                value: 28119
                                                            },
                                                            {
                                                                text: 'Minden pálcikás jégkrém lelőhely',
                                                                value: 28120
                                                            }
                                                        ],
                                                        listeners: {
                                                            change: function(){
                                                                algidaFront.app.getController('MainController').setUserSelectedPromotion(this.getValue());
                                                                algidaFront.app.getController('MainController').onRefreshButtonTap.apply(algidaFront.app.getController('MainController'));
                                                            }
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                ]
            }
        ]
    }
});
