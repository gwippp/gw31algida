
Ext.define('algidaFront.store.LocationStore', {
    extend: 'Ext.data.Store',
    xtype: 'locationstore',

    requires: [
        'algidaFront.model.LocationModel',
        'algidaFront.model.GWSearchResultModel'
    ],

    config: {
        pageSize: 20,
        autoLoad : false,
        model: 'algidaFront.model.LocationModel',
        listeners: {
            beforeload: function(store){
                
            }
        },
        proxy: {
            type: 'ajax',
            withCredentials: true,
            url: algidaFront.util.Config.getServer() + '/location/proximitysearch',
            method: 'GET',
            reader: {
                type: 'json',
                rootProperty: 'payload'
            }
        }
    }
    
});
