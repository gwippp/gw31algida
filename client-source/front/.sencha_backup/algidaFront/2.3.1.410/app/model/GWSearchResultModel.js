

Ext.define('algidaFront.model.GWSearchResultModel', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: '!id',
        fields: [
            
            'numHits',
            'objects',
            'pageSize',
            'searchId',
            'startIdx'
            
        ]
    }
});

