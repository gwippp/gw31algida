

Ext.define('algidaFront.model.GWResource', {
    
    extend: 'Ext.data.Model',
    
    config: {
        idProperty: 'resourceId',
        fields: [
            '!id',
            'aspects',
            'relationship',
            'createdon',
            'resourcetype',
            'title',
            'resourceId',
            'resourcetype',
            'shortDescription',
            {name: 'description', convert: function(value, record){
                    return value;
            }},
            'active',
            'resourceimageinfo',
            {name: 'resourceimage', convert: function(value, record){
                    return value;
            }}
        ]
    },
    
    constructor: function(config) {
        this.initConfig(config);
        
        this.callParent(arguments);
    }
});