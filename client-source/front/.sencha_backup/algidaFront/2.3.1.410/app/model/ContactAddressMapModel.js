
Ext.define('algidaFront.model.ContactAddressMapModel', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: '!id',
        fields: [
            'uniqueid',
            'aspectname',
            'address',
            {name: '!id', convert: function(value, record){
                    return value;
            }},
            'object',
            'security'
        ],
        belongsTo: [{ model: 'algidaFront.model.LocationModel', associationKey: 'object' }]
    }
});
