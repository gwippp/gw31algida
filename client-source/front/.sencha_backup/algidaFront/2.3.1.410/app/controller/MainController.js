

Ext.define('algidaFront.controller.MainController', {
	extend: 'Ext.app.Controller',
        
        requires: [
            'algidaFront.store.LocationStore',
            'algidaFront.view.MainMenu'
        ],
        
	config: {
                
		routes: {
			'': 'actionIndex',
                        'promotion/:promotionid': 'actionPromotionChange'
		},

		refs: {
                    mainMap: 'map[alias=mainMap]',
                    refreshButton: 'button[alias=refreshButton]',
                    menuButton: 'button[alias=menuButton]',
                    mainContainer: 'container[alias=mainContainer]',
                    mainMenu: 'mainmenu',
                    mapContainer: 'container[alias=mapContainer]',
                    customAddressField: 'textfield[alias=customAddress]',
                    settingsTitleDesktop: 'titlebar[alias=settingsTitleDesktop]',
                    settingsTitleMobile: 'container[alias=settingsTitleMobile]',
                    titleBar: 'titlebar',
                    algidaLogo: 'container[alias=algidaLogo]',
                    locationInfo: 'container[alias=locationInfo]',
                    locationInfoText: 'container[alias=locationInfoText]',
                    promotionSelector: 'selectfield[alias=promotion-selector]'
		},

		control: {
                    refreshButton: {
                        'tap' : 'onRefreshButtonTap'
                    },
                    menuButton: {
                        'tap' : 'onMenuButtonTap'
                    },
                    promotionSelector: {
                        
                    }
		},
                
                positionOptions: {
                    enableHighAccuracy: true
                },
                
                userMarker: false,
                posMarkers: false,
                distance: 2000,
                posListStore: false,
                infoWindows: false,
                currentInfoWindow: false,
                menuOpened: false,
                customLocation: false,
                center: true,
                noLocation: false,
                firstPanoSet: true,
                motionSize: 0,
                routeSearch: null,
                locFailText: 'Kapcsold be a GPS helymeghatározást,<br />hogy a Radar számára engedélyezd<br />a helyzeted megállapítását.',
                adressAspect: 'GWAspectPosaddressmap',
                selectedPromotion: 0,
                userSelectedPromotion: 0,
                requestingLocation: false
	},
        
	init: function() {
            this.callParent(arguments);
	},
        
        actionPromotionChange: function(promotionId){
            var promotionSelector = this.getPromotionSelector();
            
            if(promotionSelector.getValue() !== promotionId){
                //promotionSelector.setValue(promotionId);
            }
            
            this.setUserSelectedPromotion(promotionId);
            this.onRefreshButtonTap();
        },
        
        addMapListeners: function (){
            var scope = this;
            var map = this.getMainMap().getMap();

            google.maps.event.addListener(map, 'dragend', algidaFront.app.getController('MainController').onBoundsChanged);
            google.maps.event.addListener(map, 'zoom_changed', algidaFront.app.getController('MainController').onBoundsChanged);
            
            setTimeout(function () {
                if (scope.getUserMarker() === false) {
                    if (!scope.getMenuOpened()) {
                        scope.onMenuButtonTap();
                    }
                    scope.setNoLocation(true);
                    //Ext.Msg.alert('Sikertelen pozíció lekérdezés', 'Kérlek kapcsold be a GPS lokalizációt készülékeden, vagy add meg pontos helyzeted a beállítások között!');
                    scope.getLocationInfoText().setHtml(scope.getLocFailText());
                }
            }, 15000);
            
            if(scope.getRequestingLocation() === false){
                algidaFront.util.LocationHelper.getCurrentLocation(this.locationResultHandler, this.localizationFailureHandler, this.getPositionOptions(), this);
            }
        },
        
        onBoundsChanged: function () {
            var scope = this;
            
            var map = algidaFront.app.getController('MainController').getMainMap().getMap();

            var bounds = map.getBounds();

            var northEast = bounds.getNorthEast();
            var southWest = bounds.getSouthWest();

            var locationStoreOnLoadCB = algidaFront.app.getController('MainController').posListLoadedHandler;
            
            /*
            * promotion
            */                
           var params = {
               appId: algidaFront.util.Config.getAppId(),
               nelat: northEast.lat(),
               nelng: northEast.lng(),
               swlat: southWest.lat(),
               swlng: southWest.lng()
           };
           
           var promotionSelector = algidaFront.app.getController('MainController').getPromotionSelector();
           
            var selectedPromotion = promotionSelector.getValue();
            
           var userSelectedPromotion = algidaFront.app.getController('MainController').getUserSelectedPromotion();
           
           if(selectedPromotion > 0){
               params.relatedid = selectedPromotion;
               params.resourcetype = 'pos';
           }
           
           if(userSelectedPromotion > 0){
               params.relatedid = userSelectedPromotion;
               params.resourcetype = 'pos';
           }
            
            /*
             * store load has to be removed and replaced with new request and deserializer
             */
            Ext.Ajax.request({
                url: algidaFront.util.Config.getServer() + '/resource/parcelproximitysearch?loadaspect=posalternativeaddressmap&loadaspect=posbasicdata&loadaspect=posadressmap',
                async: true,
                params: params,
                success: function (response) {
                    var responseObjects = Ext.decode(response.responseText).payload;
                    var deserializedData = algidaFront.util.Helper.deserializeFlatData(responseObjects);

                    var store = algidaFront.app.getController('MainController').generateStoreFromResourceObjects(deserializedData.objects);

                    locationStoreOnLoadCB.apply(algidaFront.app.getController('MainController'), [store, 'records...', true, null, null]);
                },
                failure: function (response) {
                    if (response.status !== 0) {
                        if (typeof request !== 'undefined' && typeof request.masked !== 'undefined') {
                            request.masked.setMasked(false);
                        }
                    } else if (response.status === 0) {
                        Ext.Msg.show({
                            title: 'Server is unreachable',
                            showAnimation: false
                        });
                    }
                }
            });

        },
        successfulLocationSearch: function () {

        },
        failedLocationSearch: function () {

        },
        actionIndex: function(){
            var scope = this;
            this.getLocationInfo().setHidden(false);
            this.getLocationInfoText().setHtml('GPS helymeghatározás folyamatban...');
            setTimeout(function () {
                if (scope.getUserMarker() === false) {
                    if (!scope.getMenuOpened()) {
                        scope.onMenuButtonTap();
                    }
                    scope.setNoLocation(true);
                    scope.getLocationInfoText().setHtml(scope.getLocFailText());
                }
            }, 15000);
            
            scope.setRequestingLocation(true);
            
            algidaFront.util.LocationHelper.getCurrentLocation(this.locationResultHandler, this.localizationFailureHandler, this.getPositionOptions(), this);
            
            if (Ext.os.deviceType === 'Desktop') {
                this.onMenuButtonTap(340);
                this.getMenuButton().hide();
                this.getCustomAddressField().setMaxWidth('250px');
                this.getAlgidaLogo().element.setStyle('margin-left', '-425px');
                this.getLocationInfo().element.setStyle('margin-left', '-425px');
            }
        },
        
        onMenuButtonTap: function(customSize){
            var mainContainer = this.getMainContainer();
            var mainElement = mainContainer.element;
            
            var motionSize = /*-1*  we need left to right movement here */(mainElement.getWidth() - 60);
            
            if(motionSize>600){
                motionSize = 600;
            }
            
            if(typeof customSize !== 'undefined'){
                if(motionSize>customSize){
                    motionSize = customSize;
                    this.getMainMap().setMaxWidth(mainElement.getWidth() - motionSize);
                    this.getTitleBar().setMaxWidth(mainElement.getWidth() - motionSize);
                }
            }
            
            this.setMotionSize(motionSize);
            
            if(typeof this.getMainMenu() === 'undefined'){
                var mainMenu = Ext.create('algidaFront.view.MainMenu');
                var mainMenuElement = mainMenu.element;
                mainMenuElement.setStyle('position', 'absolute');
                mainMenuElement.setStyle('left', -1*motionSize + 'px');
                mainMenuElement.setStyle('width', motionSize + 'px');
                mainMenuElement.setStyle('height', '100%');
                
                mainElement.appendChild(mainMenuElement);
                
                if(Ext.os.deviceType === 'Desktop'){
                    this.getSettingsTitleDesktop().show();
                    this.getSettingsTitleMobile().hide();
                }else{
                    this.getSettingsTitleDesktop().hide();
                    this.getSettingsTitleMobile().show();
                }
                
            }
            
            if(typeof customSize !== 'undefined'){
                if(motionSize>=customSize){
                    motionSize = customSize;
                    this.handleMenu(motionSize);
                }
            }
            if(typeof customSize === 'object' || typeof customSize === 'undefined'){
                this.handleMenu();
            }
            
        },
                
        handleMenu: function(customSize){
            var mainContainer = this.getMainContainer();
            var mainElement = mainContainer.element;
            
            var motionSize = /*-1*  we need left to right movement here */(mainElement.getWidth() - 60);
            
            if(motionSize>600){
                motionSize = 600;
            }
            
            if(typeof customSize !== 'undefined'){
                if(motionSize>customSize){
                    motionSize = customSize;
                }
            }
            
            if(this.getMenuOpened() === false){
                mainElement.setStyle('-webkit-transition-duration','0.5s');
                mainElement.setStyle('-webkit-transform', 'translate(' + motionSize + 'px,0px)');
                mainElement.setStyle('-moz-transition-duration','0.5s');
                mainElement.setStyle('-moz-transform', 'translate(' + motionSize + 'px,0px)');
                mainElement.setStyle('-ms-transition-duration','0.5s');
                mainElement.setStyle('-ms-transform', 'translate(' + motionSize + 'px,0px)');
                mainElement.setStyle('-o-transition-duration','0.5s');
                mainElement.setStyle('-o-transform', 'translate(' + motionSize + 'px,0px)');
                mainElement.setStyle('transition-duration','0.5s');
                mainElement.setStyle('transform', 'translate(' + motionSize + 'px,0px)');
                this.setMenuOpened(true);
            }else{
                mainElement.setStyle('-webkit-transition-duration','0.5s');
                mainElement.setStyle('-webkit-transform', 'translate(0px,0px)');
                mainElement.setStyle('-moz-transition-duration','0.5s');
                mainElement.setStyle('-moz-transform', 'translate(0px,0px)');
                mainElement.setStyle('-ms-transition-duration','0.5s');
                mainElement.setStyle('-ms-transform', 'translate(0px,0px)');
                mainElement.setStyle('-o-transition-duration','0.5s');
                mainElement.setStyle('-o-transform', 'translate(0px,0px)');
                mainElement.setStyle('transition-duration','0.5s');
                mainElement.setStyle('transform', 'translate(0px,0px)');
                this.setMenuOpened(false);
            }
        },
        
        onRefreshButtonTap: function(){
            var userMarker = this.getUserMarker();
            if(userMarker){
                userMarker.setMap(null);
            }
            
            var posMarkers = this.getPosMarkers();
            
            for(var i in posMarkers){
                var marker = posMarkers[i];
                marker.setMap(null);
            }
            
            this.setUserMarker(false);
            
            if(this.getCustomLocation() === false){
                algidaFront.util.LocationHelper.getCurrentLocation(this.locationResultHandler, this.localizationFailureHandler, this.getPositionOptions(), this);
            }else{
                
                var customLocation = this.getCustomLocation();
                
                var generatedLocation = {
                    coords: {
                        latitude: customLocation.lat(),
                        longitude: customLocation.lng()
                    }
                };
                
                this.locationResultHandler(generatedLocation);
            }
        },
                
        locationResultHandler: function(position){
            var geocoder = new google.maps.Geocoder();
            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            
            geocoder.geocode({
                latLng: latLng
            }, function (results, status) {
                if(status === 'OK' && typeof results[0] !== 'undefined'){
                    for(var i in results[0].address_components){
                        var currentComponent = results[0].address_components[i];
                        if(Ext.Array.contains(currentComponent.types, 'locality') && Ext.Array.contains(currentComponent.types, 'political')){
                            if(currentComponent.long_name === 'Budapest' && currentComponent.short_name === 'Bp'){
                                ga('send', 'event', 'POS keresés', 'Budapest');
                            }else{
                                ga('send', 'event', 'POS keresés', 'Vidék');
                            }
                        }
                    }
                }
            });
            
            this.getLocationInfo().setHidden(true);
            this.getLocationInfoText().setHtml('GPS helymeghatározás folyamatban...');
            
            if(typeof position.coords !== 'undefined'){
                this.getLocationInfo().setHidden(true);
                
                this.getMapContainer().setMasked(false);
                
                if(this.getCustomLocation() === false && this.getMenuOpened() === true && this.getNoLocation() === true){
                    if(Ext.os.deviceType !== 'Desktop'){
                        this.onMenuButtonTap();
                    }
                    Ext.Msg.hide();
                    this.setNoLocation(false);
                }
                
                var locationStoreOnLoadCB = this.posListLoadedHandler;
                var gPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                var scope = this;
                var map = this.getMainMap().getMap();
                
                var panorama = map.getStreetView();
                
                if(this.getFirstPanoSet() === false){
                    var panorama = map.getStreetView();
                    
                    if(panorama.getVisible() === true){
                        panorama.setPosition(gPosition);
                    }
                }else{
                    var panorama = map.getStreetView();
                    this.setFirstPanoSet(false);
                }
                
                if(this.getUserMarker() === false && typeof this.getMainMap() !== 'undefined'){
                    var marker = new google.maps.Marker({
                        map : map,
                        position: gPosition,
                        title : '',
                        optimized: false,
                        icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                        zIndex: 999
                    });
                    this.setUserMarker(marker);
                    
                    map.panTo(gPosition);
                    
                    var infowindow = new google.maps.InfoWindow({
                            content: '<b class="youAreHere">Jelenlegi pozíciód</b>',
                            size: new google.maps.Size(300, 300),
                            width: 200
                    });
                    
                    infowindow.open(map, marker);
                    
                    this.setCurrentInfoWindow(infowindow);
                    
                }

                if(this.getUserMarker() !== false && typeof this.getMainMap() !== 'undefined'){
                    this.getUserMarker().setPosition(gPosition);
                }
                
                var locationStore = Ext.create('algidaFront.store.LocationStore');
                
                var customAddress = '';
                if(typeof algidaFront.app.getController('MainController').getCustomAddressField !== 'undefined' &&
                   typeof algidaFront.app.getController('MainController').getCustomAddressField() !== 'undefined' &&
                   typeof algidaFront.app.getController('MainController').getCustomAddressField().getValue !== 'undefined'){
                  customAddress = algidaFront.app.getController('MainController').getCustomAddressField().getValue();
                }
                
                var map = algidaFront.app.getController('MainController').getMainMap().getMap();

                var bounds = map.getBounds();

                var northEast = bounds.getNorthEast();
                var southWest = bounds.getSouthWest();
                
                
                /*
                 * promotion
                 */                
                var params = {
                    appId: algidaFront.util.Config.getAppId(),
                    nelat: northEast.lat(),
                    nelng: northEast.lng(),
                    swlat: southWest.lat(),
                    swlng: southWest.lng()
                };
                
                var promotionSelector = this.getPromotionSelector();
                
                var selectedPromotion = promotionSelector.getValue();
                var userSelectedPromotion = this.getUserSelectedPromotion();

                if(selectedPromotion > 0){
                    params.relatedid = selectedPromotion;
                    params.resourcetype = 'pos';
                }

                if(userSelectedPromotion > 0){
                    params.relatedid = userSelectedPromotion;
                    params.resourcetype = 'pos';
                }
                
                /*
                 * store load has to be removed and replaced with new request and deserializer
                 */
                Ext.Ajax.request({
                    url: algidaFront.util.Config.getServer() + '/resource/parcelproximitysearch?loadaspect=posalternativeaddressmap&loadaspect=posbasicdata&loadaspect=posadressmap',
                    async: true,
                    params: params,
                    success: function (response) {
                        var responseObjects = Ext.decode(response.responseText).payload;
                        var deserializedData = algidaFront.util.Helper.deserializeFlatData(responseObjects);

                        var store = scope.generateStoreFromResourceObjects(deserializedData.objects);

                        locationStoreOnLoadCB.apply(algidaFront.app.getController('MainController'), [store, 'records...', true, null, null]);
                    },
                    failure: function (response) {
                        if (response.status !== 0) {
                            if (typeof request !== 'undefined' && typeof request.masked !== 'undefined') {
                                request.masked.setMasked(false);
                            }
                        } else if (response.status === 0) {
                            Ext.Msg.show({
                                title: 'Server is unreachable',
                                showAnimation: false
                            });
                        }
                    }
                });
                
            }
            
        },
                
        posListLoadedHandler: function(store, records, successful, operation, eOpts){
            var items = [];
            var resultObjects = [];

            this.setPosListStore(store);
            
            this.putMarkers();
        },
        
        generateStoreFromResourceObjects: function (objects) {
            var locationStore = Ext.create('algidaFront.store.LocationStore');

            var adressAspect = this.getAdressAspect();

            for (var i in objects) {
                var currentObject = objects[i];
                var convertedObject = {};

                if (currentObject['!id'].indexOf('GWResource:') !== -1) {
                    var addressAspectObject = algidaFront.util.Helper.getAspect(currentObject, adressAspect);
                    var posbasicdataObject = algidaFront.util.Helper.getAspect(currentObject, 'GWAspectPosbasicdata');

                    convertedObject = {
                        'aspects': currentObject.aspects,
                        'longitude': addressAspectObject.longitude,
                        'locationId': currentObject.resourceId,
                        '!id': '',
                        'latitude': addressAspectObject.latitude,
                        'heading': addressAspectObject.heading,
                        'cluster': false,
                        'pitch': addressAspectObject.pitch,
                        'locationtype': posbasicdataObject.loctypename,
                        'locationbasicdata': posbasicdataObject,
                        'contactaddressmaps': addressAspectObject,
                        'approved': addressAspectObject.approved,
                        'locationalternativeaddressmap_altaddress': ''
                    };
                }

                if (currentObject['!id'].indexOf('GWLocationCluster:') !== -1) {
                    convertedObject = {
                        '!id': '',
                        'latitude': currentObject.centerLat,
                        'longitude': currentObject.centerLon,
                        'neLat': currentObject.neLat,
                        'neLon': currentObject.neLon,
                        'numOfElements': currentObject.numOfElements,
                        'swLat': currentObject.swLat,
                        'swLon': currentObject.swLon,
                        'cluster': true
                    };
                }

                locationStore.add(convertedObject);
            }

            return locationStore;
        },     
        putMarkers: function(){        
            var posMarkers = this.getPosMarkers();

            for (var i in posMarkers) {
                var marker = posMarkers[i];
                marker.setMap(null);
            }

            var posMarkers = [];
            var posListStore = this.getPosListStore();

            var map = this.getMainMap().getMap();
            
            var scope = this;

            var currentPositionLat = this.getUserMarker().getPosition().lat();
            var currentPositionLng = this.getUserMarker().getPosition().lng();

            var uPosition = new google.maps.LatLng(currentPositionLat, currentPositionLng);
            
            posListStore.each(function (item) {
                var latitude = Number(item.get('latitude'));
                var longitude = Number(item.get('longitude'));

                var gPosition = new google.maps.LatLng(latitude, longitude);

                var marker = {};

                if (item.get('cluster') === true) {
                    var numOfElements = item.get('numOfElements');
                    var numIndicator = '0';

                    if (numOfElements > 0 && numOfElements <= 10) {
                        numIndicator = '10';
                    }
                    if (numOfElements > 10 && numOfElements <= 100) {
                        numIndicator = '100';
                    }
                    if (numOfElements > 100 && numOfElements <= 1000) {
                        numIndicator = '1000';
                    }
                    if (numOfElements > 1000 && numOfElements <= 10000) {
                        numIndicator = '10000';
                    }
                    if (numOfElements > 10000 && numOfElements <= 100000) {
                        numIndicator = '100000';
                    }
                    if (numOfElements > 100000 && numOfElements <= 1000000) {
                        numIndicator = '1000000';
                    }
                    
                    marker = new google.maps.Marker({
                        map: map,
                        position: gPosition,
                        optimized: false,
                        zIndex: 9,
                        icon: 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=1.7|0|ade6a8|13|b|' + numOfElements + ' hely',
                        //icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                    });
                    
                    posMarkers.push(marker);

                    marker.neLat = item.get('neLat');
                    marker.neLon = item.get('neLon');
                    marker.swLat = item.get('swLat');
                    marker.swLon = item.get('swLon');

                    google.maps.event.addListener(marker, 'click', function(){
                        var nePosition = new google.maps.LatLng(marker.neLat, marker.neLon);
                        var swPosition = new google.maps.LatLng(marker.swLat, marker.swLon);
                        
                        var bounds = new google.maps.LatLngBounds();

                        bounds.extend(nePosition);
                        bounds.extend(swPosition);
                        
                        map.fitBounds(bounds);
                    });
                }

                if (item.get('cluster') === false) {
                    marker = new google.maps.Marker({
                        map: map,
                        position: gPosition,
                        optimized: false,
                        zIndex: 5
                    });

                    var address = item.get('contactaddressmaps').address[0];
                    var fullAddress = address.zipcode + ', ' + address.settlement + ', ' + address.streetaddress1;

                    var alternateAddress = item.get('locationalternativeaddressmap');

                    if (typeof alternateAddress !== 'undefined' && alternateAddress !== null) {
                        fullAddress = item.get('locationalternativeaddressmap').address[0]['altaddress'] + '<br />';
                    } else {
                        fullAddress = '';
                    }

                    var content = '';

                    if (typeof item.get('locationbasicdata').locname !== 'undefined') {
                        content = /*fullAddress + '<br />' + item.get('locationbasicdata').locname[0]*/ item.get('locationbasicdata').loctypename[0]
                                + '<br /><a href="javascript:algidaFront.app.getController(\'MainController\').logRouteSearch();">Útvonaltervezés Ide!</a>';
                    } else if( typeof item.get('locationbasicdata').loctypename !== 'undefined') {
                        content = /*fullAddress + */ item.get('locationbasicdata').loctypename[0]
                                + '<br /><a href="javascript:algidaFront.app.getController(\'MainController\').logRouteSearch();">Útvonaltervezés Ide!</a>';
                    }

                    var infowindow = new google.maps.InfoWindow({
                        content: content,
                        size: new google.maps.Size(300, 300),
                        width: 200
                    });

                    posMarkers.push(marker);

                    google.maps.event.addListener(marker, 'click', function () {
                        infowindow.open(map, marker);
                        scope.getCurrentInfoWindow().close();
                        scope.setCurrentInfoWindow(infowindow);

                        var customAddress = '';
                        if (typeof algidaFront.app.getController('MainController').getCustomAddressField !== 'undefined' &&
                                typeof algidaFront.app.getController('MainController').getCustomAddressField() !== 'undefined' &&
                                typeof algidaFront.app.getController('MainController').getCustomAddressField().getValue !== 'undefined') {
                            customAddress = algidaFront.app.getController('MainController').getCustomAddressField().getValue();
                        }

                        var routeSearchObject = {
                            lats: String(currentPositionLat),
                            lngs: String(currentPositionLng),
                            latitude: latitude,
                            longitude: longitude,
                            resourceid: item.get('locationId'),
                            customAddress: customAddress,
                            appId: algidaFront.util.Config.getAppId()
                        };
                        
                        scope.setRouteSearch(routeSearchObject);
                    });
                }
            });

            if (posMarkers.length > 0) {
                //map.fitBounds(bounds);
            }

            scope.setPosMarkers(posMarkers);

            setTimeout(function () {
                scope.setCenter(false);
            }, 2000);
        },
        
        localizationFailureHandler: function(result){
            this.getLocationInfoText().setHtml(this.getLocFailText());
            this.getLocationInfo().setHidden(false);
        },
        
        logRouteSearch: function(){
            ga('send', 'event', 'Útvonaltervezés', '-');
            
            var routeSearch = this.getRouteSearch();
            if (routeSearch !== null && typeof routeSearch.customAddress !== 'undefined' && routeSearch.customAddress !== null && Ext.String.trim(routeSearch.customAddress) === '') {
                delete routeSearch.customAddress;
            }
            Ext.Ajax.request({
                url: algidaFront.util.Config.getServer() + '/logger/logroutesearch',
                params: routeSearch,
                method: 'POST',
                success: function (response) {
                    if (Ext.os.deviceType !== 'Desktop') {
                        location.href = 'https://www.google.com/maps/dir/' + routeSearch.lats + ',' + routeSearch.lngs + '/' + routeSearch.latitude + ',' + routeSearch.longitude;
                    } else {
                        if (Ext.browser.name !== 'Safari') {
                            var win = window.open('https://www.google.com/maps/dir/' + routeSearch.lats + ',' + routeSearch.lngs + '/' + routeSearch.latitude + ',' + routeSearch.longitude, '_blank');
                            win.focus();
                        } else {
                            location.href = 'https://www.google.com/maps/dir/' + routeSearch.lats + ',' + routeSearch.lngs + '/' + routeSearch.latitude + ',' + routeSearch.longitude;
                        }
                    }
                },
                failure: function (response) {
                    if (Ext.os.deviceType !== 'Desktop') {
                        location.href = 'https://www.google.com/maps/dir/' + routeSearch.lats + ',' + routeSearch.lngs + '/' + routeSearch.latitude + ',' + routeSearch.longitude;
                    } else {
                        if (Ext.browser.name !== 'Safari') {
                            var win = window.open('https://www.google.com/maps/dir/' + routeSearch.lats + ',' + routeSearch.lngs + '/' + routeSearch.latitude + ',' + routeSearch.longitude, '_blank');
                            win.focus();
                        } else {
                            location.href = 'https://www.google.com/maps/dir/' + routeSearch.lats + ',' + routeSearch.lngs + '/' + routeSearch.latitude + ',' + routeSearch.longitude;
                        }
                    }
                }
            });
        }
});
