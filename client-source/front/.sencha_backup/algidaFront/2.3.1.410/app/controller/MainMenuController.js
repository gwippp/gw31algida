
Ext.define('algidaFront.controller.MainMenuController', {
	extend: 'Ext.app.Controller',
        
	config: {
            
            refs: {
                distanceSlider: 'sliderfield[alias=distanceSlider]',
                distanceText: 'container[alias=distanceText]',
                settingsSubmitButton: 'button[alias=settingsSubmitButton]',
                customAddressField: 'textfield[alias=customAddress]',
                gpsLocateMeButton: 'button[alias=gpsLocateMe]',
                mainMenu: 'mainmenu',
                locationInfo: 'container[alias=locationInfo]',
                locationInfoText: 'container[alias=locationInfoText]'
            },
            
            control: {
                distanceSlider: {
                    drag: 'onDistanceSliderDrag',
                    initialize: 'onDistanceSliderInitialize',
                    change: 'onDistanceSliderChange'
                },
                settingsSubmitButton: {
                    tap: 'onSettingsSubmitButtonTap'
                },
                gpsLocateMeButton: {
                    tap: 'onGpsLocateMeButtonTap'
                },
                customAddressField: {
                    action: 'onCustomAddressFieldAction'
                }
            }
            
        },
        
        onCustomAddressFieldAction: function(){
            this.onSettingsSubmitButtonTap();
        },
        
        onDistanceSliderChange: function(){
            var slider = this.getDistanceSlider();
            var sliderValue = slider.getValue();
            var mainController = this.getApplication().getController('MainController');
            
            this.getDistanceText().setHtml('Algida lelőhelyek <span>' + sliderValue.toString() + '</span> méteren belül');
            
            mainController.setDistance(sliderValue);
        },
        
        onGpsLocateMeButtonTap: function(){
            var mainController = this.getApplication().getController('MainController');
            var scope = this;
            this.getCustomAddressField().setValue('');
            
            this.onSettingsSubmitButtonTap();
            
            this.getLocationInfo().setHidden(false);
            this.getLocationInfoText().setHtml('GPS helymeghatározás folyamatban...');
            
            setTimeout(          function(){
                if(algidaFront.app.getController('MainController').getUserMarker() === false){
                   if(!algidaFront.app.getController('MainController').getMenuOpened()){
                    //algidaFront.app.getController('MainController').onMenuButtonTap();
                   }
                   algidaFront.app.getController('MainController').setNoLocation(true);
                   //Ext.Msg.alert('Sikertelen pozíció lekérdezés', 'Kérlek kapcsold be a GPS lokalizációt készülékeden, vagy add meg pontos helyzeted a beállítások között!');
                   algidaFront.app.getController('MainController').getLocationInfoText().setHtml(algidaFront.app.getController('MainController').getLocFailText());
                }
             },15000);
        },
        
        onSettingsSubmitButtonTap: function(){
            var customAddressField = this.getCustomAddressField();
            var customAddressText = Ext.String.trim(customAddressField.getValue());
            
            if(customAddressText === ''){
                this.getApplication().getController('MainController').setCustomLocation(false);
                this.getApplication().getController('MainController').onRefreshButtonTap();
            }else{
                algidaFront.util.LocationHelper.setApplication(this.getApplication());
                algidaFront.util.LocationHelper.geoCodeAddress(customAddressText);
            }
            
            if(Ext.os.deviceType !== 'Desktop'){
                this.getApplication().getController('MainController').handleMenu();
            }
        },
        
        onDistanceSliderInitialize: function(){
            this.getDistanceSlider().setValue(this.getApplication().getController('MainController').getDistance());
        },
        
        init: function(){
            this.callParent(arguments);
        },
        
        onDistanceSliderDrag: function(element, slider, thumb, e, eOpts){
            var sliderValue = slider.getValue();
            var mainController = this.getApplication().getController('MainController');
            
            this.getDistanceText().setHtml('Algida lelőhelyek <span>' + sliderValue.toString() + '</span> méteren belül');
            
            mainController.setDistance(sliderValue);
        }
});

