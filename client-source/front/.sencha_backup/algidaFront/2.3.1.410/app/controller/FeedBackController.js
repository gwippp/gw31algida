Ext.define('algidaFront.controller.FeedBackController', {
        
	extend: 'Ext.app.Controller',

	config: {
                refs: {
                    'feedBackEmail': 'textfield[alias=feedBackEmail]',
                    'feedBackText': 'textareafield[alias=feedBackText]',
                    'feedBackLocationToggle': 'togglefield[alias=feedBackLocationToggle]',
                    'feedBackSubmitButton': 'button[alias=feedBackSubmitButton]',
                    'mainContainer': 'container[alias=mainContainer]',
                    'feedBackPopUp': 'feedbackpopup',
                    'feedBackNextButton': 'button[alias=feedBackNextButton]',
                    'feedBackCancelButton': 'button[alias=feedBackCancelButton]',
                    'feedBackRightsToggle': 'togglefield[alias=feedBackRightsToggle]',
                    'mainMenu': 'mainmenu',
                    'main': 'main',
                    'titlebar': 'titlebar[alias=mainTitleBar]',
                    'menuTitleBarDesktop': 'titlebar[alias=settingsTitleDesktop]',
                    'menuTitleBarMobile': 'titlebar[alias=settingsTitleMobile]',
                    'refreshButton': 'button[alias=refreshButton]',
                    'menuButton': 'button[alias=menuButton]',
                    'customAddressField': 'textfield[alias=customAddress]'
                },
                control: {
                    feedBackLocationToggle: {
                        change: 'onFeedBackLocationToggleChange'
                    },
                    feedBackSubmitButton: {
                        tap: 'onFeedBackSubmitButton'
                    },
                    feedBackText: {
                        focus: 'onFeedBackTextFocus',
                        blur: 'onFeedBackTextBlur'
                    },
                    feedBackNextButton: {
                        tap: 'onFeedBackNextButtonTap'
                    },
                    feedBackCancelButton: {
                        tap: 'onFeedBackCancelButtonTap'
                    }
                },
		routes: {
		},
                sendLocation: 1,
                defaultText: 'Küldd el nekünk észrevételeidet, segíts az Algida Radar fejlesztésében!'
	},
        
        onFeedBackCancelButtonTap: function(){
            this.maskMain(false);
            this.getFeedBackPopUp().destroy();
        },
        
        onFeedBackNextButtonTap: function(){
            var feedBackRightsToggle = this.getFeedBackRightsToggle();
            var scope = this;
            this.maskMain(false);
            
            //feedBackRightsToggle.getValue()
            
            var feedBackRightsToggleValue = 1;
            
            var posMarker = algidaFront.app.getController('MainController').getUserMarker();
            
            var a = 47.497912;
            var b = 19.040235;
            
            if(typeof posMarker !== 'undefined' && posMarker !== false){
                a = posMarker.getPosition().lat();
                b = posMarker.getPosition().lng();
            }
            
            var values = {
                feedBackEmail: this.getFeedBackEmail().getValue(),
                feedBackText: this.getFeedBackText().getValue(),
                lata: a,
                longb: b,
                address: this.getCustomAddressField().getValue(),
                appId: algidaFront.util.Config.getAppId()
            };
            
            if(this.getCustomAddressField().getValue() !== ''){
                values.address = this.getCustomAddressField().getValue();
            }
            
            if(this.getSendLocation() === 0){
                delete values.address;
                delete values.lata;
                delete values.longb;
            }
            
            if(values.address === ''){
                delete values.address;
            }
            
            if(feedBackRightsToggleValue === 1){
                Ext.Ajax.request({
                    url: algidaFront.util.Config.getServer() + '/logger/sendfeedback',
                    params: values,
                    method: 'POST',
                    success: function(response){
                        scope.maskMain(false);
                        //scope.getFeedBackPopUp().destroy();
                        Ext.Msg.alert("", "Az üzenete küldésre került!", Ext.emptyFn);
                    },
                    failure: function(response){
                        scope.maskMain(false);
                        //scope.getFeedBackPopUp().destroy();
                        Ext.Msg.alert("", "Az üzenete küldésre került!", Ext.emptyFn);
                    }
                });
            }else{
                Ext.Msg.alert("", "Fogadd el a feltételeket az üzenet elküldése előtt!", Ext.emptyFn);
            }
        },
        
        onFeedBackTextFocus: function(){
            var feedBackText = this.getFeedBackText();
            
            if(Ext.String.trim(feedBackText.getValue()) === this.getDefaultText()){
                feedBackText.setValue('');
            }
        },
        
        onFeedBackTextBlur: function(){
            var feedBackText = this.getFeedBackText();
            
            if(Ext.String.trim(feedBackText.getValue().replace(this.getDefaultText(), '')) === ''){
                feedBackText.setValue(this.getDefaultText());
            }
        },
        
        onFeedBackLocationToggleChange: function(){
            var feedBackLocationToggle = this.getFeedBackLocationToggle();
            if(feedBackLocationToggle.getValue() === 1){
                this.setSendLocation(1);
            }else{
                this.setSendLocation(0);
            }
        },

        onFeedBackSubmitButton: function(){
            var mainContainer = this.getMainContainer();
            var mainElement = mainContainer.element;
            var mainMenu = this.getMainMenu();
            var titlebar = this.getTitlebar();
            
            var errors = [];
            var values = {
                feedBackEmail: this.getFeedBackEmail().getValue(),
                feedBackText: this.getFeedBackText().getValue(),
                feedBackLocation: this.getSendLocation()
            };
            
            values.feedBackText = Ext.String.trim(values.feedBackText.replace(this.getDefaultText(), ''));
            
            var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
            if (values.feedBackEmail !== '' && !pattern.test(values.feedBackEmail)){
                errors.push('Helytelen e-mail címet adtál meg!');
            } else {
                
            }
            
            if(values.feedBackText === ''){
                errors.push('Nem adtad meg a visszajelzés szövegét!');
            }
            
            if(errors.length === 0){
                this.onFeedBackNextButtonTap();
                /*
                var feedBackPopUp = Ext.create('algidaFront.view.FeedBackPopUp');
                var feedBackPopUpElement = feedBackPopUp.element;
                mainElement.appendChild(feedBackPopUpElement);

                var popUpLeft = (-1) * algidaFront.app.getController('MainController').getMotionSize() - Number(this.getFeedBackPopUp().getWidth().replace('px', '')) / 2;

                this.getFeedBackPopUp().element.dom.style.marginLeft = popUpLeft + 'px';
                this.maskMain(true);
                */
            }else{
                Ext.Msg.alert("Hiba", errors.join("<br />"), Ext.emptyFn);
            }
        },
        
        maskMain: function(maskFlag){
            var maskFlagNumber = -1;
            var mainContainer = this.getMainContainer();
            var mainElement = mainContainer.element;
            var mainMenu = this.getMainMenu();
            var titlebar = this.getTitlebar();
            
            if(maskFlag === false){
                maskFlagNumber = 1;
            }
            
            mainContainer.setMasked(maskFlag);
            mainMenu.setMasked(maskFlag);
            titlebar.setMasked(maskFlag);
            this.getMenuTitleBarDesktop().setMasked(maskFlag);
            if(typeof this.getMenuTitleBarMobile() !== 'undefined'){
                this.getMenuTitleBarMobile().setMasked(maskFlag);
            }
            this.getRefreshButton().element.dom.style.zIndex = maskFlagNumber;
            this.getMenuButton().element.dom.style.zIndex = maskFlagNumber;
        },
                
	init: function() {
		this.callParent(arguments);
	}
});