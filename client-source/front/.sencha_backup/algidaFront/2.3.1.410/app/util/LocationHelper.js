
Ext.define('algidaFront.util.LocationHelper', {
    
    requires: [
        
    ],
    
    singleton : true,
    
    config : {
        geocoder: new google.maps.Geocoder(),
        application: false
    },

    constructor: function(config) {
        this.initConfig(config);
        this.callParent(arguments);
    },
         
    getCurrentLocation: function(successCB, failureCB, options, scope){
        navigator.geolocation.getCurrentPosition(function(position, b, c, d){
            successCB.apply(scope, [position]);
        }, function(error){
            failureCB.apply(scope, [error]);
        }, options);
    },
    
    watchCurrentLocation: function(successCB, failureCB, options, scope){
        navigator.geolocation.watchPosition(function(position){
            successCB.apply(scope, [position]);
        }, function(result){
            failureCB.apply(scope, [result]);
        }, options);
    },
        
    geoCodeAddress: function(address){
        var scope = this;
        this.getGeocoder().geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK){
                
                for(var i in results[0].address_components){
                    var currentComponent = results[0].address_components[i];
                    if(Ext.Array.contains(currentComponent.types, 'locality') && Ext.Array.contains(currentComponent.types, 'political')){
                        if(currentComponent.long_name === 'Budapest' && currentComponent.short_name === 'Bp'){
                            ga('send', 'event', 'POS keresés', 'Budapest');
                        }else{
                            ga('send', 'event', 'POS keresés', 'Vidék');
                        }
                    }
                }
                
                var customLocation = results[0].geometry.location;
                var mainController = scope.getApplication().getController('MainController');
                
                mainController.setCustomLocation(customLocation);
                mainController.onRefreshButtonTap();
            } else {
                
            }
        });
    }
    
});
