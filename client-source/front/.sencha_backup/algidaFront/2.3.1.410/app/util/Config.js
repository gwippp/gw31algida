
Ext.define('algidaFront.util.Config', {
    
    requires: [
        
    ],
    
    singleton : true,
    
    config : {
        defaultLang: 'hu',
        user: false,
        //server: 'http://localhost:8080/gw3',
        server: (function(){var protocol = 'http'; if(location.protocol.indexOf('https') !== -1){protocol = 'https';}; return protocol;})() + '://' + document.location.host + '/gw3algida',
        //server: 'https://algida.gollywolly.com:8443/gw3',
        //server: 'https://dev.algida-admin.com/gw3',
        //gw31algida.gollywolly.com
        appId: 'GW31-ROCK-EBE3-ALGI',
        currentMaxPages: 1
    },

    constructor: function(config) {
        this.initConfig(config);
        this.callParent(arguments);
    },
         
    getCurrentRoute: function(app){
        var history = app.getHistory(); 
        var actions = history.getActions();
        var currentRoute = '';
        try{
            var currentRoute = actions[actions.length - 1].config.url;
        }catch(err){
            
        }
        return currentRoute;
    },
    
    getObjectSize: function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key))
                size++;
        }
        return size;
    }
    
});

